var movimientos = [
  {
    "anno": 1990,
    "destino": "Egipto",
    "total": 26,
    "cant_mjr": 17,
    "cant_hmbr": 9
  },
  {
    "anno": 1990,
    "destino": "Sudafrica",
    "total": 296,
    "cant_mjr": 178,
    "cant_hmbr": 118
  },
  {
    "anno": 1990,
    "destino": "Chipre",
    "total": 16,
    "cant_mjr": 9,
    "cant_hmbr": 7
  },
  {
    "anno": 1990,
    "destino": "Israel",
    "total": 640,
    "cant_mjr": 345,
    "cant_hmbr": 295
  },
  {
    "anno": 1990,
    "destino": "Jordania",
    "total": 45,
    "cant_mjr": 31,
    "cant_hmbr": 14
  },
  {
    "anno": 1990,
    "destino": "Turquia",
    "total": 9,
    "cant_mjr": 5,
    "cant_hmbr": 4
  },
  {
    "anno": 1990,
    "destino": "Bulgaria",
    "total": 1,
    "cant_mjr": 0,
    "cant_hmbr": 1
  },
  {
    "anno": 1990,
    "destino": "Republica Checa",
    "total": 11,
    "cant_mjr": 4,
    "cant_hmbr": 7
  },
  {
    "anno": 1990,
    "destino": "Hungria",
    "total": 169,
    "cant_mjr": 77,
    "cant_hmbr": 92
  },
  {
    "anno": 1990,
    "destino": "Polonia",
    "total": 67,
    "cant_mjr": 29,
    "cant_hmbr": 38
  },
  {
    "anno": 1990,
    "destino": "Rumania",
    "total": 3,
    "cant_mjr": 2,
    "cant_hmbr": 1
  },
  {
    "anno": 1990,
    "destino": "Federacion Rusa",
    "total": 371,
    "cant_mjr": 189,
    "cant_hmbr": 182
  },
  {
    "anno": 1990,
    "destino": "Dinamarca",
    "total": 188,
    "cant_mjr": 96,
    "cant_hmbr": 92
  },
  {
    "anno": 1990,
    "destino": "Finlandia",
    "total": 34,
    "cant_mjr": 17,
    "cant_hmbr": 17
  },
  {
    "anno": 1990,
    "destino": "Islandia",
    "total": 6,
    "cant_mjr": 4,
    "cant_hmbr": 2
  },
  {
    "anno": 1990,
    "destino": "Irlanda",
    "total": 24,
    "cant_mjr": 13,
    "cant_hmbr": 11
  },
  {
    "anno": 1990,
    "destino": "Letonia",
    "total": 6,
    "cant_mjr": 3,
    "cant_hmbr": 3
  },
  {
    "anno": 1990,
    "destino": "Noruega",
    "total": 80,
    "cant_mjr": 43,
    "cant_hmbr": 37
  },
  {
    "anno": 1990,
    "destino": "Suecia",
    "total": 352,
    "cant_mjr": 177,
    "cant_hmbr": 175
  },
  {
    "anno": 1990,
    "destino": "Reino Unido",
    "total": 555,
    "cant_mjr": 232,
    "cant_hmbr": 323
  },
  {
    "anno": 1990,
    "destino": "Grecia",
    "total": 585,
    "cant_mjr": 344,
    "cant_hmbr": 241
  },
  {
    "anno": 1990,
    "destino": "Italia",
    "total": 9773,
    "cant_mjr": 6853,
    "cant_hmbr": 2920
  },
  {
    "anno": 1990,
    "destino": "Portugal",
    "total": 14959,
    "cant_mjr": 7755,
    "cant_hmbr": 7204
  },
  {
    "anno": 1990,
    "destino": "Serbia",
    "total": 4,
    "cant_mjr": 2,
    "cant_hmbr": 2
  },
  {
    "anno": 1990,
    "destino": "Eslovenia",
    "total": 5,
    "cant_mjr": 2,
    "cant_hmbr": 3
  },
  {
    "anno": 1990,
    "destino": "España",
    "total": 32469,
    "cant_mjr": 17364,
    "cant_hmbr": 15105
  },
  {
    "anno": 1990,
    "destino": "Austria",
    "total": 504,
    "cant_mjr": 303,
    "cant_hmbr": 201
  },
  {
    "anno": 1990,
    "destino": "Belgica",
    "total": 204,
    "cant_mjr": 127,
    "cant_hmbr": 77
  },
  {
    "anno": 1990,
    "destino": "Francia",
    "total": 3253,
    "cant_mjr": 1907,
    "cant_hmbr": 1346
  },
  {
    "anno": 1990,
    "destino": "Alemania",
    "total": 1662,
    "cant_mjr": 864,
    "cant_hmbr": 798
  },
  {
    "anno": 1990,
    "destino": "Luxemburgo",
    "total": 25,
    "cant_mjr": 15,
    "cant_hmbr": 10
  },
  {
    "anno": 1990,
    "destino": "Paises Bajos",
    "total": 1474,
    "cant_mjr": 816,
    "cant_hmbr": 658
  },
  {
    "anno": 1990,
    "destino": "Suiza",
    "total": 1787,
    "cant_mjr": 1031,
    "cant_hmbr": 756
  },
  {
    "anno": 1990,
    "destino": "Antigua y Barbuda",
    "total": 11,
    "cant_mjr": 6,
    "cant_hmbr": 5
  },
  {
    "anno": 1990,
    "destino": "Aruba",
    "total": 1144,
    "cant_mjr": 490,
    "cant_hmbr": 654
  },
  {
    "anno": 1990,
    "destino": "Bahamas",
    "total": 16,
    "cant_mjr": 10,
    "cant_hmbr": 6
  },
  {
    "anno": 1990,
    "destino": "Caribe Holandes",
    "total": 454,
    "cant_mjr": 251,
    "cant_hmbr": 203
  },
  {
    "anno": 1990,
    "destino": "Islas Caiman",
    "total": 11,
    "cant_mjr": 5,
    "cant_hmbr": 6
  },
  {
    "anno": 1990,
    "destino": "Cuba",
    "total": 431,
    "cant_mjr": 209,
    "cant_hmbr": 222
  },
  {
    "anno": 1990,
    "destino": "Dominica",
    "total": 11,
    "cant_mjr": 9,
    "cant_hmbr": 2
  },
  {
    "anno": 1990,
    "destino": "Republica Dominicana",
    "total": 14235,
    "cant_mjr": 6537,
    "cant_hmbr": 7698
  },
  {
    "anno": 1990,
    "destino": "Granada",
    "total": 43,
    "cant_mjr": 22,
    "cant_hmbr": 21
  },
  {
    "anno": 1990,
    "destino": "Haiti",
    "total": 2510,
    "cant_mjr": 1185,
    "cant_hmbr": 1325
  },
  {
    "anno": 1990,
    "destino": "Puerto Rico",
    "total": 1264,
    "cant_mjr": 665,
    "cant_hmbr": 599
  },
  {
    "anno": 1990,
    "destino": "Santa Lucia",
    "total": 27,
    "cant_mjr": 15,
    "cant_hmbr": 12
  },
  {
    "anno": 1990,
    "destino": "Trinidad y Tobago",
    "total": 1337,
    "cant_mjr": 803,
    "cant_hmbr": 534
  },
  {
    "anno": 1990,
    "destino": "Costa Rica",
    "total": 1030,
    "cant_mjr": 501,
    "cant_hmbr": 529
  },
  {
    "anno": 1990,
    "destino": "El Salvador",
    "total": 258,
    "cant_mjr": 149,
    "cant_hmbr": 109
  },
  {
    "anno": 1990,
    "destino": "Guatemala",
    "total": 113,
    "cant_mjr": 72,
    "cant_hmbr": 41
  },
  {
    "anno": 1990,
    "destino": "Honduras",
    "total": 75,
    "cant_mjr": 35,
    "cant_hmbr": 40
  },
  {
    "anno": 1990,
    "destino": "Mexico",
    "total": 1460,
    "cant_mjr": 826,
    "cant_hmbr": 634
  },
  {
    "anno": 1990,
    "destino": "Nicaragua",
    "total": 146,
    "cant_mjr": 78,
    "cant_hmbr": 68
  },
  {
    "anno": 1990,
    "destino": "Panama",
    "total": 487,
    "cant_mjr": 247,
    "cant_hmbr": 240
  },
  {
    "anno": 1990,
    "destino": "Argentina",
    "total": 1981,
    "cant_mjr": 1141,
    "cant_hmbr": 840
  },
  {
    "anno": 1990,
    "destino": "Bolivia",
    "total": 369,
    "cant_mjr": 195,
    "cant_hmbr": 174
  },
  {
    "anno": 1990,
    "destino": "Brasil",
    "total": 1220,
    "cant_mjr": 648,
    "cant_hmbr": 572
  },
  {
    "anno": 1990,
    "destino": "Chile",
    "total": 2349,
    "cant_mjr": 1210,
    "cant_hmbr": 1139
  },
  {
    "anno": 1990,
    "destino": "Colombia",
    "total": 33123,
    "cant_mjr": 17713,
    "cant_hmbr": 15410
  },
  {
    "anno": 1990,
    "destino": "Ecuador",
    "total": 2549,
    "cant_mjr": 1281,
    "cant_hmbr": 1268
  },
  {
    "anno": 1990,
    "destino": "Guyana",
    "total": 478,
    "cant_mjr": 239,
    "cant_hmbr": 239
  },
  {
    "anno": 1990,
    "destino": "Peru",
    "total": 2316,
    "cant_mjr": 1184,
    "cant_hmbr": 1132
  },
  {
    "anno": 1990,
    "destino": "Uruguay",
    "total": 773,
    "cant_mjr": 365,
    "cant_hmbr": 408
  },
  {
    "anno": 1990,
    "destino": "Canada",
    "total": 3339,
    "cant_mjr": 1714,
    "cant_hmbr": 1625
  },
  {
    "anno": 1990,
    "destino": "Estados Unidos",
    "total": 42119,
    "cant_mjr": 25859,
    "cant_hmbr": 16260
  },
  {
    "anno": 1990,
    "destino": "Australia",
    "total": 606,
    "cant_mjr": 328,
    "cant_hmbr": 278
  },
  {
    "anno": 1995,
    "destino": "Egipto",
    "total": 25,
    "cant_mjr": 16,
    "cant_hmbr": 9
  },
  {
    "anno": 1995,
    "destino": "Sudafrica",
    "total": 184,
    "cant_mjr": 102,
    "cant_hmbr": 82
  },
  {
    "anno": 1995,
    "destino": "Chipre",
    "total": 23,
    "cant_mjr": 13,
    "cant_hmbr": 10
  },
  {
    "anno": 1995,
    "destino": "Israel",
    "total": 624,
    "cant_mjr": 341,
    "cant_hmbr": 283
  },
  {
    "anno": 1995,
    "destino": "Jordania",
    "total": 42,
    "cant_mjr": 25,
    "cant_hmbr": 17
  },
  {
    "anno": 1995,
    "destino": "Turquia",
    "total": 22,
    "cant_mjr": 12,
    "cant_hmbr": 10
  },
  {
    "anno": 1995,
    "destino": "Bulgaria",
    "total": 7,
    "cant_mjr": 3,
    "cant_hmbr": 4
  },
  {
    "anno": 1995,
    "destino": "Republica Checa",
    "total": 17,
    "cant_mjr": 6,
    "cant_hmbr": 11
  },
  {
    "anno": 1995,
    "destino": "Hungria",
    "total": 150,
    "cant_mjr": 70,
    "cant_hmbr": 80
  },
  {
    "anno": 1995,
    "destino": "Polonia",
    "total": 57,
    "cant_mjr": 25,
    "cant_hmbr": 32
  },
  {
    "anno": 1995,
    "destino": "Rumania",
    "total": 5,
    "cant_mjr": 3,
    "cant_hmbr": 2
  },
  {
    "anno": 1995,
    "destino": "Federacion Rusa",
    "total": 376,
    "cant_mjr": 185,
    "cant_hmbr": 191
  },
  {
    "anno": 1995,
    "destino": "Dinamarca",
    "total": 289,
    "cant_mjr": 161,
    "cant_hmbr": 128
  },
  {
    "anno": 1995,
    "destino": "Finlandia",
    "total": 41,
    "cant_mjr": 20,
    "cant_hmbr": 21
  },
  {
    "anno": 1995,
    "destino": "Islandia",
    "total": 9,
    "cant_mjr": 6,
    "cant_hmbr": 3
  },
  {
    "anno": 1995,
    "destino": "Irlanda",
    "total": 67,
    "cant_mjr": 38,
    "cant_hmbr": 29
  },
  {
    "anno": 1995,
    "destino": "Letonia",
    "total": 9,
    "cant_mjr": 4,
    "cant_hmbr": 5
  },
  {
    "anno": 1995,
    "destino": "Noruega",
    "total": 133,
    "cant_mjr": 78,
    "cant_hmbr": 55
  },
  {
    "anno": 1995,
    "destino": "Suecia",
    "total": 418,
    "cant_mjr": 212,
    "cant_hmbr": 206
  },
  {
    "anno": 1995,
    "destino": "Reino Unido",
    "total": 2182,
    "cant_mjr": 1166,
    "cant_hmbr": 1016
  },
  {
    "anno": 1995,
    "destino": "Croacia",
    "total": 13,
    "cant_mjr": 7,
    "cant_hmbr": 6
  },
  {
    "anno": 1995,
    "destino": "Grecia",
    "total": 716,
    "cant_mjr": 418,
    "cant_hmbr": 298
  },
  {
    "anno": 1995,
    "destino": "Italia",
    "total": 9261,
    "cant_mjr": 6601,
    "cant_hmbr": 2660
  },
  {
    "anno": 1995,
    "destino": "Portugal",
    "total": 18220,
    "cant_mjr": 9541,
    "cant_hmbr": 8679
  },
  {
    "anno": 1995,
    "destino": "Serbia",
    "total": 27,
    "cant_mjr": 13,
    "cant_hmbr": 14
  },
  {
    "anno": 1995,
    "destino": "Eslovenia",
    "total": 9,
    "cant_mjr": 4,
    "cant_hmbr": 5
  },
  {
    "anno": 1995,
    "destino": "España",
    "total": 38704,
    "cant_mjr": 20600,
    "cant_hmbr": 18104
  },
  {
    "anno": 1995,
    "destino": "Austria",
    "total": 569,
    "cant_mjr": 337,
    "cant_hmbr": 232
  },
  {
    "anno": 1995,
    "destino": "Belgica",
    "total": 203,
    "cant_mjr": 127,
    "cant_hmbr": 76
  },
  {
    "anno": 1995,
    "destino": "Francia",
    "total": 3358,
    "cant_mjr": 2002,
    "cant_hmbr": 1356
  },
  {
    "anno": 1995,
    "destino": "Alemania",
    "total": 2306,
    "cant_mjr": 1408,
    "cant_hmbr": 898
  },
  {
    "anno": 1995,
    "destino": "Luxemburgo",
    "total": 23,
    "cant_mjr": 13,
    "cant_hmbr": 10
  },
  {
    "anno": 1995,
    "destino": "Paises Bajos",
    "total": 1774,
    "cant_mjr": 1001,
    "cant_hmbr": 773
  },
  {
    "anno": 1995,
    "destino": "Suiza",
    "total": 2224,
    "cant_mjr": 1233,
    "cant_hmbr": 991
  },
  {
    "anno": 1995,
    "destino": "Antigua y Barbuda",
    "total": 22,
    "cant_mjr": 12,
    "cant_hmbr": 10
  },
  {
    "anno": 1995,
    "destino": "Aruba",
    "total": 2000,
    "cant_mjr": 900,
    "cant_hmbr": 1100
  },
  {
    "anno": 1995,
    "destino": "Bahamas",
    "total": 20,
    "cant_mjr": 12,
    "cant_hmbr": 8
  },
  {
    "anno": 1995,
    "destino": "Caribe Holandes",
    "total": 947,
    "cant_mjr": 526,
    "cant_hmbr": 421
  },
  {
    "anno": 1995,
    "destino": "Islas Caiman",
    "total": 24,
    "cant_mjr": 11,
    "cant_hmbr": 13
  },
  {
    "anno": 1995,
    "destino": "Cuba",
    "total": 327,
    "cant_mjr": 165,
    "cant_hmbr": 162
  },
  {
    "anno": 1995,
    "destino": "Dominica",
    "total": 14,
    "cant_mjr": 12,
    "cant_hmbr": 2
  },
  {
    "anno": 1995,
    "destino": "Republica Dominicana",
    "total": 15811,
    "cant_mjr": 7437,
    "cant_hmbr": 8374
  },
  {
    "anno": 1995,
    "destino": "Granada",
    "total": 175,
    "cant_mjr": 28,
    "cant_hmbr": 147
  },
  {
    "anno": 1995,
    "destino": "Haiti",
    "total": 2954,
    "cant_mjr": 1373,
    "cant_hmbr": 1581
  },
  {
    "anno": 1995,
    "destino": "Puerto Rico",
    "total": 1617,
    "cant_mjr": 849,
    "cant_hmbr": 768
  },
  {
    "anno": 1995,
    "destino": "Santa Lucia",
    "total": 35,
    "cant_mjr": 19,
    "cant_hmbr": 16
  },
  {
    "anno": 1995,
    "destino": "Trinidad y Tobago",
    "total": 1213,
    "cant_mjr": 730,
    "cant_hmbr": 483
  },
  {
    "anno": 1995,
    "destino": "Costa Rica",
    "total": 834,
    "cant_mjr": 407,
    "cant_hmbr": 427
  },
  {
    "anno": 1995,
    "destino": "El Salvador",
    "total": 219,
    "cant_mjr": 125,
    "cant_hmbr": 94
  },
  {
    "anno": 1995,
    "destino": "Guatemala",
    "total": 144,
    "cant_mjr": 83,
    "cant_hmbr": 61
  },
  {
    "anno": 1995,
    "destino": "Honduras",
    "total": 74,
    "cant_mjr": 36,
    "cant_hmbr": 38
  },
  {
    "anno": 1995,
    "destino": "Mexico",
    "total": 2194,
    "cant_mjr": 1222,
    "cant_hmbr": 972
  },
  {
    "anno": 1995,
    "destino": "Nicaragua",
    "total": 116,
    "cant_mjr": 63,
    "cant_hmbr": 53
  },
  {
    "anno": 1995,
    "destino": "Panama",
    "total": 715,
    "cant_mjr": 361,
    "cant_hmbr": 354
  },
  {
    "anno": 1995,
    "destino": "Argentina",
    "total": 2290,
    "cant_mjr": 1269,
    "cant_hmbr": 1021
  },
  {
    "anno": 1995,
    "destino": "Bolivia",
    "total": 458,
    "cant_mjr": 241,
    "cant_hmbr": 217
  },
  {
    "anno": 1995,
    "destino": "Brasil",
    "total": 1694,
    "cant_mjr": 860,
    "cant_hmbr": 834
  },
  {
    "anno": 1995,
    "destino": "Chile",
    "total": 3148,
    "cant_mjr": 1611,
    "cant_hmbr": 1537
  },
  {
    "anno": 1995,
    "destino": "Colombia",
    "total": 35162,
    "cant_mjr": 18652,
    "cant_hmbr": 16510
  },
  {
    "anno": 1995,
    "destino": "Ecuador",
    "total": 3120,
    "cant_mjr": 1625,
    "cant_hmbr": 1495
  },
  {
    "anno": 1995,
    "destino": "Guyana",
    "total": 768,
    "cant_mjr": 379,
    "cant_hmbr": 389
  },
  {
    "anno": 1995,
    "destino": "Peru",
    "total": 2021,
    "cant_mjr": 1045,
    "cant_hmbr": 976
  },
  {
    "anno": 1995,
    "destino": "Uruguay",
    "total": 713,
    "cant_mjr": 350,
    "cant_hmbr": 363
  },
  {
    "anno": 1995,
    "destino": "Canada",
    "total": 5582,
    "cant_mjr": 2883,
    "cant_hmbr": 2699
  },
  {
    "anno": 1995,
    "destino": "Estados Unidos",
    "total": 75572,
    "cant_mjr": 45855,
    "cant_hmbr": 29717
  },
  {
    "anno": 1995,
    "destino": "Australia",
    "total": 915,
    "cant_mjr": 484,
    "cant_hmbr": 431
  },
  {
    "anno": 2000,
    "destino": "Egipto",
    "total": 25,
    "cant_mjr": 16,
    "cant_hmbr": 9
  },
  {
    "anno": 2000,
    "destino": "Sudafrica",
    "total": 125,
    "cant_mjr": 59,
    "cant_hmbr": 66
  },
  {
    "anno": 2000,
    "destino": "Chipre",
    "total": 29,
    "cant_mjr": 17,
    "cant_hmbr": 12
  },
  {
    "anno": 2000,
    "destino": "Israel",
    "total": 574,
    "cant_mjr": 315,
    "cant_hmbr": 259
  },
  {
    "anno": 2000,
    "destino": "Jordania",
    "total": 38,
    "cant_mjr": 19,
    "cant_hmbr": 19
  },
  {
    "anno": 2000,
    "destino": "Turquia",
    "total": 37,
    "cant_mjr": 20,
    "cant_hmbr": 17
  },
  {
    "anno": 2000,
    "destino": "Bulgaria",
    "total": 12,
    "cant_mjr": 6,
    "cant_hmbr": 6
  },
  {
    "anno": 2000,
    "destino": "Republica Checa",
    "total": 23,
    "cant_mjr": 8,
    "cant_hmbr": 15
  },
  {
    "anno": 2000,
    "destino": "Hungria",
    "total": 131,
    "cant_mjr": 62,
    "cant_hmbr": 69
  },
  {
    "anno": 2000,
    "destino": "Polonia",
    "total": 49,
    "cant_mjr": 22,
    "cant_hmbr": 27
  },
  {
    "anno": 2000,
    "destino": "Rumania",
    "total": 7,
    "cant_mjr": 5,
    "cant_hmbr": 2
  },
  {
    "anno": 2000,
    "destino": "Federacion Rusa",
    "total": 369,
    "cant_mjr": 176,
    "cant_hmbr": 193
  },
  {
    "anno": 2000,
    "destino": "Dinamarca",
    "total": 389,
    "cant_mjr": 225,
    "cant_hmbr": 164
  },
  {
    "anno": 2000,
    "destino": "Finlandia",
    "total": 47,
    "cant_mjr": 22,
    "cant_hmbr": 25
  },
  {
    "anno": 2000,
    "destino": "Islandia",
    "total": 12,
    "cant_mjr": 8,
    "cant_hmbr": 4
  },
  {
    "anno": 2000,
    "destino": "Irlanda",
    "total": 148,
    "cant_mjr": 81,
    "cant_hmbr": 67
  },
  {
    "anno": 2000,
    "destino": "Letonia",
    "total": 12,
    "cant_mjr": 6,
    "cant_hmbr": 6
  },
  {
    "anno": 2000,
    "destino": "Noruega",
    "total": 178,
    "cant_mjr": 109,
    "cant_hmbr": 69
  },
  {
    "anno": 2000,
    "destino": "Suecia",
    "total": 449,
    "cant_mjr": 229,
    "cant_hmbr": 220
  },
  {
    "anno": 2000,
    "destino": "Reino Unido",
    "total": 3846,
    "cant_mjr": 2124,
    "cant_hmbr": 1722
  },
  {
    "anno": 2000,
    "destino": "Croacia",
    "total": 24,
    "cant_mjr": 13,
    "cant_hmbr": 11
  },
  {
    "anno": 2000,
    "destino": "Grecia",
    "total": 859,
    "cant_mjr": 507,
    "cant_hmbr": 352
  },
  {
    "anno": 2000,
    "destino": "Italia",
    "total": 8748,
    "cant_mjr": 6348,
    "cant_hmbr": 2400
  },
  {
    "anno": 2000,
    "destino": "Portugal",
    "total": 22222,
    "cant_mjr": 11678,
    "cant_hmbr": 10544
  },
  {
    "anno": 2000,
    "destino": "Serbia",
    "total": 38,
    "cant_mjr": 19,
    "cant_hmbr": 19
  },
  {
    "anno": 2000,
    "destino": "Eslovenia",
    "total": 15,
    "cant_mjr": 7,
    "cant_hmbr": 8
  },
  {
    "anno": 2000,
    "destino": "España",
    "total": 61587,
    "cant_mjr": 31800,
    "cant_hmbr": 29787
  },
  {
    "anno": 2000,
    "destino": "Austria",
    "total": 633,
    "cant_mjr": 370,
    "cant_hmbr": 263
  },
  {
    "anno": 2000,
    "destino": "Belgica",
    "total": 197,
    "cant_mjr": 122,
    "cant_hmbr": 75
  },
  {
    "anno": 2000,
    "destino": "Francia",
    "total": 3463,
    "cant_mjr": 2097,
    "cant_hmbr": 1366
  },
  {
    "anno": 2000,
    "destino": "Alemania",
    "total": 2950,
    "cant_mjr": 1950,
    "cant_hmbr": 999
  },
  {
    "anno": 2000,
    "destino": "Luxemburgo",
    "total": 22,
    "cant_mjr": 13,
    "cant_hmbr": 9
  },
  {
    "anno": 2000,
    "destino": "Paises Bajos",
    "total": 2136,
    "cant_mjr": 1225,
    "cant_hmbr": 911
  },
  {
    "anno": 2000,
    "destino": "Suiza",
    "total": 2671,
    "cant_mjr": 1437,
    "cant_hmbr": 1234
  },
  {
    "anno": 2000,
    "destino": "Antigua y Barbuda",
    "total": 32,
    "cant_mjr": 17,
    "cant_hmbr": 15
  },
  {
    "anno": 2000,
    "destino": "Aruba",
    "total": 2856,
    "cant_mjr": 1309,
    "cant_hmbr": 1547
  },
  {
    "anno": 2000,
    "destino": "Bahamas",
    "total": 24,
    "cant_mjr": 14,
    "cant_hmbr": 10
  },
  {
    "anno": 2000,
    "destino": "Caribe Holandes",
    "total": 1441,
    "cant_mjr": 802,
    "cant_hmbr": 639
  },
  {
    "anno": 2000,
    "destino": "Islas Caiman",
    "total": 38,
    "cant_mjr": 18,
    "cant_hmbr": 20
  },
  {
    "anno": 2000,
    "destino": "Cuba",
    "total": 222,
    "cant_mjr": 121,
    "cant_hmbr": 101
  },
  {
    "anno": 2000,
    "destino": "Dominica",
    "total": 17,
    "cant_mjr": 14,
    "cant_hmbr": 3
  },
  {
    "anno": 2000,
    "destino": "Republica Dominicana",
    "total": 17386,
    "cant_mjr": 8336,
    "cant_hmbr": 9050
  },
  {
    "anno": 2000,
    "destino": "Granada",
    "total": 308,
    "cant_mjr": 34,
    "cant_hmbr": 274
  },
  {
    "anno": 2000,
    "destino": "Haiti",
    "total": 3397,
    "cant_mjr": 1561,
    "cant_hmbr": 1836
  },
  {
    "anno": 2000,
    "destino": "Puerto Rico",
    "total": 1974,
    "cant_mjr": 1037,
    "cant_hmbr": 937
  },
  {
    "anno": 2000,
    "destino": "Santa Lucia",
    "total": 49,
    "cant_mjr": 27,
    "cant_hmbr": 22
  },
  {
    "anno": 2000,
    "destino": "Trinidad y Tobago",
    "total": 1413,
    "cant_mjr": 833,
    "cant_hmbr": 580
  },
  {
    "anno": 2000,
    "destino": "Costa Rica",
    "total": 1054,
    "cant_mjr": 532,
    "cant_hmbr": 522
  },
  {
    "anno": 2000,
    "destino": "El Salvador",
    "total": 179,
    "cant_mjr": 100,
    "cant_hmbr": 79
  },
  {
    "anno": 2000,
    "destino": "Guatemala",
    "total": 175,
    "cant_mjr": 94,
    "cant_hmbr": 81
  },
  {
    "anno": 2000,
    "destino": "Honduras",
    "total": 73,
    "cant_mjr": 37,
    "cant_hmbr": 36
  },
  {
    "anno": 2000,
    "destino": "Mexico",
    "total": 3024,
    "cant_mjr": 1665,
    "cant_hmbr": 1359
  },
  {
    "anno": 2000,
    "destino": "Nicaragua",
    "total": 125,
    "cant_mjr": 64,
    "cant_hmbr": 61
  },
  {
    "anno": 2000,
    "destino": "Panama",
    "total": 989,
    "cant_mjr": 500,
    "cant_hmbr": 489
  },
  {
    "anno": 2000,
    "destino": "Argentina",
    "total": 2600,
    "cant_mjr": 1397,
    "cant_hmbr": 1203
  },
  {
    "anno": 2000,
    "destino": "Bolivia",
    "total": 543,
    "cant_mjr": 284,
    "cant_hmbr": 259
  },
  {
    "anno": 2000,
    "destino": "Brasil",
    "total": 2167,
    "cant_mjr": 1071,
    "cant_hmbr": 1096
  },
  {
    "anno": 2000,
    "destino": "Chile",
    "total": 4044,
    "cant_mjr": 2101,
    "cant_hmbr": 1943
  },
  {
    "anno": 2000,
    "destino": "Colombia",
    "total": 37200,
    "cant_mjr": 19591,
    "cant_hmbr": 17609
  },
  {
    "anno": 2000,
    "destino": "Ecuador",
    "total": 3691,
    "cant_mjr": 1968,
    "cant_hmbr": 1723
  },
  {
    "anno": 2000,
    "destino": "Guyana",
    "total": 1057,
    "cant_mjr": 518,
    "cant_hmbr": 539
  },
  {
    "anno": 2000,
    "destino": "Peru",
    "total": 2362,
    "cant_mjr": 1216,
    "cant_hmbr": 1146
  },
  {
    "anno": 2000,
    "destino": "Uruguay",
    "total": 659,
    "cant_mjr": 337,
    "cant_hmbr": 322
  },
  {
    "anno": 2000,
    "destino": "Canada",
    "total": 7958,
    "cant_mjr": 4120,
    "cant_hmbr": 3838
  },
  {
    "anno": 2000,
    "destino": "Estados Unidos",
    "total": 109748,
    "cant_mjr": 66196,
    "cant_hmbr": 43552
  },
  {
    "anno": 2000,
    "destino": "Australia",
    "total": 1170,
    "cant_mjr": 610,
    "cant_hmbr": 560
  },
  {
    "anno": 2005,
    "destino": "Egipto",
    "total": 30,
    "cant_mjr": 18,
    "cant_hmbr": 12
  },
  {
    "anno": 2005,
    "destino": "Sudafrica",
    "total": 149,
    "cant_mjr": 72,
    "cant_hmbr": 77
  },
  {
    "anno": 2005,
    "destino": "Chipre",
    "total": 42,
    "cant_mjr": 25,
    "cant_hmbr": 17
  },
  {
    "anno": 2005,
    "destino": "Israel",
    "total": 465,
    "cant_mjr": 256,
    "cant_hmbr": 209
  },
  {
    "anno": 2005,
    "destino": "Jordania",
    "total": 55,
    "cant_mjr": 24,
    "cant_hmbr": 31
  },
  {
    "anno": 2005,
    "destino": "Turquia",
    "total": 38,
    "cant_mjr": 21,
    "cant_hmbr": 17
  },
  {
    "anno": 2005,
    "destino": "Bulgaria",
    "total": 19,
    "cant_mjr": 10,
    "cant_hmbr": 9
  },
  {
    "anno": 2005,
    "destino": "Republica Checa",
    "total": 56,
    "cant_mjr": 24,
    "cant_hmbr": 32
  },
  {
    "anno": 2005,
    "destino": "Hungria",
    "total": 162,
    "cant_mjr": 75,
    "cant_hmbr": 87
  },
  {
    "anno": 2005,
    "destino": "Polonia",
    "total": 61,
    "cant_mjr": 15,
    "cant_hmbr": 46
  },
  {
    "anno": 2005,
    "destino": "Rumania",
    "total": 8,
    "cant_mjr": 6,
    "cant_hmbr": 2
  },
  {
    "anno": 2005,
    "destino": "Federacion Rusa",
    "total": 234,
    "cant_mjr": 114,
    "cant_hmbr": 120
  },
  {
    "anno": 2005,
    "destino": "Dinamarca",
    "total": 498,
    "cant_mjr": 289,
    "cant_hmbr": 209
  },
  {
    "anno": 2005,
    "destino": "Finlandia",
    "total": 90,
    "cant_mjr": 42,
    "cant_hmbr": 48
  },
  {
    "anno": 2005,
    "destino": "Islandia",
    "total": 25,
    "cant_mjr": 16,
    "cant_hmbr": 9
  },
  {
    "anno": 2005,
    "destino": "Irlanda",
    "total": 149,
    "cant_mjr": 76,
    "cant_hmbr": 73
  },
  {
    "anno": 2005,
    "destino": "Letonia",
    "total": 23,
    "cant_mjr": 10,
    "cant_hmbr": 13
  },
  {
    "anno": 2005,
    "destino": "Noruega",
    "total": 261,
    "cant_mjr": 164,
    "cant_hmbr": 97
  },
  {
    "anno": 2005,
    "destino": "Suecia",
    "total": 585,
    "cant_mjr": 308,
    "cant_hmbr": 277
  },
  {
    "anno": 2005,
    "destino": "Reino Unido",
    "total": 6053,
    "cant_mjr": 3301,
    "cant_hmbr": 2752
  },
  {
    "anno": 2005,
    "destino": "Croacia",
    "total": 40,
    "cant_mjr": 22,
    "cant_hmbr": 18
  },
  {
    "anno": 2005,
    "destino": "Grecia",
    "total": 954,
    "cant_mjr": 567,
    "cant_hmbr": 387
  },
  {
    "anno": 2005,
    "destino": "Italia",
    "total": 28803,
    "cant_mjr": 17196,
    "cant_hmbr": 11607
  },
  {
    "anno": 2005,
    "destino": "Portugal",
    "total": 23744,
    "cant_mjr": 12509,
    "cant_hmbr": 11235
  },
  {
    "anno": 2005,
    "destino": "Serbia",
    "total": 38,
    "cant_mjr": 19,
    "cant_hmbr": 19
  },
  {
    "anno": 2005,
    "destino": "Eslovenia",
    "total": 27,
    "cant_mjr": 11,
    "cant_hmbr": 16
  },
  {
    "anno": 2005,
    "destino": "España",
    "total": 108707,
    "cant_mjr": 57796,
    "cant_hmbr": 50911
  },
  {
    "anno": 2005,
    "destino": "Austria",
    "total": 708,
    "cant_mjr": 424,
    "cant_hmbr": 284
  },
  {
    "anno": 2005,
    "destino": "Belgica",
    "total": 321,
    "cant_mjr": 199,
    "cant_hmbr": 122
  },
  {
    "anno": 2005,
    "destino": "Francia",
    "total": 5493,
    "cant_mjr": 3069,
    "cant_hmbr": 2424
  },
  {
    "anno": 2005,
    "destino": "Alemania",
    "total": 5362,
    "cant_mjr": 3115,
    "cant_hmbr": 2247
  },
  {
    "anno": 2005,
    "destino": "Luxemburgo",
    "total": 19,
    "cant_mjr": 10,
    "cant_hmbr": 9
  },
  {
    "anno": 2005,
    "destino": "Paises Bajos",
    "total": 2898,
    "cant_mjr": 1694,
    "cant_hmbr": 1204
  },
  {
    "anno": 2005,
    "destino": "Suiza",
    "total": 3003,
    "cant_mjr": 1691,
    "cant_hmbr": 1312
  },
  {
    "anno": 2005,
    "destino": "Antigua y Barbuda",
    "total": 35,
    "cant_mjr": 18,
    "cant_hmbr": 17
  },
  {
    "anno": 2005,
    "destino": "Aruba",
    "total": 3074,
    "cant_mjr": 1477,
    "cant_hmbr": 1597
  },
  {
    "anno": 2005,
    "destino": "Bahamas",
    "total": 26,
    "cant_mjr": 16,
    "cant_hmbr": 10
  },
  {
    "anno": 2005,
    "destino": "Caribe Holandes",
    "total": 1535,
    "cant_mjr": 854,
    "cant_hmbr": 681
  },
  {
    "anno": 2005,
    "destino": "Islas Caiman",
    "total": 36,
    "cant_mjr": 17,
    "cant_hmbr": 19
  },
  {
    "anno": 2005,
    "destino": "Cuba",
    "total": 221,
    "cant_mjr": 119,
    "cant_hmbr": 102
  },
  {
    "anno": 2005,
    "destino": "Dominica",
    "total": 21,
    "cant_mjr": 17,
    "cant_hmbr": 4
  },
  {
    "anno": 2005,
    "destino": "Republica Dominicana",
    "total": 11299,
    "cant_mjr": 5480,
    "cant_hmbr": 5819
  },
  {
    "anno": 2005,
    "destino": "Granada",
    "total": 322,
    "cant_mjr": 35,
    "cant_hmbr": 287
  },
  {
    "anno": 2005,
    "destino": "Haiti",
    "total": 4007,
    "cant_mjr": 1825,
    "cant_hmbr": 2182
  },
  {
    "anno": 2005,
    "destino": "Puerto Rico",
    "total": 2198,
    "cant_mjr": 1158,
    "cant_hmbr": 1040
  },
  {
    "anno": 2005,
    "destino": "Santa Lucia",
    "total": 57,
    "cant_mjr": 31,
    "cant_hmbr": 26
  },
  {
    "anno": 2005,
    "destino": "San Martin",
    "total": 20,
    "cant_mjr": 9,
    "cant_hmbr": 11
  },
  {
    "anno": 2005,
    "destino": "Trinidad y Tobago",
    "total": 1516,
    "cant_mjr": 896,
    "cant_hmbr": 620
  },
  {
    "anno": 2005,
    "destino": "Costa Rica",
    "total": 690,
    "cant_mjr": 353,
    "cant_hmbr": 337
  },
  {
    "anno": 2005,
    "destino": "El Salvador",
    "total": 206,
    "cant_mjr": 113,
    "cant_hmbr": 93
  },
  {
    "anno": 2005,
    "destino": "Guatemala",
    "total": 211,
    "cant_mjr": 111,
    "cant_hmbr": 100
  },
  {
    "anno": 2005,
    "destino": "Honduras",
    "total": 73,
    "cant_mjr": 38,
    "cant_hmbr": 35
  },
  {
    "anno": 2005,
    "destino": "Mexico",
    "total": 6526,
    "cant_mjr": 3633,
    "cant_hmbr": 2893
  },
  {
    "anno": 2005,
    "destino": "Nicaragua",
    "total": 139,
    "cant_mjr": 69,
    "cant_hmbr": 70
  },
  {
    "anno": 2005,
    "destino": "Panama",
    "total": 4592,
    "cant_mjr": 2279,
    "cant_hmbr": 2313
  },
  {
    "anno": 2005,
    "destino": "Argentina",
    "total": 1918,
    "cant_mjr": 1014,
    "cant_hmbr": 904
  },
  {
    "anno": 2005,
    "destino": "Bolivia",
    "total": 604,
    "cant_mjr": 295,
    "cant_hmbr": 309
  },
  {
    "anno": 2005,
    "destino": "Brasil",
    "total": 2524,
    "cant_mjr": 1238,
    "cant_hmbr": 1286
  },
  {
    "anno": 2005,
    "destino": "Chile",
    "total": 3279,
    "cant_mjr": 1486,
    "cant_hmbr": 1793
  },
  {
    "anno": 2005,
    "destino": "Colombia",
    "total": 37137,
    "cant_mjr": 19335,
    "cant_hmbr": 17802
  },
  {
    "anno": 2005,
    "destino": "Ecuador",
    "total": 4357,
    "cant_mjr": 2274,
    "cant_hmbr": 2083
  },
  {
    "anno": 2005,
    "destino": "Guyana",
    "total": 1424,
    "cant_mjr": 691,
    "cant_hmbr": 733
  },
  {
    "anno": 2005,
    "destino": "Peru",
    "total": 2763,
    "cant_mjr": 1416,
    "cant_hmbr": 1347
  },
  {
    "anno": 2005,
    "destino": "Uruguay",
    "total": 610,
    "cant_mjr": 311,
    "cant_hmbr": 299
  },
  {
    "anno": 2005,
    "destino": "Canada",
    "total": 12434,
    "cant_mjr": 6605,
    "cant_hmbr": 5829
  },
  {
    "anno": 2005,
    "destino": "Estados Unidos",
    "total": 142706,
    "cant_mjr": 85598,
    "cant_hmbr": 57108
  },
  {
    "anno": 2005,
    "destino": "Australia",
    "total": 1600,
    "cant_mjr": 870,
    "cant_hmbr": 730
  },
  {
    "anno": 2010,
    "destino": "Egipto",
    "total": 32,
    "cant_mjr": 20,
    "cant_hmbr": 12
  },
  {
    "anno": 2010,
    "destino": "Sudafrica",
    "total": 258,
    "cant_mjr": 127,
    "cant_hmbr": 131
  },
  {
    "anno": 2010,
    "destino": "Chipre",
    "total": 68,
    "cant_mjr": 40,
    "cant_hmbr": 28
  },
  {
    "anno": 2010,
    "destino": "Israel",
    "total": 362,
    "cant_mjr": 200,
    "cant_hmbr": 162
  },
  {
    "anno": 2010,
    "destino": "Jordania",
    "total": 70,
    "cant_mjr": 29,
    "cant_hmbr": 41
  },
  {
    "anno": 2010,
    "destino": "Turquia",
    "total": 39,
    "cant_mjr": 22,
    "cant_hmbr": 17
  },
  {
    "anno": 2010,
    "destino": "Bulgaria",
    "total": 25,
    "cant_mjr": 13,
    "cant_hmbr": 12
  },
  {
    "anno": 2010,
    "destino": "Republica Checa",
    "total": 85,
    "cant_mjr": 36,
    "cant_hmbr": 49
  },
  {
    "anno": 2010,
    "destino": "Hungria",
    "total": 193,
    "cant_mjr": 87,
    "cant_hmbr": 106
  },
  {
    "anno": 2010,
    "destino": "Polonia",
    "total": 75,
    "cant_mjr": 9,
    "cant_hmbr": 66
  },
  {
    "anno": 2010,
    "destino": "Rumania",
    "total": 13,
    "cant_mjr": 11,
    "cant_hmbr": 2
  },
  {
    "anno": 2010,
    "destino": "Federacion Rusa",
    "total": 96,
    "cant_mjr": 50,
    "cant_hmbr": 46
  },
  {
    "anno": 2010,
    "destino": "Dinamarca",
    "total": 606,
    "cant_mjr": 353,
    "cant_hmbr": 253
  },
  {
    "anno": 2010,
    "destino": "Estonia",
    "total": 8,
    "cant_mjr": 3,
    "cant_hmbr": 5
  },
  {
    "anno": 2010,
    "destino": "Finlandia",
    "total": 132,
    "cant_mjr": 61,
    "cant_hmbr": 71
  },
  {
    "anno": 2010,
    "destino": "Islandia",
    "total": 36,
    "cant_mjr": 23,
    "cant_hmbr": 13
  },
  {
    "anno": 2010,
    "destino": "Irlanda",
    "total": 125,
    "cant_mjr": 69,
    "cant_hmbr": 56
  },
  {
    "anno": 2010,
    "destino": "Letonia",
    "total": 35,
    "cant_mjr": 14,
    "cant_hmbr": 21
  },
  {
    "anno": 2010,
    "destino": "Noruega",
    "total": 631,
    "cant_mjr": 356,
    "cant_hmbr": 275
  },
  {
    "anno": 2010,
    "destino": "Suecia",
    "total": 804,
    "cant_mjr": 418,
    "cant_hmbr": 386
  },
  {
    "anno": 2010,
    "destino": "Reino Unido",
    "total": 8754,
    "cant_mjr": 4715,
    "cant_hmbr": 4039
  },
  {
    "anno": 2010,
    "destino": "Croacia",
    "total": 55,
    "cant_mjr": 31,
    "cant_hmbr": 24
  },
  {
    "anno": 2010,
    "destino": "Grecia",
    "total": 1091,
    "cant_mjr": 652,
    "cant_hmbr": 439
  },
  {
    "anno": 2010,
    "destino": "Italia",
    "total": 48962,
    "cant_mjr": 28146,
    "cant_hmbr": 20816
  },
  {
    "anno": 2010,
    "destino": "Portugal",
    "total": 21323,
    "cant_mjr": 11236,
    "cant_hmbr": 10087
  },
  {
    "anno": 2010,
    "destino": "Serbia",
    "total": 38,
    "cant_mjr": 19,
    "cant_hmbr": 19
  },
  {
    "anno": 2010,
    "destino": "Eslovenia",
    "total": 32,
    "cant_mjr": 24,
    "cant_hmbr": 8
  },
  {
    "anno": 2010,
    "destino": "España",
    "total": 148147,
    "cant_mjr": 79005,
    "cant_hmbr": 69142
  },
  {
    "anno": 2010,
    "destino": "Austria",
    "total": 782,
    "cant_mjr": 477,
    "cant_hmbr": 305
  },
  {
    "anno": 2010,
    "destino": "Belgica",
    "total": 393,
    "cant_mjr": 246,
    "cant_hmbr": 147
  },
  {
    "anno": 2010,
    "destino": "Francia",
    "total": 5858,
    "cant_mjr": 3618,
    "cant_hmbr": 2240
  },
  {
    "anno": 2010,
    "destino": "Alemania",
    "total": 7362,
    "cant_mjr": 4058,
    "cant_hmbr": 3304
  },
  {
    "anno": 2010,
    "destino": "Luxemburgo",
    "total": 24,
    "cant_mjr": 12,
    "cant_hmbr": 12
  },
  {
    "anno": 2010,
    "destino": "Paises Bajos",
    "total": 3333,
    "cant_mjr": 1954,
    "cant_hmbr": 1379
  },
  {
    "anno": 2010,
    "destino": "Suiza",
    "total": 3395,
    "cant_mjr": 2137,
    "cant_hmbr": 1258
  },
  {
    "anno": 2010,
    "destino": "Antigua y Barbuda",
    "total": 37,
    "cant_mjr": 19,
    "cant_hmbr": 18
  },
  {
    "anno": 2010,
    "destino": "Aruba",
    "total": 3231,
    "cant_mjr": 1616,
    "cant_hmbr": 1615
  },
  {
    "anno": 2010,
    "destino": "Bahamas",
    "total": 28,
    "cant_mjr": 18,
    "cant_hmbr": 10
  },
  {
    "anno": 2010,
    "destino": "Caribe Holandes",
    "total": 422,
    "cant_mjr": 223,
    "cant_hmbr": 199
  },
  {
    "anno": 2010,
    "destino": "Islas Caiman",
    "total": 34,
    "cant_mjr": 16,
    "cant_hmbr": 18
  },
  {
    "anno": 2010,
    "destino": "Cuba",
    "total": 202,
    "cant_mjr": 108,
    "cant_hmbr": 94
  },
  {
    "anno": 2010,
    "destino": "Curazao",
    "total": 1645,
    "cant_mjr": 956,
    "cant_hmbr": 689
  },
  {
    "anno": 2010,
    "destino": "Dominica",
    "total": 26,
    "cant_mjr": 22,
    "cant_hmbr": 4
  },
  {
    "anno": 2010,
    "destino": "Republica Dominicana",
    "total": 5132,
    "cant_mjr": 2585,
    "cant_hmbr": 2547
  },
  {
    "anno": 2010,
    "destino": "Granada",
    "total": 337,
    "cant_mjr": 37,
    "cant_hmbr": 300
  },
  {
    "anno": 2010,
    "destino": "Haiti",
    "total": 4616,
    "cant_mjr": 2089,
    "cant_hmbr": 2527
  },
  {
    "anno": 2010,
    "destino": "Puerto Rico",
    "total": 2147,
    "cant_mjr": 1141,
    "cant_hmbr": 1006
  },
  {
    "anno": 2010,
    "destino": "Santa Lucia",
    "total": 60,
    "cant_mjr": 33,
    "cant_hmbr": 27
  },
  {
    "anno": 2010,
    "destino": "San Martin",
    "total": 40,
    "cant_mjr": 19,
    "cant_hmbr": 21
  },
  {
    "anno": 2010,
    "destino": "Trinidad y Tobago",
    "total": 1672,
    "cant_mjr": 963,
    "cant_hmbr": 709
  },
  {
    "anno": 2010,
    "destino": "Costa Rica",
    "total": 1360,
    "cant_mjr": 694,
    "cant_hmbr": 666
  },
  {
    "anno": 2010,
    "destino": "El Salvador",
    "total": 232,
    "cant_mjr": 126,
    "cant_hmbr": 106
  },
  {
    "anno": 2010,
    "destino": "Guatemala",
    "total": 246,
    "cant_mjr": 128,
    "cant_hmbr": 118
  },
  {
    "anno": 2010,
    "destino": "Honduras",
    "total": 73,
    "cant_mjr": 38,
    "cant_hmbr": 35
  },
  {
    "anno": 2010,
    "destino": "Mexico",
    "total": 10786,
    "cant_mjr": 6017,
    "cant_hmbr": 4769
  },
  {
    "anno": 2010,
    "destino": "Nicaragua",
    "total": 148,
    "cant_mjr": 73,
    "cant_hmbr": 75
  },
  {
    "anno": 2010,
    "destino": "Panama",
    "total": 8415,
    "cant_mjr": 4132,
    "cant_hmbr": 4283
  },
  {
    "anno": 2010,
    "destino": "Argentina",
    "total": 1236,
    "cant_mjr": 631,
    "cant_hmbr": 605
  },
  {
    "anno": 2010,
    "destino": "Bolivia",
    "total": 664,
    "cant_mjr": 306,
    "cant_hmbr": 358
  },
  {
    "anno": 2010,
    "destino": "Brasil",
    "total": 2844,
    "cant_mjr": 1388,
    "cant_hmbr": 1456
  },
  {
    "anno": 2010,
    "destino": "Chile",
    "total": 2514,
    "cant_mjr": 870,
    "cant_hmbr": 1644
  },
  {
    "anno": 2010,
    "destino": "Colombia",
    "total": 43511,
    "cant_mjr": 22139,
    "cant_hmbr": 21372
  },
  {
    "anno": 2010,
    "destino": "Ecuador",
    "total": 6120,
    "cant_mjr": 3275,
    "cant_hmbr": 2845
  },
  {
    "anno": 2010,
    "destino": "Guyana",
    "total": 1791,
    "cant_mjr": 863,
    "cant_hmbr": 928
  },
  {
    "anno": 2010,
    "destino": "Peru",
    "total": 2995,
    "cant_mjr": 1527,
    "cant_hmbr": 1468
  },
  {
    "anno": 2010,
    "destino": "Uruguay",
    "total": 565,
    "cant_mjr": 290,
    "cant_hmbr": 275
  },
  {
    "anno": 2010,
    "destino": "Canada",
    "total": 16005,
    "cant_mjr": 8584,
    "cant_hmbr": 7421
  },
  {
    "anno": 2010,
    "destino": "Estados Unidos",
    "total": 180905,
    "cant_mjr": 110004,
    "cant_hmbr": 70901
  },
  {
    "anno": 2010,
    "destino": "Australia",
    "total": 3360,
    "cant_mjr": 1730,
    "cant_hmbr": 1630
  },
  {
    "anno": 2015,
    "destino": "Egipto",
    "total": 38,
    "cant_mjr": 27,
    "cant_hmbr": 11
  },
  {
    "anno": 2015,
    "destino": "Sudafrica",
    "total": 469,
    "cant_mjr": 244,
    "cant_hmbr": 225
  },
  {
    "anno": 2015,
    "destino": "Chipre",
    "total": 69,
    "cant_mjr": 40,
    "cant_hmbr": 29
  },
  {
    "anno": 2015,
    "destino": "Israel",
    "total": 373,
    "cant_mjr": 207,
    "cant_hmbr": 166
  },
  {
    "anno": 2015,
    "destino": "Jordania",
    "total": 30,
    "cant_mjr": 15,
    "cant_hmbr": 15
  },
  {
    "anno": 2015,
    "destino": "Turquia",
    "total": 39,
    "cant_mjr": 22,
    "cant_hmbr": 17
  },
  {
    "anno": 2015,
    "destino": "Bulgaria",
    "total": 43,
    "cant_mjr": 25,
    "cant_hmbr": 18
  },
  {
    "anno": 2015,
    "destino": "Republica Checa",
    "total": 88,
    "cant_mjr": 39,
    "cant_hmbr": 49
  },
  {
    "anno": 2015,
    "destino": "Hungria",
    "total": 217,
    "cant_mjr": 100,
    "cant_hmbr": 117
  },
  {
    "anno": 2015,
    "destino": "Polonia",
    "total": 72,
    "cant_mjr": 9,
    "cant_hmbr": 63
  },
  {
    "anno": 2015,
    "destino": "Rumania",
    "total": 31,
    "cant_mjr": 22,
    "cant_hmbr": 9
  },
  {
    "anno": 2015,
    "destino": "Federacion Rusa",
    "total": 98,
    "cant_mjr": 51,
    "cant_hmbr": 47
  },
  {
    "anno": 2015,
    "destino": "Dinamarca",
    "total": 734,
    "cant_mjr": 427,
    "cant_hmbr": 307
  },
  {
    "anno": 2015,
    "destino": "Estonia",
    "total": 6,
    "cant_mjr": 2,
    "cant_hmbr": 4
  },
  {
    "anno": 2015,
    "destino": "Finlandia",
    "total": 220,
    "cant_mjr": 101,
    "cant_hmbr": 119
  },
  {
    "anno": 2015,
    "destino": "Islandia",
    "total": 39,
    "cant_mjr": 26,
    "cant_hmbr": 13
  },
  {
    "anno": 2015,
    "destino": "Irlanda",
    "total": 128,
    "cant_mjr": 71,
    "cant_hmbr": 57
  },
  {
    "anno": 2015,
    "destino": "Letonia",
    "total": 29,
    "cant_mjr": 11,
    "cant_hmbr": 18
  },
  {
    "anno": 2015,
    "destino": "Noruega",
    "total": 1073,
    "cant_mjr": 597,
    "cant_hmbr": 476
  },
  {
    "anno": 2015,
    "destino": "Suecia",
    "total": 1117,
    "cant_mjr": 580,
    "cant_hmbr": 537
  },
  {
    "anno": 2015,
    "destino": "Reino Unido",
    "total": 7670,
    "cant_mjr": 4186,
    "cant_hmbr": 3484
  },
  {
    "anno": 2015,
    "destino": "Croacia",
    "total": 55,
    "cant_mjr": 31,
    "cant_hmbr": 24
  },
  {
    "anno": 2015,
    "destino": "Grecia",
    "total": 1026,
    "cant_mjr": 644,
    "cant_hmbr": 382
  },
  {
    "anno": 2015,
    "destino": "Italia",
    "total": 48970,
    "cant_mjr": 28654,
    "cant_hmbr": 20316
  },
  {
    "anno": 2015,
    "destino": "Portugal",
    "total": 24174,
    "cant_mjr": 13394,
    "cant_hmbr": 10780
  },
  {
    "anno": 2015,
    "destino": "Serbia",
    "total": 37,
    "cant_mjr": 18,
    "cant_hmbr": 19
  },
  {
    "anno": 2015,
    "destino": "Eslovenia",
    "total": 31,
    "cant_mjr": 22,
    "cant_hmbr": 9
  },
  {
    "anno": 2015,
    "destino": "España",
    "total": 160478,
    "cant_mjr": 86757,
    "cant_hmbr": 73721
  },
  {
    "anno": 2015,
    "destino": "Austria",
    "total": 915,
    "cant_mjr": 555,
    "cant_hmbr": 360
  },
  {
    "anno": 2015,
    "destino": "Belgica",
    "total": 439,
    "cant_mjr": 275,
    "cant_hmbr": 164
  },
  {
    "anno": 2015,
    "destino": "Francia",
    "total": 6446,
    "cant_mjr": 4014,
    "cant_hmbr": 2432
  },
  {
    "anno": 2015,
    "destino": "Alemania",
    "total": 7671,
    "cant_mjr": 4238,
    "cant_hmbr": 3433
  },
  {
    "anno": 2015,
    "destino": "Luxemburgo",
    "total": 26,
    "cant_mjr": 13,
    "cant_hmbr": 13
  },
  {
    "anno": 2015,
    "destino": "Paises Bajos",
    "total": 3878,
    "cant_mjr": 2243,
    "cant_hmbr": 1635
  },
  {
    "anno": 2015,
    "destino": "Suiza",
    "total": 3989,
    "cant_mjr": 2336,
    "cant_hmbr": 1653
  },
  {
    "anno": 2015,
    "destino": "Antigua y Barbuda",
    "total": 39,
    "cant_mjr": 20,
    "cant_hmbr": 19
  },
  {
    "anno": 2015,
    "destino": "Aruba",
    "total": 3399,
    "cant_mjr": 1700,
    "cant_hmbr": 1699
  },
  {
    "anno": 2015,
    "destino": "Bahamas",
    "total": 30,
    "cant_mjr": 20,
    "cant_hmbr": 10
  },
  {
    "anno": 2015,
    "destino": "Caribe Holandes",
    "total": 480,
    "cant_mjr": 253,
    "cant_hmbr": 227
  },
  {
    "anno": 2015,
    "destino": "Islas Caiman",
    "total": 33,
    "cant_mjr": 15,
    "cant_hmbr": 18
  },
  {
    "anno": 2015,
    "destino": "Cuba",
    "total": 182,
    "cant_mjr": 97,
    "cant_hmbr": 85
  },
  {
    "anno": 2015,
    "destino": "Curazao",
    "total": 1787,
    "cant_mjr": 1037,
    "cant_hmbr": 750
  },
  {
    "anno": 2015,
    "destino": "Dominica",
    "total": 30,
    "cant_mjr": 26,
    "cant_hmbr": 4
  },
  {
    "anno": 2015,
    "destino": "Republica Dominicana",
    "total": 5417,
    "cant_mjr": 2730,
    "cant_hmbr": 2687
  },
  {
    "anno": 2015,
    "destino": "Granada",
    "total": 341,
    "cant_mjr": 37,
    "cant_hmbr": 304
  },
  {
    "anno": 2015,
    "destino": "Haiti",
    "total": 5198,
    "cant_mjr": 2343,
    "cant_hmbr": 2855
  },
  {
    "anno": 2015,
    "destino": "Puerto Rico",
    "total": 2302,
    "cant_mjr": 1228,
    "cant_hmbr": 1074
  },
  {
    "anno": 2015,
    "destino": "Santa Lucia",
    "total": 63,
    "cant_mjr": 34,
    "cant_hmbr": 29
  },
  {
    "anno": 2015,
    "destino": "San Martin",
    "total": 41,
    "cant_mjr": 20,
    "cant_hmbr": 21
  },
  {
    "anno": 2015,
    "destino": "Trinidad y Tobago",
    "total": 1732,
    "cant_mjr": 981,
    "cant_hmbr": 751
  },
  {
    "anno": 2015,
    "destino": "Costa Rica",
    "total": 1381,
    "cant_mjr": 709,
    "cant_hmbr": 672
  },
  {
    "anno": 2015,
    "destino": "El Salvador",
    "total": 242,
    "cant_mjr": 131,
    "cant_hmbr": 111
  },
  {
    "anno": 2015,
    "destino": "Guatemala",
    "total": 283,
    "cant_mjr": 145,
    "cant_hmbr": 138
  },
  {
    "anno": 2015,
    "destino": "Honduras",
    "total": 102,
    "cant_mjr": 53,
    "cant_hmbr": 49
  },
  {
    "anno": 2015,
    "destino": "Mexico",
    "total": 15959,
    "cant_mjr": 8782,
    "cant_hmbr": 7177
  },
  {
    "anno": 2015,
    "destino": "Nicaragua",
    "total": 159,
    "cant_mjr": 78,
    "cant_hmbr": 81
  },
  {
    "anno": 2015,
    "destino": "Panama",
    "total": 9883,
    "cant_mjr": 4864,
    "cant_hmbr": 5019
  },
  {
    "anno": 2015,
    "destino": "Argentina",
    "total": 1240,
    "cant_mjr": 634,
    "cant_hmbr": 606
  },
  {
    "anno": 2015,
    "destino": "Bolivia",
    "total": 773,
    "cant_mjr": 356,
    "cant_hmbr": 417
  },
  {
    "anno": 2015,
    "destino": "Brasil",
    "total": 3425,
    "cant_mjr": 1671,
    "cant_hmbr": 1754
  },
  {
    "anno": 2015,
    "destino": "Chile",
    "total": 4134,
    "cant_mjr": 1840,
    "cant_hmbr": 2294
  },
  {
    "anno": 2015,
    "destino": "Colombia",
    "total": 48714,
    "cant_mjr": 24546,
    "cant_hmbr": 24168
  },
  {
    "anno": 2015,
    "destino": "Ecuador",
    "total": 8901,
    "cant_mjr": 4797,
    "cant_hmbr": 4104
  },
  {
    "anno": 2015,
    "destino": "Guyana",
    "total": 2099,
    "cant_mjr": 1012,
    "cant_hmbr": 1087
  },
  {
    "anno": 2015,
    "destino": "Peru",
    "total": 3237,
    "cant_mjr": 1644,
    "cant_hmbr": 1593
  },
  {
    "anno": 2015,
    "destino": "Uruguay",
    "total": 583,
    "cant_mjr": 299,
    "cant_hmbr": 284
  },
  {
    "anno": 2015,
    "destino": "Canada",
    "total": 17898,
    "cant_mjr": 9599,
    "cant_hmbr": 8299
  },
  {
    "anno": 2015,
    "destino": "Estados Unidos",
    "total": 224986,
    "cant_mjr": 138028,
    "cant_hmbr": 86958
  },
  {
    "anno": 2015,
    "destino": "Australia",
    "total": 4895,
    "cant_mjr": 2464,
    "cant_hmbr": 2431
  },
  {
    "anno": 2017,
    "destino": "Egipto",
    "total": 32,
    "cant_mjr": 24,
    "cant_hmbr": 8
  },
  {
    "anno": 2017,
    "destino": "Sudafrica",
    "total": 496,
    "cant_mjr": 258,
    "cant_hmbr": 238
  },
  {
    "anno": 2017,
    "destino": "Chipre",
    "total": 67,
    "cant_mjr": 39,
    "cant_hmbr": 28
  },
  {
    "anno": 2017,
    "destino": "Israel",
    "total": 363,
    "cant_mjr": 201,
    "cant_hmbr": 162
  },
  {
    "anno": 2017,
    "destino": "Jordania",
    "total": 31,
    "cant_mjr": 15,
    "cant_hmbr": 16
  },
  {
    "anno": 2017,
    "destino": "Turquia",
    "total": 46,
    "cant_mjr": 25,
    "cant_hmbr": 21
  },
  {
    "anno": 2017,
    "destino": "Bulgaria",
    "total": 49,
    "cant_mjr": 28,
    "cant_hmbr": 21
  },
  {
    "anno": 2017,
    "destino": "Republica Checa",
    "total": 91,
    "cant_mjr": 40,
    "cant_hmbr": 51
  },
  {
    "anno": 2017,
    "destino": "Hungria",
    "total": 242,
    "cant_mjr": 111,
    "cant_hmbr": 131
  },
  {
    "anno": 2017,
    "destino": "Polonia",
    "total": 75,
    "cant_mjr": 9,
    "cant_hmbr": 66
  },
  {
    "anno": 2017,
    "destino": "Rumania",
    "total": 40,
    "cant_mjr": 28,
    "cant_hmbr": 12
  },
  {
    "anno": 2017,
    "destino": "Federacion Rusa",
    "total": 98,
    "cant_mjr": 51,
    "cant_hmbr": 47
  },
  {
    "anno": 2017,
    "destino": "Dinamarca",
    "total": 809,
    "cant_mjr": 466,
    "cant_hmbr": 343
  },
  {
    "anno": 2017,
    "destino": "Estonia",
    "total": 5,
    "cant_mjr": 1,
    "cant_hmbr": 4
  },
  {
    "anno": 2017,
    "destino": "Finlandia",
    "total": 240,
    "cant_mjr": 109,
    "cant_hmbr": 131
  },
  {
    "anno": 2017,
    "destino": "Islandia",
    "total": 39,
    "cant_mjr": 26,
    "cant_hmbr": 13
  },
  {
    "anno": 2017,
    "destino": "Irlanda",
    "total": 137,
    "cant_mjr": 75,
    "cant_hmbr": 62
  },
  {
    "anno": 2017,
    "destino": "Letonia",
    "total": 28,
    "cant_mjr": 10,
    "cant_hmbr": 18
  },
  {
    "anno": 2017,
    "destino": "Noruega",
    "total": 1148,
    "cant_mjr": 638,
    "cant_hmbr": 510
  },
  {
    "anno": 2017,
    "destino": "Suecia",
    "total": 1199,
    "cant_mjr": 622,
    "cant_hmbr": 577
  },
  {
    "anno": 2017,
    "destino": "Reino Unido",
    "total": 8062,
    "cant_mjr": 4423,
    "cant_hmbr": 3639
  },
  {
    "anno": 2017,
    "destino": "Croacia",
    "total": 53,
    "cant_mjr": 30,
    "cant_hmbr": 23
  },
  {
    "anno": 2017,
    "destino": "Grecia",
    "total": 1007,
    "cant_mjr": 636,
    "cant_hmbr": 371
  },
  {
    "anno": 2017,
    "destino": "Italia",
    "total": 49831,
    "cant_mjr": 29088,
    "cant_hmbr": 20743
  },
  {
    "anno": 2017,
    "destino": "Portugal",
    "total": 24603,
    "cant_mjr": 13483,
    "cant_hmbr": 11120
  },
  {
    "anno": 2017,
    "destino": "Serbia",
    "total": 36,
    "cant_mjr": 17,
    "cant_hmbr": 19
  },
  {
    "anno": 2017,
    "destino": "Eslovenia",
    "total": 45,
    "cant_mjr": 28,
    "cant_hmbr": 17
  },
  {
    "anno": 2017,
    "destino": "España",
    "total": 162000,
    "cant_mjr": 88134,
    "cant_hmbr": 73866
  },
  {
    "anno": 2017,
    "destino": "Austria",
    "total": 1007,
    "cant_mjr": 613,
    "cant_hmbr": 394
  },
  {
    "anno": 2017,
    "destino": "Belgica",
    "total": 444,
    "cant_mjr": 278,
    "cant_hmbr": 166
  },
  {
    "anno": 2017,
    "destino": "Francia",
    "total": 6433,
    "cant_mjr": 4016,
    "cant_hmbr": 2417
  },
  {
    "anno": 2017,
    "destino": "Alemania",
    "total": 9130,
    "cant_mjr": 4996,
    "cant_hmbr": 4134
  },
  {
    "anno": 2017,
    "destino": "Luxemburgo",
    "total": 26,
    "cant_mjr": 13,
    "cant_hmbr": 13
  },
  {
    "anno": 2017,
    "destino": "Paises Bajos",
    "total": 4148,
    "cant_mjr": 2395,
    "cant_hmbr": 1753
  },
  {
    "anno": 2017,
    "destino": "Suiza",
    "total": 4137,
    "cant_mjr": 2423,
    "cant_hmbr": 1714
  },
  {
    "anno": 2017,
    "destino": "Antigua y Barbuda",
    "total": 39,
    "cant_mjr": 20,
    "cant_hmbr": 19
  },
  {
    "anno": 2017,
    "destino": "Aruba",
    "total": 3421,
    "cant_mjr": 1713,
    "cant_hmbr": 1708
  },
  {
    "anno": 2017,
    "destino": "Bahamas",
    "total": 31,
    "cant_mjr": 20,
    "cant_hmbr": 11
  },
  {
    "anno": 2017,
    "destino": "Caribe Holandes",
    "total": 498,
    "cant_mjr": 262,
    "cant_hmbr": 236
  },
  {
    "anno": 2017,
    "destino": "Islas Caiman",
    "total": 33,
    "cant_mjr": 15,
    "cant_hmbr": 18
  },
  {
    "anno": 2017,
    "destino": "Cuba",
    "total": 179,
    "cant_mjr": 95,
    "cant_hmbr": 84
  },
  {
    "anno": 2017,
    "destino": "Curazao",
    "total": 1824,
    "cant_mjr": 1058,
    "cant_hmbr": 766
  },
  {
    "anno": 2017,
    "destino": "Dominica",
    "total": 30,
    "cant_mjr": 26,
    "cant_hmbr": 4
  },
  {
    "anno": 2017,
    "destino": "Republica Dominicana",
    "total": 5539,
    "cant_mjr": 2791,
    "cant_hmbr": 2748
  },
  {
    "anno": 2017,
    "destino": "Granada",
    "total": 344,
    "cant_mjr": 37,
    "cant_hmbr": 307
  },
  {
    "anno": 2017,
    "destino": "Haiti",
    "total": 5330,
    "cant_mjr": 2402,
    "cant_hmbr": 2928
  },
  {
    "anno": 2017,
    "destino": "Puerto Rico",
    "total": 2244,
    "cant_mjr": 1197,
    "cant_hmbr": 1047
  },
  {
    "anno": 2017,
    "destino": "Santa Lucia",
    "total": 63,
    "cant_mjr": 34,
    "cant_hmbr": 29
  },
  {
    "anno": 2017,
    "destino": "San Martin",
    "total": 42,
    "cant_mjr": 20,
    "cant_hmbr": 22
  },
  {
    "anno": 2017,
    "destino": "Trinidad y Tobago",
    "total": 1743,
    "cant_mjr": 987,
    "cant_hmbr": 756
  },
  {
    "anno": 2017,
    "destino": "Costa Rica",
    "total": 1389,
    "cant_mjr": 713,
    "cant_hmbr": 676
  },
  {
    "anno": 2017,
    "destino": "El Salvador",
    "total": 243,
    "cant_mjr": 131,
    "cant_hmbr": 112
  },
  {
    "anno": 2017,
    "destino": "Guatemala",
    "total": 294,
    "cant_mjr": 150,
    "cant_hmbr": 144
  },
  {
    "anno": 2017,
    "destino": "Honduras",
    "total": 103,
    "cant_mjr": 53,
    "cant_hmbr": 50
  },
  {
    "anno": 2017,
    "destino": "Mexico",
    "total": 16373,
    "cant_mjr": 9010,
    "cant_hmbr": 7363
  },
  {
    "anno": 2017,
    "destino": "Nicaragua",
    "total": 162,
    "cant_mjr": 79,
    "cant_hmbr": 83
  },
  {
    "anno": 2017,
    "destino": "Panama",
    "total": 10204,
    "cant_mjr": 5022,
    "cant_hmbr": 5182
  },
  {
    "anno": 2017,
    "destino": "Argentina",
    "total": 1286,
    "cant_mjr": 657,
    "cant_hmbr": 629
  },
  {
    "anno": 2017,
    "destino": "Bolivia",
    "total": 804,
    "cant_mjr": 370,
    "cant_hmbr": 434
  },
  {
    "anno": 2017,
    "destino": "Brasil",
    "total": 3515,
    "cant_mjr": 1715,
    "cant_hmbr": 1800
  },
  {
    "anno": 2017,
    "destino": "Chile",
    "total": 4302,
    "cant_mjr": 1914,
    "cant_hmbr": 2388
  },
  {
    "anno": 2017,
    "destino": "Colombia",
    "total": 49829,
    "cant_mjr": 25107,
    "cant_hmbr": 24722
  },
  {
    "anno": 2017,
    "destino": "Ecuador",
    "total": 9166,
    "cant_mjr": 4940,
    "cant_hmbr": 4226
  },
  {
    "anno": 2017,
    "destino": "Guyana",
    "total": 2118,
    "cant_mjr": 1021,
    "cant_hmbr": 1097
  },
  {
    "anno": 2017,
    "destino": "Peru",
    "total": 3318,
    "cant_mjr": 1685,
    "cant_hmbr": 1633
  },
  {
    "anno": 2017,
    "destino": "Uruguay",
    "total": 588,
    "cant_mjr": 301,
    "cant_hmbr": 287
  },
  {
    "anno": 2017,
    "destino": "Canada",
    "total": 18608,
    "cant_mjr": 9979,
    "cant_hmbr": 8629
  },
  {
    "anno": 2017,
    "destino": "Estados Unidos",
    "total": 232448,
    "cant_mjr": 142606,
    "cant_hmbr": 89842
  },
  {
    "anno": 2017,
    "destino": "Australia",
    "total": 5132,
    "cant_mjr": 2593,
    "cant_hmbr": 2539
  }
]

export {
	movimientos,
	
}