var continentes = [
   {
      id    : 1,
      nombre: 'América del Norte y Central',
   },
   {
      id    : 2,
      nombre: 'América del Sur'
   },
   {
      id    : 3,
      nombre: 'Europa',   
   },
   {
      id    : 4,
      nombre: 'Asia'
   },
   {
      id    : 5,
      nombre: 'África'
   },
   {
      id    : 6,
      nombre: 'Oceanía'
   },
   {
      id    : 7,
      nombre: 'Antartica'
   }
];
var paises = [  
   {  
      "name":"Afghanistan",
      "short_code": "AF",
      "long_code":"AFG",
      "latitude":33.5,
      "longitude":65.6,
      "id":0,
      "continent_id":4
   },
   {  
      "name":"Islas Åland",
      "short_code":"AX",
      "long_code": null,
      "latitude":60.2,
      "longitude":20.0,
      "id":1,
      "continent_id": 3
   },
   {  
      "name":"Albania",
      "long_code":"ALB",
      "short_code": "AL",
      "latitude":41.1,
      "longitude":19.9,
      "id":2,
      "continent_id": 3
   },
   {  
      "name":"Algeria",
      "long_code":"DZA",
      "short_code":"DZ",
      "latitude":28.2,
      "longitude":2.4,
      "id":3,
      "continent_id": 5
   },
   {  
      "name":"Samoa Americana",
      "short_code":"AS",
      "long_code": null,
      "latitude":14.2,
      "longitude":-170.6,
      "id":4,
      "continent_id": 6
   },
   {  
      "name":"Andorra",
      "short_code":"AD",
      "long_code": null,
      "latitude":42.5,
      "longitude":1.4,
      "id":5,
      "continent_id": 3
   },
   {  
      "name":"Angola",
      "short_code":"AO",
      "long_code":"AGO",
      "latitude":-11.0,
      "longitude":8.7,
      "id":6,
      "continent_id": 6
   },
   {  
      "name":"Anguilla",
      "short_code":"AI",
      "long_code":null,
      "latitude":18.390315,
      "longitude":-63.4,
      "id":7,
      "continent_id": 1
   },
   {  
      "name":"Antartica",
      "short_code": "AT",
      "long_code":"ATA",
      "latitude":-39.4,
      "longitude":-72.2,
      "id":8,
      "continent_id": 7
   },
   {  
      "name":"Antigua y Barbuda",
      "short_code":"AG",
      "long_code": null,
      "latitude":17.4,
      "longitude":-61.9,
      "id":9,
      "continent_id": 1
   },
   {  
      "name":"Argentina",
      "long_code":"ARG",
      "short_code": "AR",
      "latitude":-37.0,
      "longitude":-65.3,
      "id":10,
      "continent_id": 2
   },
   {  
      "name":"Armenia",
      "long_code":"ARM",
      "short_code": "AM",
      "latitude":40.0,
      "longitude":43.9,
      "id":11,
      "continent_id": 4
   },
   {  
      "name":"Aruba",
      "short_code":"AW",
      "long_code": null,
      "latitude":12.5,
      "longitude":-70.1,
      "id":12,
      "continent_id": 2
   },
   {  
      "name":"Australia",
      "long_code":"AUS",
      "short_code": "AU",
      "latitude":-24.4,
      "longitude":133.3,
      "id":13,
      "continent_id": 6
   },
   {  
      "name":"Austria",
      "long_code":"AUT",
      "short_code": "AT",
      "latitude":47.5,
      "longitude":14.0,
      "id":14,
      "continent_id": 3
   },
   {  
      "name":"Azerbaijan",
      "long_code":"AZE",
      "short_code": "AZ",
      "latitude":40.3,
      "longitude":47.2,
      "id":15,
      "continent_id": 3
   },
   {  
      "name":"Bahamas",
      "long_code":"BHS",
      "short_code": "BS",
      "latitude":24.5,
      "longitude":-77.2,
      "id":16,
      "continent_id": 1
   },
   {  
      "name":"Bahrain",
      "short_code":"BH",
      "long_code": null,
      "latitude":26.0,
      "longitude":50.4,
      "id":17,
      "continent_id": 4
   },
   {  
      "name":"Bangladesh",
      "long_code":"BGD",
      "short_code": "BD",
      "latitude":23.4,
      "longitude":89.8,
      "id":18,
      "continent_id": 4
   },
   {  
      "name":"Barbados",
      "short_code":"BB",
      "long_code": null,
      "latitude":13.1,
      "longitude":-59.6,
      "id":19,
      "continent_id": 1
   },
   {  
      "name":"Bielorrusia",
      "long_code":"BLR",
      "short_code": "BY",
      "latitude":53.4,
      "longitude":27.3,
      "id":20,
      "continent_id": 3
   },
   {  
      "name":"Belgica",
      "long_code":"BEL",
      "short_code": "BE",
      "latitude":50.7,
      "longitude":4.2,
      "id":21,
      "continent_id": 3
   },
   {  
      "name":"Belice",
      "long_code":"BLZ",
      "short_code": "BZ",
      "latitude":16.9,
      "longitude":-88.9,
      "id":22,
      "continent_id": 1
   },
   {  
      "name":"Benin",
      "long_code":"BEN",
      "short_code": "BJ",
      "latitude":9.4,
      "longitude":0.9,
      "id":23,
      "continent_id": 5
   },
   {  
      "name":"Bermudas",
      "short_code":"BM",
      "long_code": null,
      "latitude":32.3,
      "longitude":-64.7,
      "id":24,
      "continent_id": 1
   },
   {  
      "name":"Butan",
      "short_code":"BTN",
      "long_code": "BT",
      "latitude":27.3,
      "longitude":90.3,
      "id":25,
      "continent_id": 4
   },
   {  
      "name":"Bolivia",
      "long_code":"BOL",
      "short_code": "BO",
      "latitude":-17.1,
      "longitude":-66.0,
      "id":26,
      "continent_id": 2
   },
   {  
      "name":"Bosnia y Herzegovina",
      "long_code":"BIH",
      "short_code": "BA",
      "latitude":44.2,
      "longitude":17.8,
      "id":27,
      "continent_id": 3
   },
   {  
      "name":"Botsuana",
      "long_code":"BWA",
      "short_code":"BW",
      "latitude":-22.2,
      "longitude":20.1,
      "id":28,
      "continent_id": 5
   },
   {  
      "name":"Isla Bouvet",
      "short_code":"BV",
      "long_code": null,
      "latitude":-54.4,
      "longitude":3.28,
      "id":29,
      "continent_id": 7
   },
   {  
      "name":"Brasil",
      "long_code":"BRA",
      "short_code": "BR",
      "latitude":-11.9,
      "longitude":-55.6,
      "id":30,
      "continent_id": 2
   },
   {  
      "name":"Territorio Británico del Océano Índico",
      "short_code":"IO",
      "long_code": null,
      "latitude":-6.3,
      "longitude":70.7,
      "id":31,
      "continent_id": 4
   },
   {  
      "name":"Brunei",
      "long_ode":"BRN",
      "short_code":"BN",
      "latitude":4.4,
      "longitude":114.5,
      "id":32,
      "continent_id": 4
   },
   {  
      "name":"Bulgaria",
      "long_code":"BGR",
      "short_code":"BG",
      "latitude":42.7,
      "longitude":23.2,
      "id":33,
      "continent_id": 3
   },
   {  
      "name":"Burkina Faso",
      "long_code":"BFA",
      "short_code":"BF",
      "latitude":12.2,
      "longitude":-3.7,
      "id":34,
      "continent_id": 5
   },
   {  
      "name":"Burundi",
      "long_code":"BDI",
      "short_code": "BI",
      "latitude":-3.3,
      "longitude":29.5,
      "id":35,
      "continent_id": 5
   },
   {  
      "name":"Cambodia",
      "long_code":"KHN",
      "short_code":"KH",
      "latitude":12.1,
      "longitude":102.7,
      "id":36,
      "continent_id": 4
   },
   {  
      "name":"Camerun",
      "long_code":"CMR",
      "short_code":"CM",
      "latitude":7.3,
      "longitude":7.7,
      "id":37,
      "continent_id": 5
   },
   {  
      "name":"Canada",
      "long_code":"CAN",
      "short_code":"CA",
      "latitude":56.1,
      "longitude":-114.3,
      "id":38,
      "continent_id": 1
   },
   {  
      "name":"Cabo Verde",
      "short_code":"CV",
      "long_code": null,
      "latitude":16.0,
      "longitude":-25.1,
      "id":39,
      "continent_id": 5
   },
   {  
      "name":"Islas Caiman",
      "short_code":"KY",
      "long_code": null,
      "latitude":19.4,
      "longitude":-80.9,
      "id":40,
      "continent_id": 1
   },
   {  
      "name":"Republica Centroafricana",
      "long_code":"CAF",
      "short_code": "CF",
      "latitude":6.4,
      "longitude":17.5,
      "id":41,
      "continent_id": 5
   },
   {  
      "name":"Chad",
      "long_code":"TCD",
      "short_code": "TD",
      "id":42,
      "latitude":14.7,
      "longitude":18.7,
      "continent_id": 5
   },
   {  
      "name":"Chile",
      "long_code":"CHL",
      "short_code": "CL",
      "latitude":-35.1, 
      "longitude":-71.3,
      "id":43,
      "continent_id": 2
   },
   {  
      "name":"China",
      "long_code":"CHN",
      "short_code": "CN",
      "latitude":35.5,
      "longitude":99.7,
      "id":44,
      "continent_id": 4
   },
   {  
      "name":"Isla de Navidad",
      "short_code":"CX",
      "long_code": null,
      "latitude":-10.4,
      "longitude":105.5,
      "id":45,
      "continent_id": 4
   },
   {  
      "name":"Isla del Coco",
      "short_code":"CC",
      "long_code": null,
      "latitude":5.5,
      "longitude":-87.0,
      "id":46,
      "continent_id": 1
   },
   {  
      "name":"Colombia",
      "long_code":"COL",
      "short_code": "CO",
      "latitude":4.6,
      "longitude":-74.3,
      "id":47,
      "continent_id": 2
   },
   {  
      "name":"Comoras",
      "short_code":"KM",
      "long_code": null,
      "latitude":-11.9,
      "longitude":43.3,
      "id":48,
      "continent_id": 5
   },
   {  
      "name":"Congo",
      "long_code":"COG",
      "short_code": "CG",
      "latitude":-0.6,
      "longitude":10.3,
      "id":49,
      "continent_id": 5
   },
   {  
      "name":"Republica Democratica del Congo",
      "long_code":"COG",
      "short_code": "CD",
      "latitude":-2.9,
      "longitude":22.5,
      "id":50,
      "continent_id": 5
   },
   {  
      "name":"Islas Cook",
      "short_code":"CK",
      "long_code":null,
      "latitude":-15.4,
      "longitude":-169.9,
      "id":51,
      "continent_id": 6
   },
   {  
      "name":"Costa Rica",
      "long_code":"CRI",
      "short_code": "CR",
      "latitude":9.7,
      "longitude":-84.1,
      "id":52,
      "continent_id": 1
   },
   {  
      "name":"Costa de Marfil",
      "long_code":"CIV",
      "short_code": "CI", 
      "latitude":7.5,
      "longitude":-5.8,
      "id":53,
      "continent_id": 5
   },
   {  
      "name":"Croacia",
      "long_code":"HRV",
      "short_code":"HR",
      "latitude":44.4,
      "longitude":14.1,
      "id":54,
      "continent_id": 3
   },
   {  
      "name":"Cuba",
      "long_code":"CUB",
      "short_code": "CU",
      "latitude":21.7,
      "longitude":-80.2,
      "id":55,
      "continent_id": 1
   },
   {  
      "name":"Chipre",
      "long_code":"CYP",
      "short_code": "CY",
      "latitude":35.0,
      "longitude":32.9,
      "id":56,
      "continent_id": 3
   },
   {  
      "name":"Republica Checa",
      "long_code":"CZE",
      "short_code": "CZ",
      "latitude":49.7,
      "longitude":13.2,
      "id":57,
      "continent_id": 3
   },
   {  
      "name":"Dinamarca",
      "long_code":"DNK",
      "short_code": "DK",
      "latitude":55.8,
      "longitude":9.2,
      "id":58,
      "continent_id": 3
   },
   {  
      "name":"Yibuti",
      "long_code":"DJI",
      "short_code":"DJ",
      "latitude":11.7,
      "longitude":42.2,
      "id":59,
      "continent_id": 5
   },
   {  
      "name":"Dominica",
      "short_code":"DM",
      "long_code":null,
      "latitude":15.4,
      "longitude":-61.4,
      "id":60,
      "continent_id": 1
   },
   {  
      "name":"Republica Dominicana",
      "long_code":"DOM",
      "short_code":"DO",
      "latitude":18.7,
      "longitude":-71.1,
      "id":61,
      "continent_id": 1
   },
   {  
      "name":"Ecuador",
      "long_code":"ECU",
      "short_code": "EC",
      "latitude":-1.6,
      "longitude":-79.2,
      "id":62,
      "continent_id": 2
   },
   {  
      "name":"Egipto",
      "long_code":"EGY",
      "short_code":"EG",
      "latitude":26.8,
      "longitude":26.3,
      "id":63,
      "continent_id": 5
   },
   {  
      "name":"El Salvador",
      "long_code":"SLV",
      "short_code":"SV",
      "latitude":13.7,
      "longitude":-89.3,
      "id":64,
      "continent_id": 1
   },
   {  
      "name":"Guinea Ecuatorial",
      "long_code":"GNQ",
      "short_code": "GQ",
      "latitude":1.5,
      "longitude":9.9,
      "id":65,
      "continent_id": 5
   },
   {  
      "name":"Eritrea",
      "long_code":"ERI",
      "short_code":"ER",
      "latitude":15.7,
      "longitude":38.1,
      "id":66,
      "continent_id": 5
   },
   {  
      "name":"Estonia",
      "long_code":"EST",
      "short_code":"EE",
      "latitude":58.7,
      "longitude":24.0,
      "id":67,
      "continent_id": 3
   },
   {  
      "name":"Etiopia",
      "long_code":"ETH",
      "short_code":"ET",
      "latitude":9.1,
      "longitude":35.9,
      "id":68,
      "continent_id": 5,
   },
   {  
      "name":"Islas Malvinas",
      "long_code":"FLK",
      "short_code":"FK",
      "latitude":-51.7,
      "longitude":-60.4,
      "id":69,
      "continent_id": 2
   },
   {  
      "name":"Islas Feroe",
      "short_code":"FO",
      "long_code":null,
      "latitude":61.9,
      "longitude":-7.5,
      "id":70,
      "continent_id": 3
   },
   {  
      "name":"Fiji",
      "long_code":"FJI",
      "short_code":"FJ",
      "latitude":-17.8,
      "longitude":178.0,
      "id":71,
      "continent_id": 6
   },
   {  
      "name":"Finlandia",
      "long_code":"FIN",
      "short_code":"FI",
      "latitude":64.6,
      "longitude":17.0,
      "id":72,
      "continent_id": 3
   },
   {  
      "name":"Francia",
      "long_code":"FRA",
      "short_code":"FR",
      "latitude":46.7,
      "longitude":0.8,
      "id":73,
      "continent_id": 3
   },
   {  
      "name":"Guayana Francesa",
      "short_code":"GF",
      "long_code":null,
      "latitude":3.9,
      "longitude":-54.8,
      "id":74,
      "continent_id": 2
   },
   {  
      "name":"Polinesia Francesa",
      "short_code":"PF",
      "long_code":null,
      "latitude":-17.1,
      "longitude":-151.0,
      "id":75,
      "continent_id": 6
   },
   {  
      "name":"Territorios Australes Franceses",
      "long_code":"ATF",
      "short_code":"TF",
      "latitude":-47.5,
      "longitude":51.2,
      "id":76,
      "continent_id": 7
   },
   {  
      "name":"Gabon",
      "long_code":"GAB",
      "short_code":"GA",
      "latitude":-0.9,
      "longitude":9.2,
      "id":77,
      "continent_id": 5
   },
   {  
      "name":"Gambia",
      "long_code":"GMB",
      "short_code":"GM",
      "latitude":13.4,
      "longitude":-15.6,
      "id":78,
      "continent_id": 5
   },
   {  
      "name":"Georgia",
      "long_code":"GEO",
      "short_code":"GE",
      "latitude":42.1,
      "longitude":42.7,
      "id":79,
      "continent_id": 3
   },
   {  
      "name":"Alemania",
      "long_code":"DEU",
      "short_code":"DE",
      "latitude":50.8,
      "longitude":8.1,
      "id":80,
      "continent_id": 3
   },
   {  
      "name":"Ghana",
      "long_code":"GHA",
      "short_code":"GH",
      "latitude":7.9,
      "longitude":-2.9,
      "id":81,
      "continent_id": 5
   },
   {  
      "name":"Gibraltar",
      "short_code":"GI",
      "long_code":null,
      "latitude":36.1,
      "longitude":-5.3,
      "id":82,
      "continent_id": 3
   },
   {  
      "name":"Grecia",
      "long_code":"GRC",
      "short_code":"GR",
      "latitude":38.8,
      "longitude":22.2,
      "id":83,
      "continent_id": 3
   },
   {  
      "name":"Groenlandia",
      "long_code":"GRL",
      "short_code":"GL",
      "latitude":72.7,
      "longitude":-43.8,
      "id":84,
      "continent_id": 1
   },
   {  
      "name":"Granada",
      "short_code":"GD",
      "long_code":null,
      "latitude":12.2,
      "longitude":-61.8,
      "id":85,
      "continent_id": 1
   },
   {  
      "name":"Guadalupe",
      "short_code":"GP",
      "long_code":null,
      "latitude":16.1,
      "longitude":-61.9,
      "id":86,
      "continent_id": 1
   },
   {  
      "name":"Guam",
      "short_code":"GU",
      "long_code":null,
      "latitude":13.4,
      "longitude":144.5,
      "id":87,
      "continent_id": 6
   },
   {  
      "name":"Guatemala",
      "long_code":"GTM",
      "short_code":"GT",
      "latitude":15.3,
      "longitude":-90.9,
      "id":88,
      "continent_id": 1
   },
   {  
      "name":"Guernsey",
      "short_code":"GG",
      "long_code":null,
      "latitude":49.4,
      "longitude":-2.5,
      "id":89,
      "continent_id": 3
   },
   {  
      "name":"Guinea",
      "long_code":"GIN",
      "short_code":"GN",
      "latitude":10.9,
      "longitude":-11.9,
      "id":90,
      "continent_id": 5
   },
   {  
      "name":"Guinea-Bisau",
      "long_code":"GNB",
      "short_code":"GW",
      "latitude":12.0,
      "longitude":-15.3,
      "id":91,
      "continent_id": 5
   },
   {  
      "name":"Guyana",
      "long_code":"GUY",
      "short_code":"GY",
      "latitude":4.9,
      "longitude":-63.4,
      "id":92,
      "continent_id": 2
   },
   {  
      "name":"Haiti",
      "long_code":"HTI",
      "short_code":"HT",
      "latitude":19.0,
      "longitude":-73.5,
      "id":93,
      "continent_id": 1
   },
   {  
      "name":"Islas Heard y McDonald",
      "short_code":"HM",
      "long_code":null,
      "latitude":-53.0,
      "longitude":73.3,
      "id":94,
      "continent_id": 7
   },
   {  
      "name":"Ciudad del Vaticano",
      "short_code":"VA",
      "long_code":null,
      "latitude":41.9,
      "longitude":12.4,
      "id":95,
      "continent_id": 3
   },
   {  
      "name":"Honduras",
      "long_code":"HND",
      "short_code":"HN",
      "latitude":14.8,
      "longitude":-87.4,
      "id":96,
      "continent_id": 1
   },
   {  
      "name":"Hong Kong",
      "short_code":"HK",
      "long_code":null,
      "id":97,
      "latitude":22.3,
      "longitude":114.1,
      "continent_id": 4
   },
   {  
      "name":"Hungria",
      "long_code":"HUN",
      "short_code":"HU",
      "latitude":47.0,
      "longitude":18.4,
      "id":98,
      "continent_id": 3
   },
   {  
      "name":"Islandia",
      "long_code":"ISL",
      "short_code":"IS",
      "latitude":64.7,
      "longitude":-20.3,
      "id":99,
      "continent_id": 3
   },
   {  
      "name":"India",
      "long_code":"IND",
      "short_code":"IN",
      "latitude":22.6,
      "longitude":78.4,
      "id":100,
      "continent_id": 4
   },
   {  
      "name":"Indonesia",
      "long_code":"IDN",
      "short_code":"ID",
      "id":101,
      "latitude":-4.2,
      "longitude":114.1,
      "continent_id": 6
   },
   {  
      "name":"Iran",
      "long_code":"IRN",
      "short_code":"IR",
      "latitude":33.5,
      "longitude":52.2,
      "id":102,
      "continent_id": 4
   },
   {  
      "name":"Iraq",
      "long_code":"IRQ",
      "short_code":"IQ",
      "latitude":33.2,
      "longitude":43.6,
      "id":103,
      "continent_id": 4
   },
   {  
      "name":"Irlanda",
      "long_code":"IRL",
      "short_code":"IE",
      "latitude":53.1,
      "longitude":-9.0,
      "id":104,
      "continent_id": 3
   },
   {  
      "name":"Isla de Man",
      "short_code":"IM",
      "long_code":null,
      "latitude":54.2,
      "longitude":-4.7,
      "id":105,
      "continent_id": 3
   },
   {  
      "name":"Israel",
      "long_code":"ISL",
      "short_code":"IL",
      "latitude":31.2,
      "longitude":34.3,
      "id":106,
      "continent_id": 4
   },
   {  
      "name":"Italia",
      "long_code":"ITA",
      "short_code":"IT",
      "latitude":42.4,
      "longitude":12.8,
      "id":107,
      "continent_id": 3
   },
   {  
      "name":"Jamaica",
      "long_code":"JAM",
      "short_code":"JM",
      "latitude":18.1,
      "longitude":-77.4,
      "id":108,
      "continent_id": 1
   },
   {  
      "name":"Japon",
      "long_code":"JPN",
      "short_code":"JP",
      "id":109,
      "latitude":35.5,
      "longitude":136.7,
      "continent_id": 4
   },
   {  
      "name":"Jersey",
      "short_code":"JE",
      "long_code":null,
      "latitude":49.2,
      "longitude":-2.2,
      "id":110,
      "continent_id": 3
   },
   {  
      "name":"Jordania",
      "long_code":"JOR",
      "short_code":"JO",
      "latitude":31.2,
      "longitude":34.8,
      "id":111,
      "continent_id": 4
   },
   {  
      "name":"Kazajistan",
      "long_code":"KAZ",
      "short_code":"KZ",
      "latitude":48.7,
      "longitude":62.1,
      "id":112,
      "continent_id": 3
   },
   {  
      "name":"Kenya",
      "long_code":"KEN",
      "short_code":"KE",
      "latitude":0.2,
      "longitude":36.2,
      "id":113,
      "continent_id": 5
   },
   {  
      "name":"Kiribati",
      "short_code":"KI",
      "long_code":null,
      "latitude":-3.5,
      "longitude":173.3,
      "id":114,
      "continent_id": 6
   },
   {  
      "name":"Corea del Norte",
      "long_code":"PRK",
      "short_code":"KP",
      "latitude":39.8,
      "longitude":125.4,
      "id":115,
      "continent_id": 4
   },
   {  
      "name":"Corea del Sur",
      "long_code":"KOR",
      "short_code":"KR",
      "latitude":36.3,
      "longitude":127.4,
      "id":116,
      "continent_id": 4
   },
   {  
      "name":"Kuwait",
      "long_code":"KWT",
      "short_code":"KW",
      "latitude":29.3,
      "longitude":47.4,
      "id":117,
      "continent_id": 4
   },
   {  
      "name":"Kirguistan",
      "long_code":"KGZ",
      "short_code":"KG",
      "id":118,
      "latitude":41.5,
      "longitude":74.1,
      "continent_id": 4
   },
   {  
      "name":"Laos",
      "long_code":"LAO",
      "short_code":"LA",
      "latitude":19.2,
      "longitude":102.6,
      "id":119,
      "continent_id": 4
   },
   {  
      "name":"Letonia",
      "long_code":"LVA",
      "short_code":"LV",
      "latitude":56.9,
      "longitude":24.0,
      "id":120,
      "continent_id": 3
   },
   {  
      "name":"Libano",
      "long_code":"LBN",
      "short_code":"LB",
      "latitude":33.9,
      "longitude":35.5,
      "id":121,
      "continent_id": 4
   },
   {  
      "name":"Lesoto",
      "long_code":"LSO",
      "short_code":"LS",
      "latitude":-29.5,
      "longitude":27.8,
      "id":122,
      "continent_id": 5
   },
   {  
      "name":"Liberia",
      "long_code":"LBR",
      "short_code":"LR",
      "latitude":6.4,
      "longitude":-10.1,
      "id":123,
      "continent_id": 5
   },
   {  
      "name":"Libia",
      "long_code":"LBY",
      "short_code":"LY",
      "latitude":27.3,
      "longitude":15.0,
      "id":124,
      "continent_id": 5
   },
   {  
      "name":"Liechtenstein",
      "short_code":"LI",
      "long_code":null,
      "latitude":47.1,
      "longitude":9.5,
      "id":125,
      "continent_id": 3
   },
   {  
      "name":"Lituania",
      "long_code":"LTU",
      "short_code":"LT",
      "id":126,
      "latitude":55.4,
      "longitude":23.5,
      "continent_id": 3
   },
   {  
      "name":"Luxemburgo",
      "long_code":"LUX",
      "short_code":"LU",
      "latitude":49.7,
      "longitude":6.0,
      "id":127,
      "continent_id": 3
   },
   {  
      "name":"Macao",
      "short_code":"MO",
      "long_code":null,
      "latitude":22.1,
      "longitude":113.5,
      "id":128,
      "continent_id": 5
   },
   {  
      "name":"Macedonia",
      "long_code":"MKD",
      "short_code":"MK",
      "latitude":41.5,
      "longitude":21.4,
      "id":129,
      "continent_id": 3
   },
   {  
      "name":"Madagascar",
      "long_code":"MDG",
      "short_code":"MG",
      "latitude":-19.4,
      "longitude":43.4,
      "id":130,
      "continent_id": 5
   },
   {  
      "name":"Malaui",
      "long_code":"MWI",
      "short_code":"MW",
      "latitude":-12.9,
      "longitude":33.0,
      "id":131,
      "continent_id": 5
   },
   {  
      "name":"Malasia",
      "long_code":"MYS",
      "short_code":"MY",
      "latitude":3.7,
      "longitude":102.2,
      "id":132,
      "continent_id": 4
   },
   {  
      "name":"Maldivas",
      "short_code":"MV",
      "long_code":null,
      "latitude":3.1,
      "longitude":68.7,
      "id":133,
      "continent_id": 4
   },
   {  
      "name":"Mali",
      "long_code":"MLI",
      "short_code":"ML",
      "latitude":16.3,
      "longitude":-6.4,
      "id":134,
      "continent_id": 5
   },
   {  
      "name":"Malta",
      "short_code":"MT",
      "long_code":null,
      "latitude":35.9,
      "longitude":14.3,
      "id":135,
      "continent_id": 3
   },
   {  
      "name":"Islas Marshall",
      "short_code":"MH",
      "long_code":null,
      "latitude":8.2,
      "longitude":166.1,
      "id":136,
      "continent_id": 6
   },
   {  
      "name":"Martinica",
      "short_code":"MQ",
      "long_code":null,
      "latitude":14.6,
      "longitude":-61.1,
      "id":137,
      "continent_id": 1
   },
   {  
      "name":"Mauritania",
      "long_code":"MRT",
      "short_code":"MR",
      "latitude":19.7,
      "longitude":-12.7,
      "id":138,
      "continent_id": 5
   },
   {  
      "name":"Mauricio",
      "short_code":"MU",
      "long_code":null,
      "latitude":-20.2,
      "longitude":57.3,
      "id":139,
      "continent_id": 5
   },
   {  
      "name":"Mayotte",
      "short_code":"YT",
      "long_code":null,
      "latitude":-12.8,
      "longitude":45.0,
      "id":140,
      "continent_id": 5
   },
   {  
      "name":"Mexico",
      "long_code":"MEX",
      "short_code":"MX",
      "latitude":21.7,
      "longitude":-102.1,
      "id":141,
      "continent_id": 1
   },
   {  
      "name":"Estados Federados de Micronesia",
      "short_code":"FM",
      "long_code":null,
      "latitude":5.1,
      "longitude":141.1,
      "id":142,
      "continent_id": 6
   },
   {  
      "name":"Republica de Moldavia",
      "long_code":"MDA",
      "short_code":"MD",
      "latitude":46.9,
      "longitude":26.1,
      "id":143,
      "continent_id": 3
   },
   {  
      "name":"Monaco",
      "short_code":"MC",
      "long_code":null,
      "latitude":43.7,
      "longitude":7.4,
      "id":144,
      "continent_id": 3
   },
   {  
      "name":"Mongolia",
      "short_code":"MN",
      "long_code":null,
      "latitude":46.7,
      "longitude":101.2,
      "id":145,
      "continent_id": 4
   },
   {  
      "name":"Montserrat",
      "short_code":"MS",
      "long_code":null,
      "latitude":16.7,
      "longitude":-62.2,
      "id":146,
      "continent_id": 1
   },
   {  
      "name":"Marruecos",
      "long_code":"MAR",
      "short_code":"MA",
      "latitude":31.9,
      "longitude":-7.1,
      "id":147,
      "continent_id": 5,
   },
   {  
      "name":"Mozambique",
      "long_code":"MOZ",
      "short_code":"MZ",
      "latitude":-18.4,
      "longitude":26.7,
      "id":148,
      "continent_id": 5
   },
   {  
      "name":"Myanmar",
      "long_code":"MMR",
      "short_code":"MM",
      "latitude":20.5,
      "longitude":94.1,
      "id":149,
      "continent_id": 4
   },
   {  
      "name":"Namibia",
      "long_code":"NAM",
      "short_code":"NA",
      "latitude":-22.6402246,
      "longitude":16.2,
      "id":150,
      "continent_id": 5
   },
   {  
      "name":"Nauru",
      "short_code":"NR",
      "long_code":null,
      "latitude":-0.5,
      "longitude":166.9,
      "id":151,
      "continent_id": 6
   },
   {  
      "name":"Nepal",
      "long_code":"NPL",
      "short_code":"NP",
      "latitude":28.1,
      "longitude":83.9,
      "id":152,
      "continent_id": 4
   },
   {  
      "name":"Paises Bajos",
      "long_code":"NLD",
      "short_code":"NL",
      "latitude":52.1,
      "longitude":4.2,
      "id":153,
      "continent_id": 3
   },
   {  
      "name":"Caribe Holandes",
      "short_code":"AN",
      "long_code":null,
      "latitude":12.1,
      "longitude":-68.2,
      "id":154,
      "continent_id": 2
   },
   {  
      "name":"Nueva Caledonia",
      "long_code":"NCL",
      "short_code":"NC",
      "latitude":-21.3,
      "longitude":165.3,
      "id":155,
      "continent_id": 6
   },
   {  
      "name":"Nueva Zelanda",
      "long_code":"NZL",
      "short_code":"NZ",
      "latitude":-42.2,
      "longitude":171.8,
      "id":156,
      "continent_id": 6
   },
   {  
      "name":"Nicaragua",
      "long_code":"NIC",
      "short_code":"NI",
      "latitude":12.8,
      "longitude":-85.2,
      "id":157,
      "continent_id": 1
   },
   {  
      "name":"Niger",
      "long_code":"NER",
      "short_code":"NE",
      "latitude":17.0,
      "longitude":6.4,
      "id":158,
      "continent_id": 5
   },
   {  
      "name":"Nigeria",
      "long_code":"NGA",
      "short_code":"NG",
      "latitude":9.0,
      "longitude":7.02,
      "id":159,
      "continent_id": 5
   },
   {  
      "name":"Niue",
      "short_code":"NU",
      "long_code":null,
      "latitude":-19.0,
      "longitude":-170.0,
      "id":160,
      "continent_id": 6
   },
   {  
      "name":"Isla Norfolk",
      "short_code":"NF",
      "long_code":null,
      "latitude":-29.0,
      "longitude":167.9,
      "id":161,
      "continent_id": 6
   },
   {  
      "name":"Islas Marianas del Norte",
      "short_code":"MP",
      "long_code":null,
      "latitude":17.2,
      "longitude":140.9,
      "id":162,
      "continent_id": 6
   },
   {  
      "name":"Noruega",
      "long_code":"NOR",
      "short_code":"NO",
      "latitude":61.4,
      "longitude":9.1,
      "id":163,
      "continent_id": 3
   },
   {  
      "name":"Oman",
      "long_code":"OMN",
      "short_code":"OM",
      "latitude":20.4,
      "longitude":55.2,
      "id":164,
      "continent_id": 4
   },
   {  
      "name":"Pakistan",
      "long_code":"PAK",
      "short_code":"PK",
      "latitude":28.8,
      "longitude":68.0,
      "id":165,
      "continent_id": 4
   },
   {  
      "name":"Palau",
      "short_code":"PW",
      "long_code":null,
      "latitude":7.3,
      "longitude":134.4,
      "id":166,
      "continent_id": 6
   },
   {  
      "name":"Territorios Palestinos",
      "short_code":"PS",
      "long_code":null,
      "latitude":31.8,
      "longitude":34.3,
      "id":167,
      "continent_id": 4
   },
   {  
      "name":"Panama",
      "long_code":"PAN",
      "short_code":"PA",
      "latitude":8.5,
      "longitude":-80.5,
      "id":168,
      "continent_id": 1
   },
   {  
      "name":"Papua Nueva Guinea",
      "long_code":"PNG",
      "short_code":"PG",
      "latitude":-6.5,
      "longitude":142.9,
      "id":169,
      "continent_id": 6
   },
   {  
      "name":"Paraguay",
      "long_code":"PRY",
      "short_code":"PY",
      "latitude":-23.1,
      "longitude":-58.7,
      "id":170,
      "continent_id": 2
   },
   {  
      "name":"Peru",
      "long_code":"PER",
      "short_code":"PE",
      "latitude":-10.3,
      "longitude":-75.8,
      "id":171,
      "continent_id": 2
   },
   {  
      "name":"Filipinas",
      "long_code":"PHL",
      "short_code":"PH",
      "latitude":11.6,
      "longitude":120.7,
      "id":172,
      "continent_id": 4
   },
   {  
      "name":"Islas Pitcairn",
      "short_code":"PN",
      "long_code":null,
      "latitude":-24.4,
      "longitude":-129.9,
      "id":173,
      "continent_id": 6
   },
   {  
      "name":"Polonia",
      "long_code":"POL",
      "short_code":"PL",
      "latitude":51.8,
      "longitude":14.6,
      "id":174,
      "continent_id": 3
   },
   {  
      "name":"Portugal",
      "long_code":"PRT",
      "short_code":"PT",
      "latitude":39.4,
      "longitude":-9.6,
      "id":175,
      "continent_id": 3
   },
   {  
      "name":"Puerto Rico",
      "long_code":"PRI",
      "short_code":"PR",
      "latitude":18.2,
      "longitude":-66.6,
      "id":176,
      "continent_id": 1
   },
   {  
      "name":"Qatar",
      "long_code":"QAT",
      "short_code":"QA",
      "latitude":25.2,
      "longitude":50.9,
      "id":177,
      "continent_id": 4
   },
   {  
      "name":"Reunion",
      "short_code":"RE",
      "long_code":null,
      "latitude":-21.1,
      "longitude":55.2,
      "id":178,
      "continent_id": 5
   },
   {  
      "name":"Rumania",
      "long_code":"ROU",
      "short_code":"RO",
      "latitude":45.9,
      "longitude":23.3,
      "id":179,
      "continent_id": 3
   },
   {  
      "name":"Federacion Rusa",
      "long_code":"RUS",
      "short_code":"RU",
      "latitude":62.3,
      "longitude":98.4,
      "id":180,
      "continent_id": 4
   },
   {  
      "name":"Ruanda",
      "long_code":"RWA",
      "short_code":"RW",
      "latitude":-1.9,
      "longitude":29.4,
      "id":181,
      "continent_id": 5
   },
   {  
      "name":"Santa Helena",
      "short_code":"SH",
      "long_code":null,
      "latitude":-22.9,
      "longitude":-28.5,
      "id":182,
      "continent_id": 5
   },
   {  
      "name":"San Cristóbal y Nieves",
      "short_code":"KN",
      "long_code":null,
      "latitude":17.2,
      "longitude":-62.8,
      "id":183,
      "continent_id": 1
   },
   {  
      "name":"Santa Lucia",
      "short_code":"LC",
      "long_code":null,
      "latitude":13.9,
      "longitude":-61.2,
      "id":184,
      "continent_id": 1
   },
   {  
      "name":"San Pedro y Miquelón",
      "short_code":"PM",
      "long_code":null,
      "latitude":46.9,
      "longitude":-56.5,
      "id":185,
      "continent_id": 1
   },
   {  
      "name":"San Vicente y las Granadinas",
      "short_code":"VC",
      "long_code":null,
      "latitude":12.9,
      "longitude":-61.8,
      "id":186,
      "continent_id": 1
   },
   {  
      "name":"Samoa",
      "short_code":"WS",
      "long_code":null,
      "latitude":-13.7,
      "longitude":-172.6,
      "id":187,
      "continent_id": 6
   },
   {  
      "name":"San Marino",
      "short_code":"SM",
      "long_code":null,
      "latitude":43.9,
      "longitude":12.3,
      "id":188,
      "continent_id": 3
   },
   {  
      "name":"Santo Tomé y Príncipe",
      "short_code":"ST",
      "long_code":null,
      "latitude":0.9,
      "longitude":5.8,
      "id":189,
      "continent_id": 5
   },
   {  
      "name":"Arabia Saudita",
      "long_code":"SAU",
      "short_code":"SA",
      "latitude":23.8,
      "longitude":42.6,
      "id":190,
      "continent_id": 4
   },
   {  
      "name":"Senegal",
      "long_code":"SEN",
      "short_code":"SN",
      "latitude":14.2,
      "longitude":-15.2,
      "id":191,
      "continent_id": 5
   },
   {  
      "name":"Montenegro",
      "long_code":"MNE",
      "short_code":"ME",
      "latitude":42.6,
      "longitude":18.2,
      "id":192,
      "continent_id": 3
   },
   {  
      "name":"Seychelles",
      "short_code":"SC",
      "long_code":null,
      "latitude":-5.0,
      "longitude":54.0,
      "id":193,
      "continent_id": 5
   },
   {  
      "name":"Sierra Leona",
      "long_code":"SLE",
      "short_code":"SL",
      "latitude":8.5,
      "longitude":-12.3,
      "id":194,
      "continent_id": 5
   },
   {  
      "name":"Singapur",
      "short_code":"SG",
      "long_code":null,
      "latitude":1.3,
      "longitude":103.7,
      "id":195,
      "continent_id": 4
   },
   {  
      "name":"Eslovaquia",
      "long_code":"SVK",
      "short_code":"SK",
      "latitude":48.6,
      "longitude":17.4,
      "id":196,
      "continent_id": 3
   },
   {  
      "name":"Eslovenia",
      "long_code":"SVN",
      "short_code":"SI",
      "latitude":46.1,
      "longitude":13.8,
      "id":197,
      "continent_id": 3
   },
   {  
      "name":"Islas Solomon",
      "long_code":"SLB",
      "short_code":"SB",
      "latitude":-9.3,
      "longitude":157.0,
      "id":198,
      "continent_id": 6
   },
   {  
      "name":"Somalia",
      "long_code":"SOM",
      "short_code":"SO",
      "latitude":3.1,
      "longitude":44.7,
      "id":199,
      "continent_id": 5
   },
   {  
      "name":"Sudafrica",
      "long_code":"ZAF",
      "short_code":"ZA",
      "latitude":-31.4,
      "longitude":23.1,
      "id":200,
      "continent_id": 5
   },
   {  
      "name":"Islas Georgias del Sur y Sandwich del Sur",
      "short_code":"GS",
      "long_code":null,
      "latitude":-56.7,
      "longitude":-36.4,
      "id":201,
      "continent_id": 2
   },
   {  
      "name":"España",
      "long_code":"ESP",
      "short_code":"ES",
      "latitude":40.1,
      "longitude":-7.4,
      "id":202,
      "continent_id": 3
   },
   {  
      "name":"Sri Lanka",
      "long_code":"LKA",
      "short_code":"LK",
      "latitude":7.7,
      "longitude":79.5,
      "id":203,
      "continent_id": 4
   },
   {  
      "name":"Sudan",
      "short_code":"SD",
      "long_code":"SDN",
      "latitude":15.7,
      "longitude":26.5,
      "id":204,
      "continent_id": 5
   },
   {  
      "name":"Suriname",
      "long_code":"SUR",
      "short_code":"SR",
      "latitude":4.2,
      "longitude":-56.2,
      "id":205,
      "continent_id": 2
   },
   {  
      "name":"Svalbard y Jan Mayen",
      "short_code":"SJ",
      "long_code":null,
      "latitude":76.7,
      "longitude":11.2,
      "id":206,
      "continent_id": 3
   },
   {  
      "name":"Esuatini",
      "long_code":"SWZ",
      "short_code":"SZ",
      "latitude":-26.5,
      "longitude":31.2,
      "id":207,
      "continent_id": 5
   },
   {  
      "name":"Suecia",
      "long_code":"SWE",
      "short_code":"SE",
      "latitude":62.0,
      "longitude":14.7,
      "id":208,
      "continent_id": 3
   },
   {  
      "name":"Suiza",
      "long_code":"CHE",
      "short_code":"CH",
      "latitude":46.8,
      "longitude":7.1,
      "id":209,
      "continent_id": 3
   },
   {  
      "name":"Siria",
      "long_code":"SYR",
      "short_code":"SY",
      "latitude":34.9,
      "longitude":38.2,
      "id":210,
      "continent_id": 4
   },
   {  
      "name":"Taiwan",
      "long_code":"TWN",
      "short_code":"TW",
      "latitude":23.6,
      "longitude":120.7,
      "id":211,
      "continent_id": 4
   },
   {  
      "name":"Tajikistan",
      "long_code":"TJK",
      "short_code":"TJ",
      "latitude":38.9,
      "longitude":70.7,
      "id":212,
      "continent_id": 4
   },
   {  
      "name":"Tanzania",
      "long_code":"TZA",
      "short_code":"TZ",
      "latitude":-6.3,
      "longitude":33.9,
      "id":213,
      "continent_id": 5
   },
   {  
      "name":"Tailandia",
      "long_code":"THA",
      "short_code":"TH",
      "latitude":14.8,
      "longitude":100.2,
      "id":214,
      "continent_id": 4
   },
   {  
      "name":"Timor Oriental",
      "long_code":"TLS",
      "short_code":"TL",
      "latitude":-8.8,
      "longitude":125.3,
      "id":215,
      "continent_id": 4
   },
   {  
      "name":"Togo",
      "long_code":"TGO",
      "short_code":"TG",
      "latitude":8.4,
      "longitude":0.8,
      "id":216,
      "continent_id": 5
   },
   {  
      "name":"Tokelau",
      "short_code":"TK",
      "long_code":null,
      "latitude":-8.9,
      "longitude":-172.4,
      "id":217,
      "continent_id": 6
   },
   {  
      "name":"Tonga",
      "short_code":"TO",
      "long_code":null,
      "latitude":-20.5,
      "longitude":-175.3,
      "id":218,
      "continent_id": 6
   },
   {  
      "name":"Trinidad y Tobago",
      "long_code":"TTO",
      "short_code":"TT",
      "latitude":10.7,
      "longitude":-61.1,
      "id":219,
      "continent_id": 2
   },
   {  
      "name":"Tunez",
      "long_code":"TUN",
      "short_code":"TN",
      "latitude":34.3,
      "longitude":9.3,
      "id":220,
      "continent_id": 5
   },
   {  
      "name":"Turquia",
      "long_code":"TUR",
      "short_code":"TR",
      "id":221,
      "latitude":38.9,
      "longitude":34.3,
      "continent_id": 3
   },
   {  
      "name":"Turkmenistan",
      "long_code":"TKM",
      "short_code":"TM",
      "id":222,
      "latitude":39.1,
      "longitude":57.6,
      "continent_id": 4
   },
   {  
      "name":"Islas Turcas y Caicos",
      "short_code":"TC",
      "long_code":null,
      "latitude":21.7,
      "longitude":-71.7,
      "id":223,
      "continent_id": 1
   },
   {  
      "name":"Tuvalu",
      "short_code":"TV",
      "long_code":null,
      "latitude":-8.4,
      "longitude":178.9,
      "id":224,
      "continent_id": 6
   },
   {  
      "name":"Uganda",
      "long_code":"UGA",
      "short_code":"UG",
      "latitude":1.4,
      "longitude":32.0,
      "id":225,
      "continent_id": 5
   },
   {  
      "name":"Ucrania",
      "long_code":"UKR",
      "short_code":"UA",
      "latitude":48.7,
      "longitude":30.1,
      "id":226,
      "continent_id": 3
   },
   {  
      "name":"Emiratos Arabes Unidos",
      "long_code":"ARE",
      "short_code":"AE",
      "latitude":23.8,
      "longitude":53.6,
      "id":227,
      "continent_id": 4
   },
   {  
      "name":"Reino Unido",
      "short_code":"GB",
      "long_code":null,
      "latitude":54.1,
      "longitude":-6.4,
      "id":228,
      "continent_id": 3
   },
   {  
      "name":"Estados Unidos",
      "long_code":"USA",
      "short_code":"US",
      "latitude":39.5,
      "longitude":-100.2,
      "id":229,
      "continent_id": 1
   },
   {  
      "name":"Islas Ultramarinas Menores de Estados Unidos",
      "short_code":"UM",
      "long_code":null,
      "latitude":13.3,
      "longitude":164.8,
      "id":230,
      "continent_id": 1
   },
   {  
      "name":"Uruguay",
      "long_code":"URY",
      "short_code":"UY",
      "latitude":-32.8,
      "longitude":-56.1,
      "id":231,
      "continent_id": 2
   },
   {  
      "name":"Uzbekistan",
      "long_code":"UZB",
      "short_code":"UZ",
      "latitude":41.6,
      "longitude":63.3,
      "id":232,
      "continent_id": 4
   },
   {  
      "name":"Vanuatu",
      "long_code":"VUT",
      "short_code":"VU",
      "latitude":-16.4,
      "longitude":166.6,
      "id":233,
      "continent_id": 6
   },
   {  
      "name":"Venezuela",
      "long_code":"VEN",
      "short_code":"VE",
      "latitude":7.1,
      "longitude":-66.8,
      "id":234,
      "continent_id": 2
   },
   {  
      "name":"Vietnam",
      "long_code":"VNM",
      "short_code":"VN",
      "latitude":12.2,
      "longitude":108.3,
      "id":235,
      "continent_id": 4
   },
   {  
      "name":"Islas Virgenes Britanicas",
      "short_code":"VG",
      "long_code":null,
      "latitude":18.4,
      "longitude":-64.6,
      "id":236,
      "continent_id": 1
   },
   {  
      "name":"Islas Virgenes de los Estados Unidos",
      "short_code":"VI",
      "long_code":null,
      "latitude":17.7,
      "longitude":-64.7,
      "id":237,
      "continent_id": 1
   },
   {  
      "name":"Wallis y Futuna",
      "short_code":"WF",
      "long_code":null,
      "latitude":-13.7,
      "longitude":-177.7,
      "id":238,
      "continent_id": 6
   },
   {  
      "name":"Sahara Occidental",
      "long_code":"ESH",
      "short_code":"EH",
      "latitude":24.7,
      "longitude":-14.9,
      "id":239,
      "continent_id": 5
   },
   {  
      "name":"Yemen",
      "long_code":"YEM",
      "short_code":"YE",
      "latitude":15.5,
      "longitude":47.1,
      "id":240,
      "continent_id": 4
   },
   {  
      "name":"Zambia",
      "long_code":"ZMB",
      "short_code":"ZM",
      "latitude":-14.3,
      "longitude":28.1,
      "id":241,
      "continent_id": 5
   },
   {  
      "name":"Zimbabwe",
      "long_code":"ZWE",
      "short_code":"ZW",
      "latitude":-19.2,
      "longitude":29.1,
      "id":242,
      "continent_id": 5
   },
   {
      "name": "San Martin",
      "short_code": "MF",
      "long_code":null,
      "latitude":18.0,
      "longitude":-63.0,
      "id":243,
      "continent_id": 1
   },
   {
      "name": "Curazao",
      "short_code": "CW",
      "long_code": null,
      "latitude":12.2,
      "longitude":-69.2,
      "id":244,
      "continent_id": 2
   },
   {  
      "name":"Serbia",
      "long_code":"SRB",
      "short_code":"RS",
      "latitude":44.1,
      "longitude":20.3,
      "id":245,
      "continent_id": 3
   },
];
var annos = [1990, 1995, 2000, 2005, 2010, 2015, 2017];

export {
   continentes,
	paises,
	annos
}