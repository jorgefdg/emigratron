var movimientos = [
  {
    "anno": 1990,
    "destino": "Chipre",
    "total": 1,
    "cant_mjr": 1,
    "cant_hmbr": 0
  },
  {
    "anno": 1990,
    "destino": "Bulgaria",
    "total": 1,
    "cant_mjr": 0,
    "cant_hmbr": 1
  },
  {
    "anno": 1990,
    "destino": "Republica Checa",
    "total": 5,
    "cant_mjr": 3,
    "cant_hmbr": 2
  },
  {
    "anno": 1990,
    "destino": "Dinamarca",
    "total": 50,
    "cant_mjr": 22,
    "cant_hmbr": 28
  },
  {
    "anno": 1990,
    "destino": "Islandia",
    "total": 1,
    "cant_mjr": 1,
    "cant_hmbr": 0
  },
  {
    "anno": 1990,
    "destino": "Irlanda",
    "total": 1,
    "cant_mjr": 1,
    "cant_hmbr": 0
  },
  {
    "anno": 1990,
    "destino": "Noruega",
    "total": 6,
    "cant_mjr": 3,
    "cant_hmbr": 3
  },
  {
    "anno": 1990,
    "destino": "Suecia",
    "total": 84,
    "cant_mjr": 36,
    "cant_hmbr": 48
  },
  {
    "anno": 1990,
    "destino": "Reino Unido",
    "total": 21,
    "cant_mjr": 10,
    "cant_hmbr": 11
  },
  {
    "anno": 1990,
    "destino": "Grecia",
    "total": 143,
    "cant_mjr": 63,
    "cant_hmbr": 80
  },
  {
    "anno": 1990,
    "destino": "Italia",
    "total": 38,
    "cant_mjr": 25,
    "cant_hmbr": 13
  },
  {
    "anno": 1990,
    "destino": "España",
    "total": 38,
    "cant_mjr": 19,
    "cant_hmbr": 19
  },
  {
    "anno": 1990,
    "destino": "Austria",
    "total": 35,
    "cant_mjr": 20,
    "cant_hmbr": 15
  },
  {
    "anno": 1990,
    "destino": "Belgica",
    "total": 242,
    "cant_mjr": 116,
    "cant_hmbr": 126
  },
  {
    "anno": 1990,
    "destino": "Francia",
    "total": 26253,
    "cant_mjr": 14381,
    "cant_hmbr": 11872
  },
  {
    "anno": 1990,
    "destino": "Alemania",
    "total": 292,
    "cant_mjr": 119,
    "cant_hmbr": 173
  },
  {
    "anno": 1990,
    "destino": "Paises Bajos",
    "total": 271,
    "cant_mjr": 131,
    "cant_hmbr": 140
  },
  {
    "anno": 1990,
    "destino": "Suiza",
    "total": 993,
    "cant_mjr": 664,
    "cant_hmbr": 329
  },
  {
    "anno": 1990,
    "destino": "Antigua y Barbuda",
    "total": 11,
    "cant_mjr": 6,
    "cant_hmbr": 5
  },
  {
    "anno": 1990,
    "destino": "Aruba",
    "total": 320,
    "cant_mjr": 282,
    "cant_hmbr": 38
  },
  {
    "anno": 1990,
    "destino": "Bahamas",
    "total": 10894,
    "cant_mjr": 4080,
    "cant_hmbr": 6814
  },
  {
    "anno": 1990,
    "destino": "Caribe Holandes",
    "total": 4062,
    "cant_mjr": 1647,
    "cant_hmbr": 2415
  },
  {
    "anno": 1990,
    "destino": "Islas Caiman",
    "total": 6,
    "cant_mjr": 2,
    "cant_hmbr": 4
  },
  {
    "anno": 1990,
    "destino": "Cuba",
    "total": 2063,
    "cant_mjr": 585,
    "cant_hmbr": 1478
  },
  {
    "anno": 1990,
    "destino": "Dominica",
    "total": 561,
    "cant_mjr": 287,
    "cant_hmbr": 274
  },
  {
    "anno": 1990,
    "destino": "Republica Dominicana",
    "total": 187210,
    "cant_mjr": 64417,
    "cant_hmbr": 122793
  },
  {
    "anno": 1990,
    "destino": "Guadalupe",
    "total": 12028,
    "cant_mjr": 5252,
    "cant_hmbr": 6776
  },
  {
    "anno": 1990,
    "destino": "Martinica",
    "total": 1109,
    "cant_mjr": 478,
    "cant_hmbr": 631
  },
  {
    "anno": 1990,
    "destino": "Puerto Rico",
    "total": 295,
    "cant_mjr": 155,
    "cant_hmbr": 140
  },
  {
    "anno": 1990,
    "destino": "Islas Turcas y Caicos",
    "total": 2430,
    "cant_mjr": 1172,
    "cant_hmbr": 1258
  },
  {
    "anno": 1990,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 188,
    "cant_mjr": 98,
    "cant_hmbr": 90
  },
  {
    "anno": 1990,
    "destino": "Belice",
    "total": 35,
    "cant_mjr": 16,
    "cant_hmbr": 19
  },
  {
    "anno": 1990,
    "destino": "Costa Rica",
    "total": 48,
    "cant_mjr": 16,
    "cant_hmbr": 32
  },
  {
    "anno": 1990,
    "destino": "Guatemala",
    "total": 26,
    "cant_mjr": 19,
    "cant_hmbr": 7
  },
  {
    "anno": 1990,
    "destino": "Honduras",
    "total": 34,
    "cant_mjr": 5,
    "cant_hmbr": 29
  },
  {
    "anno": 1990,
    "destino": "Mexico",
    "total": 475,
    "cant_mjr": 92,
    "cant_hmbr": 383
  },
  {
    "anno": 1990,
    "destino": "Nicaragua",
    "total": 25,
    "cant_mjr": 6,
    "cant_hmbr": 19
  },
  {
    "anno": 1990,
    "destino": "Panama",
    "total": 122,
    "cant_mjr": 62,
    "cant_hmbr": 60
  },
  {
    "anno": 1990,
    "destino": "Argentina",
    "total": 75,
    "cant_mjr": 9,
    "cant_hmbr": 66
  },
  {
    "anno": 1990,
    "destino": "Bolivia",
    "total": 6,
    "cant_mjr": 1,
    "cant_hmbr": 5
  },
  {
    "anno": 1990,
    "destino": "Brasil",
    "total": 254,
    "cant_mjr": 135,
    "cant_hmbr": 119
  },
  {
    "anno": 1990,
    "destino": "Chile",
    "total": 36,
    "cant_mjr": 12,
    "cant_hmbr": 24
  },
  {
    "anno": 1990,
    "destino": "Colombia",
    "total": 100,
    "cant_mjr": 23,
    "cant_hmbr": 77
  },
  {
    "anno": 1990,
    "destino": "Ecuador",
    "total": 32,
    "cant_mjr": 10,
    "cant_hmbr": 22
  },
  {
    "anno": 1990,
    "destino": "Guayana Francesa",
    "total": 9605,
    "cant_mjr": 4479,
    "cant_hmbr": 5126
  },
  {
    "anno": 1990,
    "destino": "Peru",
    "total": 34,
    "cant_mjr": 20,
    "cant_hmbr": 14
  },
  {
    "anno": 1990,
    "destino": "Venezuela",
    "total": 2013,
    "cant_mjr": 878,
    "cant_hmbr": 1135
  },
  {
    "anno": 1990,
    "destino": "Bermudas",
    "total": 1028,
    "cant_mjr": 362,
    "cant_hmbr": 666
  },
  {
    "anno": 1990,
    "destino": "Canada",
    "total": 38271,
    "cant_mjr": 19640,
    "cant_hmbr": 18631
  },
  {
    "anno": 1990,
    "destino": "Estados Unidos",
    "total": 225393,
    "cant_mjr": 113063,
    "cant_hmbr": 112330
  },
  {
    "anno": 1990,
    "destino": "Australia",
    "total": 43,
    "cant_mjr": 21,
    "cant_hmbr": 22
  },
  {
    "anno": 1995,
    "destino": "Sri Lanka",
    "total": 3,
    "cant_mjr": 0,
    "cant_hmbr": 3
  },
  {
    "anno": 1995,
    "destino": "Chipre",
    "total": 2,
    "cant_mjr": 2,
    "cant_hmbr": 0
  },
  {
    "anno": 1995,
    "destino": "Bulgaria",
    "total": 2,
    "cant_mjr": 0,
    "cant_hmbr": 2
  },
  {
    "anno": 1995,
    "destino": "Republica Checa",
    "total": 8,
    "cant_mjr": 4,
    "cant_hmbr": 4
  },
  {
    "anno": 1995,
    "destino": "Dinamarca",
    "total": 90,
    "cant_mjr": 36,
    "cant_hmbr": 54
  },
  {
    "anno": 1995,
    "destino": "Finlandia",
    "total": 1,
    "cant_mjr": 0,
    "cant_hmbr": 1
  },
  {
    "anno": 1995,
    "destino": "Islandia",
    "total": 1,
    "cant_mjr": 1,
    "cant_hmbr": 0
  },
  {
    "anno": 1995,
    "destino": "Irlanda",
    "total": 3,
    "cant_mjr": 2,
    "cant_hmbr": 1
  },
  {
    "anno": 1995,
    "destino": "Noruega",
    "total": 10,
    "cant_mjr": 5,
    "cant_hmbr": 5
  },
  {
    "anno": 1995,
    "destino": "Suecia",
    "total": 99,
    "cant_mjr": 43,
    "cant_hmbr": 56
  },
  {
    "anno": 1995,
    "destino": "Reino Unido",
    "total": 86,
    "cant_mjr": 54,
    "cant_hmbr": 32
  },
  {
    "anno": 1995,
    "destino": "Grecia",
    "total": 87,
    "cant_mjr": 40,
    "cant_hmbr": 47
  },
  {
    "anno": 1995,
    "destino": "Italia",
    "total": 133,
    "cant_mjr": 99,
    "cant_hmbr": 34
  },
  {
    "anno": 1995,
    "destino": "España",
    "total": 58,
    "cant_mjr": 28,
    "cant_hmbr": 30
  },
  {
    "anno": 1995,
    "destino": "Austria",
    "total": 39,
    "cant_mjr": 22,
    "cant_hmbr": 17
  },
  {
    "anno": 1995,
    "destino": "Belgica",
    "total": 240,
    "cant_mjr": 115,
    "cant_hmbr": 125
  },
  {
    "anno": 1995,
    "destino": "Francia",
    "total": 27102,
    "cant_mjr": 15096,
    "cant_hmbr": 12006
  },
  {
    "anno": 1995,
    "destino": "Alemania",
    "total": 350,
    "cant_mjr": 180,
    "cant_hmbr": 170
  },
  {
    "anno": 1995,
    "destino": "Paises Bajos",
    "total": 399,
    "cant_mjr": 198,
    "cant_hmbr": 201
  },
  {
    "anno": 1995,
    "destino": "Suiza",
    "total": 1120,
    "cant_mjr": 704,
    "cant_hmbr": 416
  },
  {
    "anno": 1995,
    "destino": "Antigua y Barbuda",
    "total": 19,
    "cant_mjr": 10,
    "cant_hmbr": 9
  },
  {
    "anno": 1995,
    "destino": "Aruba",
    "total": 660,
    "cant_mjr": 482,
    "cant_hmbr": 178
  },
  {
    "anno": 1995,
    "destino": "Bahamas",
    "total": 15759,
    "cant_mjr": 6389,
    "cant_hmbr": 9370
  },
  {
    "anno": 1995,
    "destino": "Caribe Holandes",
    "total": 3574,
    "cant_mjr": 1490,
    "cant_hmbr": 2084
  },
  {
    "anno": 1995,
    "destino": "Islas Caiman",
    "total": 13,
    "cant_mjr": 5,
    "cant_hmbr": 8
  },
  {
    "anno": 1995,
    "destino": "Cuba",
    "total": 1564,
    "cant_mjr": 462,
    "cant_hmbr": 1102
  },
  {
    "anno": 1995,
    "destino": "Dominica",
    "total": 732,
    "cant_mjr": 375,
    "cant_hmbr": 357
  },
  {
    "anno": 1995,
    "destino": "Republica Dominicana",
    "total": 207931,
    "cant_mjr": 73279,
    "cant_hmbr": 134652
  },
  {
    "anno": 1995,
    "destino": "Guadalupe",
    "total": 10272,
    "cant_mjr": 4744,
    "cant_hmbr": 5528
  },
  {
    "anno": 1995,
    "destino": "Martinica",
    "total": 1335,
    "cant_mjr": 530,
    "cant_hmbr": 805
  },
  {
    "anno": 1995,
    "destino": "Puerto Rico",
    "total": 310,
    "cant_mjr": 163,
    "cant_hmbr": 147
  },
  {
    "anno": 1995,
    "destino": "Islas Turcas y Caicos",
    "total": 3567,
    "cant_mjr": 1754,
    "cant_hmbr": 1813
  },
  {
    "anno": 1995,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 346,
    "cant_mjr": 182,
    "cant_hmbr": 164
  },
  {
    "anno": 1995,
    "destino": "Belice",
    "total": 37,
    "cant_mjr": 17,
    "cant_hmbr": 20
  },
  {
    "anno": 1995,
    "destino": "Costa Rica",
    "total": 41,
    "cant_mjr": 15,
    "cant_hmbr": 26
  },
  {
    "anno": 1995,
    "destino": "Guatemala",
    "total": 18,
    "cant_mjr": 12,
    "cant_hmbr": 6
  },
  {
    "anno": 1995,
    "destino": "Honduras",
    "total": 34,
    "cant_mjr": 5,
    "cant_hmbr": 29
  },
  {
    "anno": 1995,
    "destino": "Mexico",
    "total": 395,
    "cant_mjr": 98,
    "cant_hmbr": 297
  },
  {
    "anno": 1995,
    "destino": "Nicaragua",
    "total": 20,
    "cant_mjr": 5,
    "cant_hmbr": 15
  },
  {
    "anno": 1995,
    "destino": "Panama",
    "total": 204,
    "cant_mjr": 103,
    "cant_hmbr": 101
  },
  {
    "anno": 1995,
    "destino": "Argentina",
    "total": 80,
    "cant_mjr": 18,
    "cant_hmbr": 62
  },
  {
    "anno": 1995,
    "destino": "Bolivia",
    "total": 7,
    "cant_mjr": 3,
    "cant_hmbr": 4
  },
  {
    "anno": 1995,
    "destino": "Brasil",
    "total": 135,
    "cant_mjr": 71,
    "cant_hmbr": 64
  },
  {
    "anno": 1995,
    "destino": "Chile",
    "total": 41,
    "cant_mjr": 16,
    "cant_hmbr": 25
  },
  {
    "anno": 1995,
    "destino": "Colombia",
    "total": 100,
    "cant_mjr": 26,
    "cant_hmbr": 74
  },
  {
    "anno": 1995,
    "destino": "Ecuador",
    "total": 36,
    "cant_mjr": 11,
    "cant_hmbr": 25
  },
  {
    "anno": 1995,
    "destino": "Guayana Francesa",
    "total": 11381,
    "cant_mjr": 5566,
    "cant_hmbr": 5815
  },
  {
    "anno": 1995,
    "destino": "Peru",
    "total": 30,
    "cant_mjr": 18,
    "cant_hmbr": 12
  },
  {
    "anno": 1995,
    "destino": "Venezuela",
    "total": 1824,
    "cant_mjr": 789,
    "cant_hmbr": 1035
  },
  {
    "anno": 1995,
    "destino": "Bermudas",
    "total": 1160,
    "cant_mjr": 450,
    "cant_hmbr": 710
  },
  {
    "anno": 1995,
    "destino": "Canada",
    "total": 45292,
    "cant_mjr": 23362,
    "cant_hmbr": 21930
  },
  {
    "anno": 1995,
    "destino": "Estados Unidos",
    "total": 326669,
    "cant_mjr": 163170,
    "cant_hmbr": 163499
  },
  {
    "anno": 1995,
    "destino": "Australia",
    "total": 53,
    "cant_mjr": 26,
    "cant_hmbr": 27
  },
  {
    "anno": 2000,
    "destino": "Sri Lanka",
    "total": 8,
    "cant_mjr": 3,
    "cant_hmbr": 5
  },
  {
    "anno": 2000,
    "destino": "Chipre",
    "total": 2,
    "cant_mjr": 2,
    "cant_hmbr": 0
  },
  {
    "anno": 2000,
    "destino": "Bulgaria",
    "total": 3,
    "cant_mjr": 0,
    "cant_hmbr": 3
  },
  {
    "anno": 2000,
    "destino": "Republica Checa",
    "total": 11,
    "cant_mjr": 5,
    "cant_hmbr": 6
  },
  {
    "anno": 2000,
    "destino": "Dinamarca",
    "total": 130,
    "cant_mjr": 49,
    "cant_hmbr": 81
  },
  {
    "anno": 2000,
    "destino": "Finlandia",
    "total": 1,
    "cant_mjr": 0,
    "cant_hmbr": 1
  },
  {
    "anno": 2000,
    "destino": "Islandia",
    "total": 1,
    "cant_mjr": 1,
    "cant_hmbr": 0
  },
  {
    "anno": 2000,
    "destino": "Irlanda",
    "total": 6,
    "cant_mjr": 3,
    "cant_hmbr": 3
  },
  {
    "anno": 2000,
    "destino": "Noruega",
    "total": 11,
    "cant_mjr": 3,
    "cant_hmbr": 8
  },
  {
    "anno": 2000,
    "destino": "Suecia",
    "total": 107,
    "cant_mjr": 47,
    "cant_hmbr": 60
  },
  {
    "anno": 2000,
    "destino": "Reino Unido",
    "total": 154,
    "cant_mjr": 100,
    "cant_hmbr": 54
  },
  {
    "anno": 2000,
    "destino": "Grecia",
    "total": 32,
    "cant_mjr": 18,
    "cant_hmbr": 14
  },
  {
    "anno": 2000,
    "destino": "Italia",
    "total": 228,
    "cant_mjr": 173,
    "cant_hmbr": 55
  },
  {
    "anno": 2000,
    "destino": "España",
    "total": 704,
    "cant_mjr": 313,
    "cant_hmbr": 391
  },
  {
    "anno": 2000,
    "destino": "Austria",
    "total": 43,
    "cant_mjr": 24,
    "cant_hmbr": 19
  },
  {
    "anno": 2000,
    "destino": "Belgica",
    "total": 232,
    "cant_mjr": 111,
    "cant_hmbr": 121
  },
  {
    "anno": 2000,
    "destino": "Francia",
    "total": 27950,
    "cant_mjr": 15810,
    "cant_hmbr": 12140
  },
  {
    "anno": 2000,
    "destino": "Alemania",
    "total": 408,
    "cant_mjr": 240,
    "cant_hmbr": 168
  },
  {
    "anno": 2000,
    "destino": "Paises Bajos",
    "total": 541,
    "cant_mjr": 273,
    "cant_hmbr": 268
  },
  {
    "anno": 2000,
    "destino": "Suiza",
    "total": 1252,
    "cant_mjr": 746,
    "cant_hmbr": 506
  },
  {
    "anno": 2000,
    "destino": "Antigua y Barbuda",
    "total": 27,
    "cant_mjr": 14,
    "cant_hmbr": 13
  },
  {
    "anno": 2000,
    "destino": "Aruba",
    "total": 1000,
    "cant_mjr": 681,
    "cant_hmbr": 319
  },
  {
    "anno": 2000,
    "destino": "Bahamas",
    "total": 20628,
    "cant_mjr": 8698,
    "cant_hmbr": 11930
  },
  {
    "anno": 2000,
    "destino": "Caribe Holandes",
    "total": 3086,
    "cant_mjr": 1334,
    "cant_hmbr": 1752
  },
  {
    "anno": 2000,
    "destino": "Islas Caiman",
    "total": 20,
    "cant_mjr": 8,
    "cant_hmbr": 12
  },
  {
    "anno": 2000,
    "destino": "Cuba",
    "total": 1065,
    "cant_mjr": 339,
    "cant_hmbr": 726
  },
  {
    "anno": 2000,
    "destino": "Dominica",
    "total": 873,
    "cant_mjr": 441,
    "cant_hmbr": 432
  },
  {
    "anno": 2000,
    "destino": "Republica Dominicana",
    "total": 228652,
    "cant_mjr": 82141,
    "cant_hmbr": 146511
  },
  {
    "anno": 2000,
    "destino": "Guadalupe",
    "total": 8515,
    "cant_mjr": 4235,
    "cant_hmbr": 4280
  },
  {
    "anno": 2000,
    "destino": "Martinica",
    "total": 1562,
    "cant_mjr": 583,
    "cant_hmbr": 979
  },
  {
    "anno": 2000,
    "destino": "Puerto Rico",
    "total": 327,
    "cant_mjr": 172,
    "cant_hmbr": 155
  },
  {
    "anno": 2000,
    "destino": "Islas Turcas y Caicos",
    "total": 5127,
    "cant_mjr": 2563,
    "cant_hmbr": 2564
  },
  {
    "anno": 2000,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 506,
    "cant_mjr": 267,
    "cant_hmbr": 239
  },
  {
    "anno": 2000,
    "destino": "Belice",
    "total": 38,
    "cant_mjr": 18,
    "cant_hmbr": 20
  },
  {
    "anno": 2000,
    "destino": "Costa Rica",
    "total": 54,
    "cant_mjr": 22,
    "cant_hmbr": 32
  },
  {
    "anno": 2000,
    "destino": "Guatemala",
    "total": 10,
    "cant_mjr": 5,
    "cant_hmbr": 5
  },
  {
    "anno": 2000,
    "destino": "Honduras",
    "total": 33,
    "cant_mjr": 5,
    "cant_hmbr": 28
  },
  {
    "anno": 2000,
    "destino": "Mexico",
    "total": 334,
    "cant_mjr": 109,
    "cant_hmbr": 225
  },
  {
    "anno": 2000,
    "destino": "Nicaragua",
    "total": 20,
    "cant_mjr": 7,
    "cant_hmbr": 13
  },
  {
    "anno": 2000,
    "destino": "Panama",
    "total": 300,
    "cant_mjr": 152,
    "cant_hmbr": 148
  },
  {
    "anno": 2000,
    "destino": "Argentina",
    "total": 86,
    "cant_mjr": 26,
    "cant_hmbr": 60
  },
  {
    "anno": 2000,
    "destino": "Bolivia",
    "total": 7,
    "cant_mjr": 5,
    "cant_hmbr": 2
  },
  {
    "anno": 2000,
    "destino": "Brasil",
    "total": 15,
    "cant_mjr": 7,
    "cant_hmbr": 8
  },
  {
    "anno": 2000,
    "destino": "Chile",
    "total": 45,
    "cant_mjr": 20,
    "cant_hmbr": 25
  },
  {
    "anno": 2000,
    "destino": "Colombia",
    "total": 99,
    "cant_mjr": 28,
    "cant_hmbr": 71
  },
  {
    "anno": 2000,
    "destino": "Ecuador",
    "total": 40,
    "cant_mjr": 12,
    "cant_hmbr": 28
  },
  {
    "anno": 2000,
    "destino": "Guayana Francesa",
    "total": 13157,
    "cant_mjr": 6652,
    "cant_hmbr": 6505
  },
  {
    "anno": 2000,
    "destino": "Peru",
    "total": 36,
    "cant_mjr": 22,
    "cant_hmbr": 14
  },
  {
    "anno": 2000,
    "destino": "Venezuela",
    "total": 1689,
    "cant_mjr": 726,
    "cant_hmbr": 963
  },
  {
    "anno": 2000,
    "destino": "Bermudas",
    "total": 1291,
    "cant_mjr": 537,
    "cant_hmbr": 754
  },
  {
    "anno": 2000,
    "destino": "Canada",
    "total": 53390,
    "cant_mjr": 27634,
    "cant_hmbr": 25756
  },
  {
    "anno": 2000,
    "destino": "Estados Unidos",
    "total": 429964,
    "cant_mjr": 214010,
    "cant_hmbr": 215954
  },
  {
    "anno": 2000,
    "destino": "Australia",
    "total": 60,
    "cant_mjr": 30,
    "cant_hmbr": 30
  },
  {
    "anno": 2005,
    "destino": "Sri Lanka",
    "total": 18,
    "cant_mjr": 10,
    "cant_hmbr": 8
  },
  {
    "anno": 2005,
    "destino": "Chipre",
    "total": 3,
    "cant_mjr": 3,
    "cant_hmbr": 0
  },
  {
    "anno": 2005,
    "destino": "Bulgaria",
    "total": 4,
    "cant_mjr": 1,
    "cant_hmbr": 3
  },
  {
    "anno": 2005,
    "destino": "Republica Checa",
    "total": 8,
    "cant_mjr": 4,
    "cant_hmbr": 4
  },
  {
    "anno": 2005,
    "destino": "Dinamarca",
    "total": 140,
    "cant_mjr": 54,
    "cant_hmbr": 86
  },
  {
    "anno": 2005,
    "destino": "Finlandia",
    "total": 7,
    "cant_mjr": 2,
    "cant_hmbr": 5
  },
  {
    "anno": 2005,
    "destino": "Islandia",
    "total": 4,
    "cant_mjr": 3,
    "cant_hmbr": 1
  },
  {
    "anno": 2005,
    "destino": "Irlanda",
    "total": 5,
    "cant_mjr": 3,
    "cant_hmbr": 2
  },
  {
    "anno": 2005,
    "destino": "Noruega",
    "total": 22,
    "cant_mjr": 7,
    "cant_hmbr": 15
  },
  {
    "anno": 2005,
    "destino": "Suecia",
    "total": 116,
    "cant_mjr": 52,
    "cant_hmbr": 64
  },
  {
    "anno": 2005,
    "destino": "Reino Unido",
    "total": 220,
    "cant_mjr": 125,
    "cant_hmbr": 95
  },
  {
    "anno": 2005,
    "destino": "Grecia",
    "total": 36,
    "cant_mjr": 21,
    "cant_hmbr": 15
  },
  {
    "anno": 2005,
    "destino": "Italia",
    "total": 466,
    "cant_mjr": 307,
    "cant_hmbr": 159
  },
  {
    "anno": 2005,
    "destino": "España",
    "total": 339,
    "cant_mjr": 153,
    "cant_hmbr": 186
  },
  {
    "anno": 2005,
    "destino": "Austria",
    "total": 49,
    "cant_mjr": 28,
    "cant_hmbr": 21
  },
  {
    "anno": 2005,
    "destino": "Belgica",
    "total": 209,
    "cant_mjr": 100,
    "cant_hmbr": 109
  },
  {
    "anno": 2005,
    "destino": "Francia",
    "total": 67078,
    "cant_mjr": 37038,
    "cant_hmbr": 30040
  },
  {
    "anno": 2005,
    "destino": "Alemania",
    "total": 884,
    "cant_mjr": 488,
    "cant_hmbr": 396
  },
  {
    "anno": 2005,
    "destino": "Paises Bajos",
    "total": 972,
    "cant_mjr": 480,
    "cant_hmbr": 492
  },
  {
    "anno": 2005,
    "destino": "Suiza",
    "total": 1398,
    "cant_mjr": 873,
    "cant_hmbr": 525
  },
  {
    "anno": 2005,
    "destino": "Antigua y Barbuda",
    "total": 29,
    "cant_mjr": 15,
    "cant_hmbr": 14
  },
  {
    "anno": 2005,
    "destino": "Aruba",
    "total": 1288,
    "cant_mjr": 842,
    "cant_hmbr": 446
  },
  {
    "anno": 2005,
    "destino": "Bahamas",
    "total": 23047,
    "cant_mjr": 9823,
    "cant_hmbr": 13224
  },
  {
    "anno": 2005,
    "destino": "Barbados",
    "total": 24,
    "cant_mjr": 16,
    "cant_hmbr": 8
  },
  {
    "anno": 2005,
    "destino": "Caribe Holandes",
    "total": 3288,
    "cant_mjr": 1422,
    "cant_hmbr": 1866
  },
  {
    "anno": 2005,
    "destino": "Islas Caiman",
    "total": 19,
    "cant_mjr": 7,
    "cant_hmbr": 12
  },
  {
    "anno": 2005,
    "destino": "Cuba",
    "total": 1061,
    "cant_mjr": 335,
    "cant_hmbr": 726
  },
  {
    "anno": 2005,
    "destino": "Dominica",
    "total": 1071,
    "cant_mjr": 546,
    "cant_hmbr": 525
  },
  {
    "anno": 2005,
    "destino": "Republica Dominicana",
    "total": 271273,
    "cant_mjr": 100748,
    "cant_hmbr": 170525
  },
  {
    "anno": 2005,
    "destino": "Guadalupe",
    "total": 11623,
    "cant_mjr": 6023,
    "cant_hmbr": 5600
  },
  {
    "anno": 2005,
    "destino": "Martinica",
    "total": 1634,
    "cant_mjr": 625,
    "cant_hmbr": 1009
  },
  {
    "anno": 2005,
    "destino": "Puerto Rico",
    "total": 299,
    "cant_mjr": 157,
    "cant_hmbr": 142
  },
  {
    "anno": 2005,
    "destino": "San Martin",
    "total": 1317,
    "cant_mjr": 558,
    "cant_hmbr": 759
  },
  {
    "anno": 2005,
    "destino": "Islas Turcas y Caicos",
    "total": 5652,
    "cant_mjr": 2804,
    "cant_hmbr": 2848
  },
  {
    "anno": 2005,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 574,
    "cant_mjr": 301,
    "cant_hmbr": 273
  },
  {
    "anno": 2005,
    "destino": "Belice",
    "total": 42,
    "cant_mjr": 20,
    "cant_hmbr": 22
  },
  {
    "anno": 2005,
    "destino": "Costa Rica",
    "total": 35,
    "cant_mjr": 15,
    "cant_hmbr": 20
  },
  {
    "anno": 2005,
    "destino": "Guatemala",
    "total": 11,
    "cant_mjr": 5,
    "cant_hmbr": 6
  },
  {
    "anno": 2005,
    "destino": "Honduras",
    "total": 32,
    "cant_mjr": 5,
    "cant_hmbr": 27
  },
  {
    "anno": 2005,
    "destino": "Mexico",
    "total": 602,
    "cant_mjr": 218,
    "cant_hmbr": 384
  },
  {
    "anno": 2005,
    "destino": "Nicaragua",
    "total": 21,
    "cant_mjr": 11,
    "cant_hmbr": 10
  },
  {
    "anno": 2005,
    "destino": "Panama",
    "total": 371,
    "cant_mjr": 197,
    "cant_hmbr": 174
  },
  {
    "anno": 2005,
    "destino": "Argentina",
    "total": 64,
    "cant_mjr": 19,
    "cant_hmbr": 45
  },
  {
    "anno": 2005,
    "destino": "Bolivia",
    "total": 10,
    "cant_mjr": 5,
    "cant_hmbr": 5
  },
  {
    "anno": 2005,
    "destino": "Brasil",
    "total": 35,
    "cant_mjr": 22,
    "cant_hmbr": 13
  },
  {
    "anno": 2005,
    "destino": "Chile",
    "total": 37,
    "cant_mjr": 15,
    "cant_hmbr": 22
  },
  {
    "anno": 2005,
    "destino": "Colombia",
    "total": 95,
    "cant_mjr": 28,
    "cant_hmbr": 67
  },
  {
    "anno": 2005,
    "destino": "Ecuador",
    "total": 47,
    "cant_mjr": 13,
    "cant_hmbr": 34
  },
  {
    "anno": 2005,
    "destino": "Guayana Francesa",
    "total": 14450,
    "cant_mjr": 7868,
    "cant_hmbr": 6582
  },
  {
    "anno": 2005,
    "destino": "Peru",
    "total": 43,
    "cant_mjr": 26,
    "cant_hmbr": 17
  },
  {
    "anno": 2005,
    "destino": "Venezuela",
    "total": 1750,
    "cant_mjr": 754,
    "cant_hmbr": 996
  },
  {
    "anno": 2005,
    "destino": "Bermudas",
    "total": 1475,
    "cant_mjr": 695,
    "cant_hmbr": 780
  },
  {
    "anno": 2005,
    "destino": "Canada",
    "total": 66504,
    "cant_mjr": 36305,
    "cant_hmbr": 30199
  },
  {
    "anno": 2005,
    "destino": "Estados Unidos",
    "total": 491772,
    "cant_mjr": 243421,
    "cant_hmbr": 248351
  },
  {
    "anno": 2005,
    "destino": "Australia",
    "total": 60,
    "cant_mjr": 20,
    "cant_hmbr": 40
  },
  {
    "anno": 2010,
    "destino": "Sri Lanka",
    "total": 38,
    "cant_mjr": 21,
    "cant_hmbr": 17
  },
  {
    "anno": 2010,
    "destino": "Chipre",
    "total": 5,
    "cant_mjr": 5,
    "cant_hmbr": 0
  },
  {
    "anno": 2010,
    "destino": "Bulgaria",
    "total": 5,
    "cant_mjr": 1,
    "cant_hmbr": 4
  },
  {
    "anno": 2010,
    "destino": "Republica Checa",
    "total": 4,
    "cant_mjr": 2,
    "cant_hmbr": 2
  },
  {
    "anno": 2010,
    "destino": "Dinamarca",
    "total": 150,
    "cant_mjr": 59,
    "cant_hmbr": 91
  },
  {
    "anno": 2010,
    "destino": "Finlandia",
    "total": 12,
    "cant_mjr": 4,
    "cant_hmbr": 8
  },
  {
    "anno": 2010,
    "destino": "Islandia",
    "total": 7,
    "cant_mjr": 5,
    "cant_hmbr": 2
  },
  {
    "anno": 2010,
    "destino": "Irlanda",
    "total": 4,
    "cant_mjr": 3,
    "cant_hmbr": 1
  },
  {
    "anno": 2010,
    "destino": "Noruega",
    "total": 30,
    "cant_mjr": 11,
    "cant_hmbr": 19
  },
  {
    "anno": 2010,
    "destino": "Suecia",
    "total": 132,
    "cant_mjr": 62,
    "cant_hmbr": 70
  },
  {
    "anno": 2010,
    "destino": "Reino Unido",
    "total": 304,
    "cant_mjr": 161,
    "cant_hmbr": 143
  },
  {
    "anno": 2010,
    "destino": "Grecia",
    "total": 40,
    "cant_mjr": 23,
    "cant_hmbr": 17
  },
  {
    "anno": 2010,
    "destino": "Italia",
    "total": 703,
    "cant_mjr": 441,
    "cant_hmbr": 262
  },
  {
    "anno": 2010,
    "destino": "Eslovenia",
    "total": 2,
    "cant_mjr": 0,
    "cant_hmbr": 2
  },
  {
    "anno": 2010,
    "destino": "España",
    "total": 610,
    "cant_mjr": 268,
    "cant_hmbr": 342
  },
  {
    "anno": 2010,
    "destino": "Austria",
    "total": 54,
    "cant_mjr": 31,
    "cant_hmbr": 23
  },
  {
    "anno": 2010,
    "destino": "Belgica",
    "total": 245,
    "cant_mjr": 110,
    "cant_hmbr": 135
  },
  {
    "anno": 2010,
    "destino": "Francia",
    "total": 68723,
    "cant_mjr": 39643,
    "cant_hmbr": 29080
  },
  {
    "anno": 2010,
    "destino": "Alemania",
    "total": 1279,
    "cant_mjr": 687,
    "cant_hmbr": 592
  },
  {
    "anno": 2010,
    "destino": "Paises Bajos",
    "total": 1245,
    "cant_mjr": 603,
    "cant_hmbr": 642
  },
  {
    "anno": 2010,
    "destino": "Suiza",
    "total": 1573,
    "cant_mjr": 1100,
    "cant_hmbr": 473
  },
  {
    "anno": 2010,
    "destino": "Antigua y Barbuda",
    "total": 31,
    "cant_mjr": 16,
    "cant_hmbr": 15
  },
  {
    "anno": 2010,
    "destino": "Aruba",
    "total": 1551,
    "cant_mjr": 986,
    "cant_hmbr": 565
  },
  {
    "anno": 2010,
    "destino": "Bahamas",
    "total": 25465,
    "cant_mjr": 10984,
    "cant_hmbr": 14481
  },
  {
    "anno": 2010,
    "destino": "Barbados",
    "total": 32,
    "cant_mjr": 22,
    "cant_hmbr": 10
  },
  {
    "anno": 2010,
    "destino": "Caribe Holandes",
    "total": 88,
    "cant_mjr": 48,
    "cant_hmbr": 40
  },
  {
    "anno": 2010,
    "destino": "Islas Caiman",
    "total": 18,
    "cant_mjr": 7,
    "cant_hmbr": 11
  },
  {
    "anno": 2010,
    "destino": "Cuba",
    "total": 969,
    "cant_mjr": 304,
    "cant_hmbr": 665
  },
  {
    "anno": 2010,
    "destino": "Curazao",
    "total": 1811,
    "cant_mjr": 819,
    "cant_hmbr": 992
  },
  {
    "anno": 2010,
    "destino": "Dominica",
    "total": 1196,
    "cant_mjr": 602,
    "cant_hmbr": 594
  },
  {
    "anno": 2010,
    "destino": "Republica Dominicana",
    "total": 311969,
    "cant_mjr": 118641,
    "cant_hmbr": 193328
  },
  {
    "anno": 2010,
    "destino": "Guadalupe",
    "total": 14731,
    "cant_mjr": 7811,
    "cant_hmbr": 6920
  },
  {
    "anno": 2010,
    "destino": "Martinica",
    "total": 1707,
    "cant_mjr": 668,
    "cant_hmbr": 1039
  },
  {
    "anno": 2010,
    "destino": "Puerto Rico",
    "total": 235,
    "cant_mjr": 125,
    "cant_hmbr": 110
  },
  {
    "anno": 2010,
    "destino": "San Martin",
    "total": 2635,
    "cant_mjr": 1117,
    "cant_hmbr": 1518
  },
  {
    "anno": 2010,
    "destino": "Islas Turcas y Caicos",
    "total": 9756,
    "cant_mjr": 4878,
    "cant_hmbr": 4878
  },
  {
    "anno": 2010,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 644,
    "cant_mjr": 336,
    "cant_hmbr": 308
  },
  {
    "anno": 2010,
    "destino": "Belice",
    "total": 46,
    "cant_mjr": 22,
    "cant_hmbr": 24
  },
  {
    "anno": 2010,
    "destino": "Costa Rica",
    "total": 70,
    "cant_mjr": 29,
    "cant_hmbr": 41
  },
  {
    "anno": 2010,
    "destino": "Guatemala",
    "total": 12,
    "cant_mjr": 4,
    "cant_hmbr": 8
  },
  {
    "anno": 2010,
    "destino": "Honduras",
    "total": 31,
    "cant_mjr": 4,
    "cant_hmbr": 27
  },
  {
    "anno": 2010,
    "destino": "Mexico",
    "total": 940,
    "cant_mjr": 354,
    "cant_hmbr": 586
  },
  {
    "anno": 2010,
    "destino": "Nicaragua",
    "total": 22,
    "cant_mjr": 11,
    "cant_hmbr": 11
  },
  {
    "anno": 2010,
    "destino": "Panama",
    "total": 461,
    "cant_mjr": 249,
    "cant_hmbr": 212
  },
  {
    "anno": 2010,
    "destino": "Argentina",
    "total": 41,
    "cant_mjr": 11,
    "cant_hmbr": 30
  },
  {
    "anno": 2010,
    "destino": "Bolivia",
    "total": 13,
    "cant_mjr": 4,
    "cant_hmbr": 9
  },
  {
    "anno": 2010,
    "destino": "Brasil",
    "total": 54,
    "cant_mjr": 36,
    "cant_hmbr": 18
  },
  {
    "anno": 2010,
    "destino": "Chile",
    "total": 28,
    "cant_mjr": 9,
    "cant_hmbr": 19
  },
  {
    "anno": 2010,
    "destino": "Colombia",
    "total": 108,
    "cant_mjr": 33,
    "cant_hmbr": 75
  },
  {
    "anno": 2010,
    "destino": "Ecuador",
    "total": 66,
    "cant_mjr": 19,
    "cant_hmbr": 47
  },
  {
    "anno": 2010,
    "destino": "Guayana Francesa",
    "total": 15743,
    "cant_mjr": 9083,
    "cant_hmbr": 6660
  },
  {
    "anno": 2010,
    "destino": "Peru",
    "total": 46,
    "cant_mjr": 28,
    "cant_hmbr": 18
  },
  {
    "anno": 2010,
    "destino": "Venezuela",
    "total": 1810,
    "cant_mjr": 782,
    "cant_hmbr": 1028
  },
  {
    "anno": 2010,
    "destino": "Bermudas",
    "total": 1658,
    "cant_mjr": 852,
    "cant_hmbr": 806
  },
  {
    "anno": 2010,
    "destino": "Canada",
    "total": 80100,
    "cant_mjr": 45081,
    "cant_hmbr": 35019
  },
  {
    "anno": 2010,
    "destino": "Estados Unidos",
    "total": 570290,
    "cant_mjr": 285170,
    "cant_hmbr": 285120
  },
  {
    "anno": 2010,
    "destino": "Australia",
    "total": 70,
    "cant_mjr": 40,
    "cant_hmbr": 30
  },
  {
    "anno": 2015,
    "destino": "Guinea",
    "total": 25,
    "cant_mjr": 12,
    "cant_hmbr": 13
  },
  {
    "anno": 2015,
    "destino": "Sri Lanka",
    "total": 37,
    "cant_mjr": 21,
    "cant_hmbr": 16
  },
  {
    "anno": 2015,
    "destino": "Chipre",
    "total": 5,
    "cant_mjr": 5,
    "cant_hmbr": 0
  },
  {
    "anno": 2015,
    "destino": "Bulgaria",
    "total": 6,
    "cant_mjr": 1,
    "cant_hmbr": 5
  },
  {
    "anno": 2015,
    "destino": "Republica Checa",
    "total": 4,
    "cant_mjr": 2,
    "cant_hmbr": 2
  },
  {
    "anno": 2015,
    "destino": "Dinamarca",
    "total": 164,
    "cant_mjr": 66,
    "cant_hmbr": 98
  },
  {
    "anno": 2015,
    "destino": "Finlandia",
    "total": 12,
    "cant_mjr": 3,
    "cant_hmbr": 9
  },
  {
    "anno": 2015,
    "destino": "Islandia",
    "total": 12,
    "cant_mjr": 9,
    "cant_hmbr": 3
  },
  {
    "anno": 2015,
    "destino": "Irlanda",
    "total": 4,
    "cant_mjr": 3,
    "cant_hmbr": 1
  },
  {
    "anno": 2015,
    "destino": "Noruega",
    "total": 41,
    "cant_mjr": 17,
    "cant_hmbr": 24
  },
  {
    "anno": 2015,
    "destino": "Suecia",
    "total": 144,
    "cant_mjr": 74,
    "cant_hmbr": 70
  },
  {
    "anno": 2015,
    "destino": "Reino Unido",
    "total": 265,
    "cant_mjr": 141,
    "cant_hmbr": 124
  },
  {
    "anno": 2015,
    "destino": "Grecia",
    "total": 38,
    "cant_mjr": 23,
    "cant_hmbr": 15
  },
  {
    "anno": 2015,
    "destino": "Italia",
    "total": 703,
    "cant_mjr": 449,
    "cant_hmbr": 254
  },
  {
    "anno": 2015,
    "destino": "Eslovenia",
    "total": 17,
    "cant_mjr": 7,
    "cant_hmbr": 10
  },
  {
    "anno": 2015,
    "destino": "España",
    "total": 723,
    "cant_mjr": 327,
    "cant_hmbr": 396
  },
  {
    "anno": 2015,
    "destino": "Austria",
    "total": 63,
    "cant_mjr": 35,
    "cant_hmbr": 28
  },
  {
    "anno": 2015,
    "destino": "Belgica",
    "total": 274,
    "cant_mjr": 123,
    "cant_hmbr": 151
  },
  {
    "anno": 2015,
    "destino": "Francia",
    "total": 75616,
    "cant_mjr": 43988,
    "cant_hmbr": 31628
  },
  {
    "anno": 2015,
    "destino": "Alemania",
    "total": 1333,
    "cant_mjr": 718,
    "cant_hmbr": 615
  },
  {
    "anno": 2015,
    "destino": "Paises Bajos",
    "total": 1411,
    "cant_mjr": 677,
    "cant_hmbr": 734
  },
  {
    "anno": 2015,
    "destino": "Suiza",
    "total": 1848,
    "cant_mjr": 1202,
    "cant_hmbr": 646
  },
  {
    "anno": 2015,
    "destino": "Antigua y Barbuda",
    "total": 33,
    "cant_mjr": 17,
    "cant_hmbr": 16
  },
  {
    "anno": 2015,
    "destino": "Aruba",
    "total": 1632,
    "cant_mjr": 1037,
    "cant_hmbr": 595
  },
  {
    "anno": 2015,
    "destino": "Bahamas",
    "total": 27591,
    "cant_mjr": 11948,
    "cant_hmbr": 15643
  },
  {
    "anno": 2015,
    "destino": "Barbados",
    "total": 33,
    "cant_mjr": 23,
    "cant_hmbr": 10
  },
  {
    "anno": 2015,
    "destino": "Caribe Holandes",
    "total": 100,
    "cant_mjr": 54,
    "cant_hmbr": 46
  },
  {
    "anno": 2015,
    "destino": "Islas Caiman",
    "total": 17,
    "cant_mjr": 6,
    "cant_hmbr": 11
  },
  {
    "anno": 2015,
    "destino": "Cuba",
    "total": 872,
    "cant_mjr": 274,
    "cant_hmbr": 598
  },
  {
    "anno": 2015,
    "destino": "Curazao",
    "total": 1968,
    "cant_mjr": 888,
    "cant_hmbr": 1080
  },
  {
    "anno": 2015,
    "destino": "Dominica",
    "total": 1395,
    "cant_mjr": 701,
    "cant_hmbr": 694
  },
  {
    "anno": 2015,
    "destino": "Republica Dominicana",
    "total": 329281,
    "cant_mjr": 125297,
    "cant_hmbr": 203984
  },
  {
    "anno": 2015,
    "destino": "Guadalupe",
    "total": 15284,
    "cant_mjr": 8182,
    "cant_hmbr": 7102
  },
  {
    "anno": 2015,
    "destino": "Martinica",
    "total": 1768,
    "cant_mjr": 698,
    "cant_hmbr": 1070
  },
  {
    "anno": 2015,
    "destino": "Puerto Rico",
    "total": 160,
    "cant_mjr": 86,
    "cant_hmbr": 74
  },
  {
    "anno": 2015,
    "destino": "San Martin",
    "total": 2746,
    "cant_mjr": 1165,
    "cant_hmbr": 1581
  },
  {
    "anno": 2015,
    "destino": "Islas Turcas y Caicos",
    "total": 13928,
    "cant_mjr": 6964,
    "cant_hmbr": 6964
  },
  {
    "anno": 2015,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 644,
    "cant_mjr": 336,
    "cant_hmbr": 308
  },
  {
    "anno": 2015,
    "destino": "Belice",
    "total": 53,
    "cant_mjr": 26,
    "cant_hmbr": 27
  },
  {
    "anno": 2015,
    "destino": "Costa Rica",
    "total": 71,
    "cant_mjr": 29,
    "cant_hmbr": 42
  },
  {
    "anno": 2015,
    "destino": "Guatemala",
    "total": 14,
    "cant_mjr": 5,
    "cant_hmbr": 9
  },
  {
    "anno": 2015,
    "destino": "Honduras",
    "total": 43,
    "cant_mjr": 5,
    "cant_hmbr": 38
  },
  {
    "anno": 2015,
    "destino": "Mexico",
    "total": 1296,
    "cant_mjr": 444,
    "cant_hmbr": 852
  },
  {
    "anno": 2015,
    "destino": "Nicaragua",
    "total": 23,
    "cant_mjr": 11,
    "cant_hmbr": 12
  },
  {
    "anno": 2015,
    "destino": "Panama",
    "total": 542,
    "cant_mjr": 294,
    "cant_hmbr": 248
  },
  {
    "anno": 2015,
    "destino": "Argentina",
    "total": 41,
    "cant_mjr": 11,
    "cant_hmbr": 30
  },
  {
    "anno": 2015,
    "destino": "Bolivia",
    "total": 15,
    "cant_mjr": 5,
    "cant_hmbr": 10
  },
  {
    "anno": 2015,
    "destino": "Brasil",
    "total": 65,
    "cant_mjr": 43,
    "cant_hmbr": 22
  },
  {
    "anno": 2015,
    "destino": "Chile",
    "total": 44,
    "cant_mjr": 17,
    "cant_hmbr": 27
  },
  {
    "anno": 2015,
    "destino": "Colombia",
    "total": 120,
    "cant_mjr": 36,
    "cant_hmbr": 84
  },
  {
    "anno": 2015,
    "destino": "Ecuador",
    "total": 96,
    "cant_mjr": 27,
    "cant_hmbr": 69
  },
  {
    "anno": 2015,
    "destino": "Guayana Francesa",
    "total": 19005,
    "cant_mjr": 11097,
    "cant_hmbr": 7908
  },
  {
    "anno": 2015,
    "destino": "Peru",
    "total": 49,
    "cant_mjr": 30,
    "cant_hmbr": 19
  },
  {
    "anno": 2015,
    "destino": "Venezuela",
    "total": 1900,
    "cant_mjr": 831,
    "cant_hmbr": 1069
  },
  {
    "anno": 2015,
    "destino": "Bermudas",
    "total": 1680,
    "cant_mjr": 862,
    "cant_hmbr": 818
  },
  {
    "anno": 2015,
    "destino": "Canada",
    "total": 89577,
    "cant_mjr": 50415,
    "cant_hmbr": 39162
  },
  {
    "anno": 2015,
    "destino": "Estados Unidos",
    "total": 649941,
    "cant_mjr": 329047,
    "cant_hmbr": 320894
  },
  {
    "anno": 2015,
    "destino": "Australia",
    "total": 95,
    "cant_mjr": 42,
    "cant_hmbr": 53
  },
  {
    "anno": 2017,
    "destino": "Guinea",
    "total": 24,
    "cant_mjr": 11,
    "cant_hmbr": 13
  },
  {
    "anno": 2017,
    "destino": "Sri Lanka",
    "total": 37,
    "cant_mjr": 21,
    "cant_hmbr": 16
  },
  {
    "anno": 2017,
    "destino": "Chipre",
    "total": 4,
    "cant_mjr": 4,
    "cant_hmbr": 0
  },
  {
    "anno": 2017,
    "destino": "Bulgaria",
    "total": 6,
    "cant_mjr": 1,
    "cant_hmbr": 5
  },
  {
    "anno": 2017,
    "destino": "Republica Checa",
    "total": 4,
    "cant_mjr": 2,
    "cant_hmbr": 2
  },
  {
    "anno": 2017,
    "destino": "Dinamarca",
    "total": 180,
    "cant_mjr": 72,
    "cant_hmbr": 108
  },
  {
    "anno": 2017,
    "destino": "Finlandia",
    "total": 13,
    "cant_mjr": 3,
    "cant_hmbr": 10
  },
  {
    "anno": 2017,
    "destino": "Islandia",
    "total": 12,
    "cant_mjr": 9,
    "cant_hmbr": 3
  },
  {
    "anno": 2017,
    "destino": "Irlanda",
    "total": 4,
    "cant_mjr": 3,
    "cant_hmbr": 1
  },
  {
    "anno": 2017,
    "destino": "Noruega",
    "total": 43,
    "cant_mjr": 18,
    "cant_hmbr": 25
  },
  {
    "anno": 2017,
    "destino": "Suecia",
    "total": 145,
    "cant_mjr": 70,
    "cant_hmbr": 75
  },
  {
    "anno": 2017,
    "destino": "Reino Unido",
    "total": 278,
    "cant_mjr": 149,
    "cant_hmbr": 129
  },
  {
    "anno": 2017,
    "destino": "Grecia",
    "total": 37,
    "cant_mjr": 22,
    "cant_hmbr": 15
  },
  {
    "anno": 2017,
    "destino": "Italia",
    "total": 715,
    "cant_mjr": 455,
    "cant_hmbr": 260
  },
  {
    "anno": 2017,
    "destino": "Eslovenia",
    "total": 19,
    "cant_mjr": 9,
    "cant_hmbr": 10
  },
  {
    "anno": 2017,
    "destino": "España",
    "total": 729,
    "cant_mjr": 332,
    "cant_hmbr": 397
  },
  {
    "anno": 2017,
    "destino": "Austria",
    "total": 69,
    "cant_mjr": 38,
    "cant_hmbr": 31
  },
  {
    "anno": 2017,
    "destino": "Belgica",
    "total": 277,
    "cant_mjr": 124,
    "cant_hmbr": 153
  },
  {
    "anno": 2017,
    "destino": "Francia",
    "total": 75467,
    "cant_mjr": 44010,
    "cant_hmbr": 31457
  },
  {
    "anno": 2017,
    "destino": "Alemania",
    "total": 1586,
    "cant_mjr": 846,
    "cant_hmbr": 740
  },
  {
    "anno": 2017,
    "destino": "Paises Bajos",
    "total": 1518,
    "cant_mjr": 727,
    "cant_hmbr": 791
  },
  {
    "anno": 2017,
    "destino": "Suiza",
    "total": 1916,
    "cant_mjr": 1246,
    "cant_hmbr": 670
  },
  {
    "anno": 2017,
    "destino": "Antigua y Barbuda",
    "total": 33,
    "cant_mjr": 17,
    "cant_hmbr": 16
  },
  {
    "anno": 2017,
    "destino": "Aruba",
    "total": 1642,
    "cant_mjr": 1045,
    "cant_hmbr": 597
  },
  {
    "anno": 2017,
    "destino": "Bahamas",
    "total": 28754,
    "cant_mjr": 12451,
    "cant_hmbr": 16303
  },
  {
    "anno": 2017,
    "destino": "Barbados",
    "total": 33,
    "cant_mjr": 23,
    "cant_hmbr": 10
  },
  {
    "anno": 2017,
    "destino": "Caribe Holandes",
    "total": 103,
    "cant_mjr": 56,
    "cant_hmbr": 47
  },
  {
    "anno": 2017,
    "destino": "Islas Caiman",
    "total": 17,
    "cant_mjr": 6,
    "cant_hmbr": 11
  },
  {
    "anno": 2017,
    "destino": "Cuba",
    "total": 858,
    "cant_mjr": 269,
    "cant_hmbr": 589
  },
  {
    "anno": 2017,
    "destino": "Curazao",
    "total": 2009,
    "cant_mjr": 906,
    "cant_hmbr": 1103
  },
  {
    "anno": 2017,
    "destino": "Dominica",
    "total": 1407,
    "cant_mjr": 707,
    "cant_hmbr": 700
  },
  {
    "anno": 2017,
    "destino": "Republica Dominicana",
    "total": 336729,
    "cant_mjr": 128130,
    "cant_hmbr": 208599
  },
  {
    "anno": 2017,
    "destino": "Guadalupe",
    "total": 15414,
    "cant_mjr": 8251,
    "cant_hmbr": 7163
  },
  {
    "anno": 2017,
    "destino": "Martinica",
    "total": 1763,
    "cant_mjr": 696,
    "cant_hmbr": 1067
  },
  {
    "anno": 2017,
    "destino": "Puerto Rico",
    "total": 156,
    "cant_mjr": 83,
    "cant_hmbr": 73
  },
  {
    "anno": 2017,
    "destino": "San Martin",
    "total": 2843,
    "cant_mjr": 1206,
    "cant_hmbr": 1637
  },
  {
    "anno": 2017,
    "destino": "Islas Turcas y Caicos",
    "total": 15040,
    "cant_mjr": 7519,
    "cant_hmbr": 7521
  },
  {
    "anno": 2017,
    "destino": "Islas Virgenes de los Estados Unidos",
    "total": 644,
    "cant_mjr": 336,
    "cant_hmbr": 308
  },
  {
    "anno": 2017,
    "destino": "Belice",
    "total": 58,
    "cant_mjr": 28,
    "cant_hmbr": 30
  },
  {
    "anno": 2017,
    "destino": "Costa Rica",
    "total": 71,
    "cant_mjr": 29,
    "cant_hmbr": 42
  },
  {
    "anno": 2017,
    "destino": "Guatemala",
    "total": 14,
    "cant_mjr": 5,
    "cant_hmbr": 9
  },
  {
    "anno": 2017,
    "destino": "Honduras",
    "total": 43,
    "cant_mjr": 5,
    "cant_hmbr": 38
  },
  {
    "anno": 2017,
    "destino": "Mexico",
    "total": 1329,
    "cant_mjr": 455,
    "cant_hmbr": 874
  },
  {
    "anno": 2017,
    "destino": "Nicaragua",
    "total": 23,
    "cant_mjr": 11,
    "cant_hmbr": 12
  },
  {
    "anno": 2017,
    "destino": "Panama",
    "total": 559,
    "cant_mjr": 303,
    "cant_hmbr": 256
  },
  {
    "anno": 2017,
    "destino": "Argentina",
    "total": 42,
    "cant_mjr": 11,
    "cant_hmbr": 31
  },
  {
    "anno": 2017,
    "destino": "Bolivia",
    "total": 15,
    "cant_mjr": 5,
    "cant_hmbr": 10
  },
  {
    "anno": 2017,
    "destino": "Brasil",
    "total": 66,
    "cant_mjr": 44,
    "cant_hmbr": 22
  },
  {
    "anno": 2017,
    "destino": "Chile",
    "total": 45,
    "cant_mjr": 17,
    "cant_hmbr": 28
  },
  {
    "anno": 2017,
    "destino": "Colombia",
    "total": 122,
    "cant_mjr": 36,
    "cant_hmbr": 86
  },
  {
    "anno": 2017,
    "destino": "Ecuador",
    "total": 98,
    "cant_mjr": 27,
    "cant_hmbr": 71
  },
  {
    "anno": 2017,
    "destino": "Guayana Francesa",
    "total": 20009,
    "cant_mjr": 11683,
    "cant_hmbr": 8326
  },
  {
    "anno": 2017,
    "destino": "Peru",
    "total": 50,
    "cant_mjr": 30,
    "cant_hmbr": 20
  },
  {
    "anno": 2017,
    "destino": "Venezuela",
    "total": 1929,
    "cant_mjr": 843,
    "cant_hmbr": 1086
  },
  {
    "anno": 2017,
    "destino": "Bermudas",
    "total": 1664,
    "cant_mjr": 854,
    "cant_hmbr": 810
  },
  {
    "anno": 2017,
    "destino": "Canada",
    "total": 93131,
    "cant_mjr": 52415,
    "cant_hmbr": 40716
  },
  {
    "anno": 2017,
    "destino": "Estados Unidos",
    "total": 671499,
    "cant_mjr": 339961,
    "cant_hmbr": 331538
  },
  {
    "anno": 2017,
    "destino": "Australia",
    "total": 99,
    "cant_mjr": 44,
    "cant_hmbr": 55
  }
];

var salud = [
  {
    "anno": 1990,
    "exp_vid_mjr": 56.1,
    "exp_vid_hbr": 53.1,
    "mort_mjr": 29.7,
    "mort_hbr": 35.2,
    "maln_inf": 40.1,
    "gast_salud": "",
    "mort_inf": 14.49,
    "Pprev_vih": 3.7,
    "inc_mal": ""
  },
  {
    "anno": 1995,
    "exp_vid_mjr": 58,
    "exp_vid_hbr": 54.6,
    "mort_mjr": 28.2,
    "mort_hbr": 24.4,
    "maln_inf": 37.1,
    "gast_salud": "",
    "mort_inf": 12.43,
    "Pprev_vih": 3.8,
    "inc_mal": ""
  },
  {
    "anno": 2000,
    "exp_vid_mjr": 59.5,
    "exp_vid_hbr": 55.9,
    "mort_mjr": 27.4,
    "mort_hbr": 33.8,
    "maln_inf": 28.8,
    "gast_salud": 6.9,
    "mort_inf": 10.45,
    "Pprev_vih": 2.9,
    "inc_mal": 3.65
  },
  {
    "anno": 2005,
    "exp_vid_mjr": 61.1,
    "exp_vid_hbr": 57.3,
    "mort_mjr": 25.6,
    "mort_hbr": 31.9,
    "maln_inf": 29.6,
    "gast_salud": 5.5,
    "mort_inf": 8.98,
    "Pprev_vih": 2.4,
    "inc_mal": 3.66
  },
  {
    "anno": 2010,
    "exp_vid_mjr": 63.4,
    "exp_vid_hbr": 59.2,
    "mort_mjr": 23.1,
    "mort_hbr": 29.3,
    "maln_inf": 22,
    "gast_salud": 10.2,
    "mort_inf": 20.8,
    "Pprev_vih": 2.2,
    "inc_mal": 3.66
  },
  {
    "anno": 2011,
    "exp_vid_mjr": 63.8,
    "exp_vid_hbr": 59.6,
    "mort_mjr": 22.6,
    "mort_hbr": 28.8,
    "maln_inf": "",
    "gast_salud": 10.6,
    "mort_inf": 7.72,
    "Pprev_vih": 2.2,
    "inc_mal": ""
  },
  {
    "anno": 2012,
    "exp_vid_mjr": 64.2,
    "exp_vid_hbr": 60,
    "mort_mjr": 22.2,
    "mort_hbr": 28.3,
    "maln_inf": "",
    "gast_salud": 10.1,
    "mort_inf": 7.52,
    "Pprev_vih": 2.2,
    "inc_mal": ""
  },
  {
    "anno": 2013,
    "exp_vid_mjr": 64.6,
    "exp_vid_hbr": 60.3,
    "mort_mjr": 21.9,
    "mort_hbr": 28.1,
    "maln_inf": "",
    "gast_salud": 7.2,
    "mort_inf": 7.31,
    "Pprev_vih": 2.2,
    "inc_mal": ""
  },
  {
    "anno": 2014,
    "exp_vid_mjr": 64.9,
    "exp_vid_hbr": 60.6,
    "mort_mjr": 21.6,
    "mort_hbr": 27.8,
    "maln_inf": "",
    "gast_salud": 7.2,
    "mort_inf": 7.1,
    "Pprev_vih": 2.2,
    "inc_mal": ""
  },
  {
    "anno": 2015,
    "exp_vid_mjr": 65.2,
    "exp_vid_hbr": 60.9,
    "mort_mjr": 21.3,
    "mort_hbr": 27.5,
    "maln_inf": 21.9,
    "gast_salud": 6.9,
    "mort_inf": 6.89,
    "Pprev_vih": 2.1,
    "inc_mal": 1.13
  },
  {
    "anno": 2016,
    "exp_vid_mjr": 65.5,
    "exp_vid_hbr": 61.2,
    "mort_mjr": 21,
    "mort_hbr": 27.2,
    "maln_inf": 21.9,
    "gast_salud": "",
    "mort_inf": 6.7,
    "Pprev_vih": 2.1,
    "inc_mal": 1.39
  },
  {
    "anno": 2017,
    "exp_vid_mjr": 65.8,
    "exp_vid_hbr": 61.4,
    "mort_mjr": "",
    "mort_hbr": "",
    "maln_inf": 21.9,
    "gast_salud": "",
    "mort_inf": "",
    "Pprev_vih": "",
    "inc_mal": ""
  }
];

var trabajo = [
  {
    "anno": 1990,
    "desempleo": 7.7,
    "tasa_part_lab": 67.4,
    "tasa_part_lab_mjr": 57.4,
    "tasa_part_lab_hbr": 78.1
  },
  {
    "anno": 1995,
    "desempleo": 7,
    "tasa_part_lab": 65.8,
    "tasa_part_lab_mjr": 57.9,
    "tasa_part_lab_hbr": 74.2
  },
  {
    "anno": 2000,
    "desempleo": 8.6,
    "tasa_part_lab": 63.4,
    "tasa_part_lab_mjr": 57.6,
    "tasa_part_lab_hbr": 69.6
  },
  {
    "anno": 2005,
    "desempleo": 15,
    "tasa_part_lab": 64.5,
    "tasa_part_lab_mjr": 59.2,
    "tasa_part_lab_hbr": 70.1
  },
  {
    "anno": 2010,
    "desempleo": 16,
    "tasa_part_lab": 65.9,
    "tasa_part_lab_mjr": 60.7,
    "tasa_part_lab_hbr": 71.4
  },
  {
    "anno": 2011,
    "desempleo": 13.7,
    "tasa_part_lab": 66.2,
    "tasa_part_lab_mjr": 61.3,
    "tasa_part_lab_hbr": 71.5
  },
  {
    "anno": 2012,
    "desempleo": 14.1,
    "tasa_part_lab": 66.6,
    "tasa_part_lab_mjr": 61.8,
    "tasa_part_lab_hbr": 71.7
  },
  {
    "anno": 2013,
    "desempleo": 13.9,
    "tasa_part_lab": 66.9,
    "tasa_part_lab_mjr": 62.4,
    "tasa_part_lab_hbr": 71.8
  },
  {
    "anno": 2014,
    "desempleo": 13.9,
    "tasa_part_lab": 67.3,
    "tasa_part_lab_mjr": 62.8,
    "tasa_part_lab_hbr": 71.9
  },
  {
    "anno": 2015,
    "desempleo": 14.1,
    "tasa_part_lab": 67.6,
    "tasa_part_lab_mjr": 63.2,
    "tasa_part_lab_hbr": 72.2
  },
  {
    "anno": 2016,
    "desempleo": 14,
    "tasa_part_lab": 67.9,
    "tasa_part_lab_mjr": 63.5,
    "tasa_part_lab_hbr": 72.4
  },
  {
    "anno": 2017,
    "desempleo": 14,
    "tasa_part_lab": 68.1,
    "tasa_part_lab_mjr": 63.8,
    "tasa_part_lab_hbr": 72.6
  }
];

var finanzas = [
  {
    "anno": 1990,
    "exp_imp": "",
    "for_inv": "",
    "off_assist": "",
    "rem": "",
    "p_cap_flow": ""
  },
  {
    "anno": 1995,
    "exp_imp": 37.8,
    "for_inv": 0.3,
    "off_assist": "",
    "rem": "",
    "p_cap_flow": ""
  },
  {
    "anno": 2000,
    "exp_imp": 46.1,
    "for_inv": 0.3,
    "off_assist": 5.2,
    "rem": 14.62,
    "p_cap_flow": -0.3
  },
  {
    "anno": 2005,
    "exp_imp": 57,
    "for_inv": 0.6,
    "off_assist": 10,
    "rem": 22.88,
    "p_cap_flow": -0.6
  },
  {
    "anno": 2010,
    "exp_imp": 80.1,
    "for_inv": 2.7,
    "off_assist": 46.3,
    "rem": 22.25,
    "p_cap_flow": -2.7
  },
  {
    "anno": 2011,
    "exp_imp": 76.4,
    "for_inv": 1.6,
    "off_assist": 22.4,
    "rem": 20.64,
    "p_cap_flow": -1.6
  },
  {
    "anno": 2012,
    "exp_imp": 70.1,
    "for_inv": 2,
    "off_assist": 16,
    "rem": 20.43,
    "p_cap_flow": -2
  },
  {
    "anno": 2013,
    "exp_imp": 70.5,
    "for_inv": 1.9,
    "off_assist": 13.6,
    "rem": 21.07,
    "p_cap_flow": ""
  },
  {
    "anno": 2014,
    "exp_imp": 71.3,
    "for_inv": 1.1,
    "off_assist": 12.3,
    "rem": 22.53,
    "p_cap_flow": ""
  },
  {
    "anno": 2015,
    "exp_imp": 70.6,
    "for_inv": 1.2,
    "off_assist": 11.9,
    "rem": 25.16,
    "p_cap_flow": ""
  },
  {
    "anno": 2016,
    "exp_imp": 73.3,
    "for_inv": 1.3,
    "off_assist": 13.3,
    "rem": 29.59,
    "p_cap_flow": ""
  },
  {
    "anno": 2017,
    "exp_imp": 75.3,
    "for_inv": "",
    "off_assist": "",
    "rem": 29.25,
    "p_cap_flow": ""
  }
];

var recursos = [
  {
    "anno": 1990,
    "inb": 1904,
    "cred_int": "",
    "pib_pc": "",
    "pib": "",
    "form_brut_cap_fij": "",
    "ind_ing": "0,445"
  },
  {
    "anno": 1995,
    "inb": 1558,
    "cred_int": 25.3,
    "pib_pc": "",
    "pib": "",
    "form_brut_cap_fij": "",
    "ind_ing": "0,415"
  },
  {
    "anno": 2000,
    "inb": 1744,
    "cred_int": 34.3,
    "pib_pc": 1739,
    "pib": 14.9,
    "form_brut_cap_fij": 27.3,
    "ind_ing": "0,432"
  },
  {
    "anno": 2005,
    "inb": 1537,
    "cred_int": 34.3,
    "pib_pc": 1562,
    "pib": 14.5,
    "form_brut_cap_fij": 27.4,
    "ind_ing": "0,413"
  },
  {
    "anno": 2010,
    "inb": 1506,
    "cred_int": 21.3,
    "pib_pc": 1502,
    "pib": 15,
    "form_brut_cap_fij": 25.4,
    "ind_ing": "0,410"
  },
  {
    "anno": 2011,
    "inb": 1570,
    "cred_int": 18.1,
    "pib_pc": 1562,
    "pib": 15.8,
    "form_brut_cap_fij": 27.9,
    "ind_ing": "0,416"
  },
  {
    "anno": 2012,
    "inb": 1600,
    "cred_int": 21,
    "pib_pc": 1585,
    "pib": 16.3,
    "form_brut_cap_fij": 35.6,
    "ind_ing": "0,419"
  },
  {
    "anno": 2013,
    "inb": 1634,
    "cred_int": 26.2,
    "pib_pc": 1629,
    "pib": 17,
    "form_brut_cap_fij": 36.7,
    "ind_ing": "0,422"
  },
  {
    "anno": 2014,
    "inb": 1663,
    "cred_int": 28.9,
    "pib_pc": 1653,
    "pib": 17.5,
    "form_brut_cap_fij": 38.2,
    "ind_ing": "0,425"
  },
  {
    "anno": 2015,
    "inb": 1658,
    "cred_int": 32.2,
    "pib_pc": 1651,
    "pib": 17.7,
    "form_brut_cap_fij": 41.1,
    "ind_ing": "0,424"
  },
  {
    "anno": 2016,
    "inb": 1681,
    "cred_int": 31.6,
    "pib_pc": 1654,
    "pib": 17.9,
    "form_brut_cap_fij": 38.7,
    "ind_ing": "0,426"
  },
  {
    "anno": 2017,
    "inb": 1665,
    "cred_int": "",
    "pib_pc": 1653,
    "pib": 18.2,
    "form_brut_cap_fij": 43.4,
    "ind_ing": "0,425"
  }
];

export {
	salud,
	movimientos,
	trabajo,
	finanzas,
	recursos
}