/*
+------------------------------+---------+------+-----+---------+-------+
| Field                        | Type    | Null | Key | Default | Extra |
+------------------------------+---------+------+-----+---------+-------+
| anno                         | int(11) | NO   | PRI | NULL    |       |
| inb                          | int(11) | YES  |     | NULL    |       |
| credito_interno              | float   | YES  |     | NULL    |       |
| pib_per_capita               | int(11) | YES  |     | NULL    |       |
| pib_total                    | float   | YES  |     | NULL    |       |
| formacion_bruta_capital_fijo | float   | YES  |     | NULL    |       |
| indice_ingresos              | float   | YES  |     | NULL    |       |
+------------------------------+---------+------+-----+---------+-------+
*/
export default class Recursos {
	constructor(rec){
		this.inb = {
			name 	: 'Ingreso bruto neto  per cápita (PPA $ 2011)',
			value 	: rec.inb
		}
		this.cred_int = {
			name 	: 'Crédito interno proporcionado por el sector financiero (% del PIB)',
			value	: rec.cred_int
		}
		this.pib_pc = {
			name 	: 'PIB per cápita ($ PPP 2011)',
			value	: rec.pib_pc
		}
		this.pib = {
			name	: 'Producto interno bruto (PIB), total (miles de millones 2011 en PPA)',
			value	: rec.pib
		}
		this.form_brut_cap_fij = {
			name	: 'Formación bruta de capital fijo (% del PIB),Índice de ingresos',
			value	: rec.form_brut_cap_fij
		}
		this.ind_ing = {
			name 	: 'Índice de ingresos',
			value	: rec.ind_ing
		}
	}
} 