import {countries_data,names} from '@/data/countries_data.js';

export default class CountryDataManager{
	constructor(){
		this.data = countries_data
		this.dataNames = names
	}
	findData(name){
		return this.data.find(function(dat){
			return dat.country.toLowerCase() === name.toLowerCase()
		})
	}
	getByName(name){
		if(name === null || name===undefined){
			return null
		}
		let result = this.findData(name)
		if(result === undefined){
			return null
		}
		return result
	}
}