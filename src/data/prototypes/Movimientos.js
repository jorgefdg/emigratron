/*
	+------------------+--------------+------+-----+---------+-------+
	| Field            | Type         | Null | Key | Default | Extra |
	+------------------+--------------+------+-----+---------+-------+
	| anno             | int(11)      | NO   | PRI | NULL    |       |
	| destino          | varchar(100) | NO   | PRI | NULL    |       |
	| cantidad_total   | int(11)      | YES  |     | NULL    |       |
	| cantidad_mujeres | int(11)      | YES  |     | NULL    |       |
	| cantidad_hombres | int(11)      | YES  |     | NULL    |       |
	+------------------+--------------+------+-----+---------+-------+
*/
export default class Movimiento {
	constructor(mov){
		this.destino = mov.destino;
		this.total = {
			name	: 'Cantidad total',
			value	: mov.total
		}
		this.cant_mjr = {
			name 	: 'Cantidad de mujeres',
			value	: mov.cant_mjr,
		}
		this.cant_hmbr = {
			name	: 'Cantidad de hombres',
			value	: mov.cant_hmbr
		}
	}
}