export default class Finanzas{
	constructor(fin){
		this.exp_imp = {
			name	: 'Exportaciones e importaciones', 
			value	: fin.exp_imp
		}
		this.for_inv = {
			name 	: 'Inversión extranjera',
			value 	: fin.for_inv
		}
		this.off_assist = {
			name	: 'Asistencia oficial neta para el desarrollo recibida (% del INB)',
			value	: fin.off_assist
		}
		this.p_cap_flow = {
			name 	: 'Flujos capital privado (%PIB)',
			value	: fin.p_cap_flow
		}
		this.rem = {
			name	: 'Remesas, entradas (%PIB)',
			value 	: fin.rem
		}
	}
}