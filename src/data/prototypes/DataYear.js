import Finanza 	 	from './Finanzas.js';
import Recurso		from './Recursos.js';
import Salud 		from './Salud.js';
import Trabajo 		from './Trabajo.js';
import Movimiento 	from './Movimientos.js';
import Migracion 	from './Migraciones.js';

export default class DataYear{
	constructor(year ,movimientos, recursos, salud, trabajo, finanzas){
		function createMovimientos(movs){
			return movs.map(function(mov){
				return new Movimiento(mov);
			})
		}

		this.year 			= year;
		this.migraciones	= new Migracion(createMovimientos(movimientos));
		this.recursos 		= new Recurso(recursos[0]);
		this.salud 			= new Salud(salud[0]);
		this.trabajo 		= new Trabajo(trabajo[0]);
		this.finanzas 		= new Finanza(finanzas[0])
	}
}