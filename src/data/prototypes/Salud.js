/*
+-------------------------+---------+------+-----+---------+-------+
| Field                   | Type    | Null | Key | Default | Extra |
+-------------------------+---------+------+-----+---------+-------+
| anno                    | int(11) | NO   | PRI | NULL    |       |
| expectativa_vida_mujer  | float   | YES  |     | NULL    |       |
| expectativa_vida_hombre | float   | YES  |     | NULL    |       |
| mortalidad_mujer        | float   | YES  |     | NULL    |       |
| mortalidad_hombre       | float   | YES  |     | NULL    |       |
| malnutricion_infantil   | float   | YES  |     | NULL    |       |
| gasto_salud             | float   | YES  |     | NULL    |       |
| mortalidad_infantil     | float   | YES  |     | NULL    |       |
| prevalecencia_vih       | float   | YES  |     | NULL    |       |
| incidencia_malaria      | float   | YES  |     | NULL    |       |
+-------------------------+---------+------+-----+---------+-------+
*/
export default class Salud {
	constructor(sal){
		this.exp_vid_mjr = {
			name 	: 'Expectativa vida mujer',
			value 	: sal.exp_vid_mjr
		}
		this.exp_vid_hbr = {
			name 	: 'Expectativa vida hombre',
			value	: sal.exp_vid_hbr
		}
		this.mort_mjr = {
			name 	: 'Mortalidad mujer',
			value	: sal.mort_mjr
		}
		this.mort_hbr = {
			name 	: 'Mortalidad hombre',
			value	: sal.mort_hbr
		}
		this.maln_inf = {
			name 	: 'Malnutrición infantil',
			value 	: sal.maln_inf,
		}
		this.gast_salud = {
			name 	: 'Gasto en salud (% PIB)',
			value 	: sal.gast_salud
		}
		this.mort_inf = {
			name 	: 'Mortalidad infantil',
			value 	: sal.mort_inf
		}
		this.prev_vih = {
			name 	: 'Prevalencia VIH',
			value	: sal.prev_vih
		}
		this.inc_mal = {
			name 	: 'Incidencia malaria',
			value	: sal.inc_mal
		}
	}
}