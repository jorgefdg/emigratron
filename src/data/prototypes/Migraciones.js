import {continentes} from '../BasicData.js';

export default class Migraciones{
	constructor(movimientos){
		this.movimientos = movimientos;
	}
	sort(criteria){
		let posible_criteria = ['total','cant_mjr','cant_hmbr'];
		if(!posible_criteria.includes(criteria)){
			console.warn('Criterio inválido :'+criteria);
			return [];
		}
		return this.movimientos.sort(function(curr,next){
			return curr[criteria].value - next[criteria].value;
		});
	}
	getbyDestiny(pais){
		return this.movimientos.filter(function(mov){
			return mov.destino.id === pais.id
		})
	}
	getDistribution(name){
		const data = this.movimientos.find(function(dat){
			return dat.destino.name.toLowerCase() === name.toLowerCase()
		})
		return {cant_mjr:data.cant_mjr.value, cant_hmbr:data.cant_hmbr.value}
	}
	getByContinent(id){
		return this.movimientos.filter(function(mov){
			return mov.destino.continent_id === id
		})
	}
	getForD3(criteria='total', group_by='country'){
		let posible_criteria = ['total','cant_mjr','cant_hmbr'],
			posible_group_by = ['country', 'continent'];
		if(!posible_group_by.includes(group_by)){
			console.warn('Agrupación inválida: '+group_by);
			return []; 
		}
		if(!posible_criteria.includes(criteria)){
			console.warn('Criterio inválido: '+criteria);
			return [];
		}
		if(group_by === 'continent'){
			let r = this.getForD3GroupedByContinent(criteria);
			return r
		}
		return this.getForD3GroupedByCountry(criteria);
	}
	getForD3GroupedByContinent(criteria){
		const coordinates = [
			{latitude:35.9,longitude:-104.3},
			{latitude:-12.8,longitude: -58.3}, 
			{latitude:48.9,longitude:13.8},
			{latitude:47.2,longitude:99.8},
			{latitude:7.1,longitude:21.1},
			{latitude:-24.3,longitude:133.4},
			{latitude:-72.0,longitude:-1.8}];
		let data = coordinates.map(function(cord,i){
			let q = this.movimientos.reduce(function(prev,mov){
					if(mov.destino.continent.id === (i+1)){
						return prev+mov[criteria].value
					}
					return prev
				},0)
			return {
				latitude: cord.latitude,
				longitude:cord.longitude,
				continent_id: (i+1),
				quantity: q,
				country: null,
				code: null
			}
		}.bind(this))
		return data.filter(function(d){return d.quantity !== 0})
	}
	getForD3GroupedByCountry(criteria){
		return this.movimientos.map(function(mov){
			return {
				latitude: mov.destino.latitude,
				longitude: mov.destino.longitude,
				continent_id: mov.destino.continent_id,
				country: mov.destino.name,
				quantity: mov[criteria].value,
				code:mov.destino.short_code
			}	
		})
	}
	getSorted(order, criteria, limit=null){
		let sorted;
		if(order.toUpperCase() === 'DESC'){
			sorted = this.sort(criteria).reverse();
		}
		if(order.toUpperCase() === 'ASC'){
			sorted = this.sort(criteria);
		}
		if(limit !== null){
			sorted = sorted.slice(0, limit);
		}
		return sorted;
	}
}