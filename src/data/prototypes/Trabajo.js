/*
+-----------------------------------+---------+------+-----+---------+-------+
| Field                             | Type    | Null | Key | Default | Extra |
+-----------------------------------+---------+------+-----+---------+-------+
| anno                              | int(11) | NO   | PRI | NULL    |       |
| total_desempleo                   | float   | YES  |     | NULL    |       |
| tasa_participacion_laboral_total  | float   | YES  |     | NULL    |       |
| tasa_participacion_laboral_mujer  | float   | YES  |     | NULL    |       |
| tasa_participacion_laboral_hombre | float   | YES  |     | NULL    |       |
+-----------------------------------+---------+------+-----+---------+-------+
*/

export default class Trabajo {
	constructor(trab){
		this.desempleo = {
			name 	: 'Total desempleo',
			value 	: trab.desempleo
		}
		this.tasa_part_lab = {
			name 	: 'Tasa de partición laboral total',
			value 	: trab.tasa_part_lab
		}
		this.tasa_part_lab_mjr = {
			name 	: 'Tasa de partición laboral femenina',
			value	: trab.tasa_part_lab_mjr
		}
		this.tasa_part_lab_hbr = {
			name	: 'Tasa de partición laboral masculina',
			value	: trab.tasa_part_lab_hbr
		}
	}
}