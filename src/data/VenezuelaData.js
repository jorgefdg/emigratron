import {movimientos} from './RawDataVenezuela.js';
import {paises,annos,continentes} from  './BasicData.js';
import Movimiento	from './prototypes/Movimientos.js';
import Migraciones	from './prototypes/Migraciones.js';

var data = [];

function setContinentes(moves){
	return moves.map(function(mov){
		mov.destino.continent = continentes.find((c)=>{return c.id === mov.destino.continent_id})
		return mov;
	})
}

function filterByYear(arr,year){
	return arr.filter(function(obj){
		if(obj.anno === null){console.log(obj)}
		return obj.anno === year;
	}.bind(this))
}

function setDestinyData(movs){
	return movs.map(function(mov){
		mov.destino = paises.find(function(pais){ return mov.destino.toLowerCase() === pais.name.toLowerCase()});
		if(mov.destino !== undefined){
			return mov;
		}
		return null;
	}.bind(this));
}

function createMovimientos(movs){
	return movs.map(function(mov){
		return new Movimiento(mov);
	})
}

let moves = setDestinyData(movimientos);

moves = setContinentes(moves);

annos.forEach(function(anno){
	//let anno_salud = filterByYear(salud, anno);
	let anno_movim = filterByYear(moves, anno);
	anno_movim = createMovimientos(anno_movim);
	data.push({
		anno: anno,
		migraciones: new Migraciones(anno_movim)
	});
	//let anno_traba = filterByYear(trabajo, anno);
	//let anno_finan = filterByYear(finanzas, anno);
	//let anno_recur = filterByYear(recursos, anno);
	//data.push(new DataYear(anno, anno_movim, anno_recur, anno_salud, anno_traba, anno_finan));
})

export default data;