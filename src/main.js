import Vue from 'vue';
import App from './App.vue';
import router from './router';
import 'flag-icon-css/css/flag-icon.min.css';
import 'vue-select/dist/vue-select.css';

import Flag from './components/UI/Flag.vue';

Vue.config.productionTip = false
Vue.component('Flag', Flag);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
